# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Unit tests for regression.

Part of qmmlpack library.
"""

import pytest
import numpy as np

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ###########################
#  #  KernelRidgeRegression  #
#  ###########################

class TestKernelRidgeRegression:
    def test_queryable_properties(self):
        K = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        y = np.array( [ -1, 0, 1 ], dtype=np.float_)
        m = qmml.KernelRidgeRegression(K, y, [ 1. ], centering=False)

        assert _equal(m.kernel_matrix, [ [ 2, 0, -1 ], [ 0, 1, 0 ], [ -1, 0, 2 ] ])
        assert _equal(m.label_mean, 0)
        assert _equal(m.labels, [ -1, 0, 1 ])
        assert _equal(m.weights, [ -1/3., 0., 1/3. ])
        assert _equal(m.theta, [ 1. ])
        assert _equal(m.regularization_strength, [ 1., 1., 1. ])
        assert not m.centering

    def test_invalid_option(self):
        with pytest.raises(NameError):
            m.non_existing_option

    def test_two_samples_one_dimension_no_centering(self):
        X  = np.array( [ [-1], [1] ], dtype=np.float_ )
        XX = np.array( [ [-2], [-1], [0], [1], [2] ], dtype=np.float_ )
        y  = np.array( [ -1, 1 ], dtype=np.float_ )
        m  = qmml.KernelRidgeRegression(qmml.kernel_linear(X), y, [ 1e-7 ], centering=False)

        assert _equal( m(qmml.kernel_linear(X, XX), "Predictions"), [-2, -1, 0, 1, 2] )
        assert _equal( m(qmml.kernel_linear(X, XX)), [-2, -1, 0, 1, 2] )

    def test_two_samples_one_dimension_with_centering(self):
        X  = np.array( [ [-1], [0] ], dtype=np.float_ )
        XX = np.array( [ [-2], [-1], [0], [1] ], dtype=np.float_ )
        y  = np.array( [ 0, 1 ], dtype=np.float_ )
        m  = qmml.KernelRidgeRegression(qmml.kernel_linear(X), y, [ 1e-7 ], centering=True)

        assert np.allclose( m(qmml.kernel_linear(X, XX), "Predictions"), [-1, 0, 1, 2], rtol=1e-6, atol=1e-6 )
        assert np.allclose( m(qmml.kernel_linear(X, XX)), [-1, 0, 1, 2], rtol=1e-6, atol=1e-6 )

    def test_no_leak_K(self):
        # ensures that passed kernel matrix K is not modified by KernelRidgeRegression

        # no centering (different codepath from centering)
        Korig = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        Kcopy = np.copy(Korig)
        y = np.array( [ -1, 0, 1 ], dtype=np.float_)
        m = qmml.KernelRidgeRegression(Kcopy, y, [ 1. ], centering=False)

        assert _equal(Korig, Kcopy)

        # centering (different codepath from no centering)
        Korig = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        Kcopy = np.copy(Korig)
        y = np.array( [ -1, 0, 1 ], dtype=np.float_)
        m = qmml.KernelRidgeRegression(Kcopy, y, [ 1. ], centering=True)

        assert _equal(Korig, Kcopy)

    def test_no_leak_y(self):
        # ensures that passed label vector y is not modified by KernelRidgeRegression

        # no centering (different codepath from centering)
        K = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        yorig = np.array( [ -1, 0, 1 ], dtype=np.float_)
        ycopy = np.copy(yorig)
        m = qmml.KernelRidgeRegression(K, ycopy, [ 1. ], centering=False)

        assert _equal(yorig, ycopy)

        # centering (different codepath from no centering)
        K = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        yorig = np.array( [ -1, 0, 1 ], dtype=np.float_)
        ycopy = np.copy(yorig)
        m = qmml.KernelRidgeRegression(K, ycopy, [ 1. ], centering=True)

        assert _equal(yorig, ycopy)

    def test_no_leak_theta(self):
        # ensures that passed hyperparameter vector theta is not modified by KernelRidgeRegression

        # no centering (different codepath from centering)
        K = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        y = np.array( [ -1, 0, 1 ], dtype=np.float_)
        theta_orig = [ 1. ]; theta_copy = np.copy(theta_orig)
        m = qmml.KernelRidgeRegression(K, y, theta_copy, centering=False)

        assert _equal(theta_orig, theta_copy)

        # centering (different codepath from no centering)
        K = qmml.kernel_linear( np.array( [ [-1], [0], [1] ], dtype=np.float_) )
        y = np.array( [ -1, 0, 1 ], dtype=np.float_)
        theta_orig = [ 1. ]; theta_copy = np.copy(theta_orig)
        m = qmml.KernelRidgeRegression(K, y, theta_copy, centering=True)

        assert _equal(theta_orig, theta_copy)
