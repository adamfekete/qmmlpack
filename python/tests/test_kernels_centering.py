# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Unit tests for centering of kernel matrices.

Part of qmmlpack library.
"""

import pytest
import numpy as np
from math import exp, sqrt

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

class TestCenterKernelMatrixSimpleExamples:
    def test_four_corners_of_square_in_plane(self):
        x  = np.array( [ [  0,  0 ], [ 2,  0 ], [ 2, 2 ], [  0, 2 ] ], dtype=np.float_)
        xc = np.array( [ [ -1, -1 ], [ 1, -1 ], [ 1, 1 ], [ -1, 1 ] ], dtype=np.float_)
        z  = np.array( [ [  1,  0 ], [ 2,  1 ], [ 1, 2 ], [  0, 1 ], [ 1, 1 ] ], dtype=np.float_)
        zc = np.array( [ [  0, -1 ], [ 1,  0 ], [ 0, 1 ], [ -1, 0 ], [ 0, 0 ] ], dtype=np.float_)
        k = qmml.kernel_linear(x)
        l = qmml.kernel_linear(x, z)
        m = qmml.kernel_linear(z)
        assert _equal(qmml.center_kernel_matrix(k), qmml.kernel_linear(xc))
        assert _equal(qmml.center_kernel_matrix(k, l), qmml.kernel_linear(xc, zc))
        assert _equal(qmml.center_kernel_matrix(k, l, m), qmml.kernel_linear(zc))
        assert _equal(qmml.center_kernel_matrix(k, l, np.diag(m)), qmml.kernel_linear(zc, diagonal=True))

    def test_already_centered_matrix(self):
        x = np.array( [ [ 1, 0 ], [ 0, 1 ], [ -1, 0 ], [  0, -1 ] ], dtype=np.float_)  # already centered
        z = np.array( [ [ 0, 0 ], [ 1, 1 ], [ -1, 1 ], [ -1, -1 ], [ 1, -1 ] ], dtype=np.float_);
        k = qmml.kernel_linear(x)
        l = qmml.kernel_linear(x, z)
        m = qmml.kernel_linear(z)
        assert _equal(qmml.center_kernel_matrix(k), qmml.kernel_linear(x))
        assert _equal(qmml.center_kernel_matrix(k, l), qmml.kernel_linear(x, z))
        assert _equal(qmml.center_kernel_matrix(k, l, m), qmml.kernel_linear(z))
        assert _equal(qmml.center_kernel_matrix(k, l, np.diag(m)), qmml.kernel_linear(z, diagonal=True))

    def test_explicit_centering_linear(self):
        x = np.array(  # random 5 x 7 matrix via RandomVariate[UniformDistribution[{-1, 1}], {5, 7}]
        [
            [ 0.4083490992163403, -0.4722036310884134, -0.7133467805491098,  0.2247836461102847, -0.719503409363579, -0.1069898523366137, -0.05469955030632],
            [ 0.4958288968614499, -0.3747408826635814, -0.9999212253004539,  0.5205869933588789,  0.519199845236859, -0.0018685879162445, -0.24011250592445],
            [-0.6215029121172009,  0.1311954712172514, -0.6608474349547375, -0.8874563863238518, -0.438787870208581, -0.1874539932402448, -0.41225051671516],
            [ 0.0263832300394972, -0.9191075197464689, -0.3601131791319156, -0.7209818217983011, -0.920836529436206, -0.6400415960479342, -0.82831651693080],
            [ 0.5370991473711153,  0.0917310483499074, -0.6163690913164408, -0.9216996195139551,  0.176330121310050,  0.8725666376878318, -0.03316446640701]
        ], dtype=np.float_);
        xc = np.array(  # centered via Standardize[xx, Mean, 1 &]
        [
            [ 0.23911760694209994, -0.16357852830215242, -0.043227238298578330,  0.58173708374367350, -0.44278384087128764, -0.094232373965972630,  0.25900916095042800],
            [ 0.32659740458720950, -0.06611577987732042, -0.329801683049922400,  0.87754043099226780,  0.79591941372915040,  0.010888890454396572,  0.07359620533229799],
            [-0.79073440439144130,  0.43982057400351240,  0.009272107295793974, -0.53050294869046300, -0.16206830171628960, -0.174696514869603750, -0.09854180545841201],
            [-0.14284826223474320, -0.61048241696020790,  0.310006363118615900, -0.36402838416491223, -0.64411696094391460, -0.627284117677293100, -0.51460780567405200],
            [ 0.36786765509687490,  0.40035615113616840,  0.053750450934090700, -0.56474618188056620,  0.45304968980234140,  0.885324116058472900,  0.28054424484973800]
        ], dtype=np.float_);
        assert _equal( qmml.center_kernel_matrix(qmml.kernel_linear(x)), qmml.kernel_linear(xc) )

class TestCenterKernelMatrixSymmetry:
    def test_from_linear_kernel(self):
        n, d = 100, 50
        kc = qmml.center_kernel_matrix(qmml.kernel_linear(np.random.uniform(-1, 1, (n,d))))
        assert ( kc == kc.T ).all()

    def test_from_gaussian_kernel(self):
        n, d = 100, 50
        kc = qmml.center_kernel_matrix(qmml.kernel_gaussian(np.random.uniform(-1, 1, (n,d)), theta=5.))
        assert ( kc == kc.T ).all()

class TestRandomizedVersusNumpyVersion:
    def center_kernel_matrix_k(self, k):
        n = len(k)
        onesn = np.identity(n) - np.ones((n,n)) / n  # onesn = identity (n,n) - (1/n)*ones (n,n)
        return onesn @ k @ onesn

    def center_kernel_matrix_l(self, k, l):
        n = len(k); m = l.shape[1]
        onesnn = np.ones((n,n)) / n
        onesnm = np.ones((n,m)) / n
        return l - k @ onesnm - onesnn @ l + onesnn @ k @ onesnm

    def center_kernel_matrix_m(self, k, l, mm):
        n = len(k); m = l.shape[1]
        onesnm = np.ones((n,m)) / n
        onesmn = np.ones((m,n)) / n
        return mm - np.transpose(l) @ onesnm - onesmn @ l + onesmn @ k @ onesnm

    def center_kernel_matrix_mdiag(self, k, l, mv):
        m = l.shape[1]
        return mv - 2*np.mean(l, axis=0) + np.mean(k)

    def test_randomized_versus_numpy_version(self):
        x = 2*(np.random.rand(15, 5) - 0.5)
        z = 2*(np.random.rand(25, 5) - 0.5)
        k = qmml.kernel_gaussian(x, theta=[1])
        l = qmml.kernel_gaussian(x, z, theta=[1])
        m = qmml.kernel_gaussian(z, theta=[1])
        assert _equal(qmml.center_kernel_matrix(k), self.center_kernel_matrix_k(k))
        assert _equal(qmml.center_kernel_matrix(k, l), self.center_kernel_matrix_l(k, l))
        assert _equal(qmml.center_kernel_matrix(k, l, m), self.center_kernel_matrix_m(k, l, m))
        assert _equal(qmml.center_kernel_matrix(k, l, np.diag(m)), self.center_kernel_matrix_mdiag(k, l, np.diag(m)))
