// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for linear kernel

#include "kernels_linear.hpp"
#include "qmmlpack/kernels_linear.hpp"

// kernels_linear

void _kernel_matrix_linear_k(py_cdarray_t kk, py_cdarray_t xx)
{
    auto bufk = kk.request(), bufx = xx.request();

    if(bufk.ndim != 2 || bufx.ndim != 2) throw QMML_ERROR("_kernel_matrix_linear_k arguments must be matrices");

    const size_t n = bufk.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufk.shape[1] != n) throw QMML_ERROR("_kernel_matrix_linear_k: K must be quadratic");
    if(bufx.shape[0] != n) throw QMML_ERROR("_kernel_matrix_linear_k: X and K must have same number of rows")

    qmml::kernel_matrix_linear_k(static_cast<double*>(bufk.ptr), static_cast<double*>(bufx.ptr), n, d);
}

void _kernel_matrix_linear_l(py_cdarray_t ll, py_cdarray_t xx, py_cdarray_t zz)
{
    auto bufl = ll.request(), bufx = xx.request(), bufz = zz.request();

    if(bufl.ndim != 2 || bufx.ndim != 2 || bufz.ndim != 2) throw QMML_ERROR("_kernel_matrix_linear_l arguments must be matrices");

    const size_t n = bufl.shape[0];  // number of training samples
    const size_t m = bufl.shape[1];  // number of test samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufx.shape[0] != n) throw QMML_ERROR("_kernel_matrix_linear_l: X and L must have same number of rows");
    if(bufz.shape[0] != m) throw QMML_ERROR("_kernel_matrix_linear_l: Z must have as many rows as L has columns");
    if(bufz.shape[1] != d) throw QMML_ERROR("_kernel_matrix_linear_l: X and Z must have same number of dimensions");

    qmml::kernel_matrix_linear_l(static_cast<double*>(bufl.ptr), static_cast<double*>(bufx.ptr), static_cast<double*>(bufz.ptr), n, m, d);
}

void _kernel_matrix_linear_m(py_cdarray_t m, py_cdarray_t xx)
{
    auto bufm = m.request(), bufx = xx.request();

    if(bufm.ndim != 1 || bufx.ndim != 2) throw QMML_ERROR("_kernel_matrix_linear_m arguments must be vector and matrix");

    const size_t n = bufx.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufm.shape[0] != n) throw QMML_ERROR("_kernel_matrix_linear_m: m must be vector of compatible dimension");

    qmml::kernel_matrix_linear_m(static_cast<double*>(bufm.ptr), static_cast<double*>(bufx.ptr), n, d);
}

// Python bindings

void init_kernels_linear(py::module &m){
    m.def("_kernel_matrix_linear_k", &_kernel_matrix_linear_k, "Computes linear kernel matrix K", py::arg("K"), py::arg("X"));
    m.def("_kernel_matrix_linear_l", &_kernel_matrix_linear_l, "Computes linear kernel matrix L", py::arg("L"), py::arg("X"), py::arg("Z"));
    m.def("_kernel_matrix_linear_m", &_kernel_matrix_linear_m, "Computes linear kernel vector m", py::arg("m"), py::arg("X"));
}
