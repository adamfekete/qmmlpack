// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for input/output of extended XYZ format

#include "io_extxyz.hpp"
#include "pybind11/stl.h"

// io_exyz

std::vector<qmml::extxyz_data> _import_extxyz_string(std::string const& s, bool addp)
{
    return qmml::import_extxyz<std::string::const_iterator>(s.cbegin(), s.cend(), addp);
}

std::vector<qmml::extxyz_data> _import_extxyz_file(std::string const& fn, bool addp)
{
    return qmml::import_extxyz<qmml::parsing_file_iterator>(qmml::parsing_file_iterator(fn), qmml::parsing_file_iterator(), addp);
}

// Python bindings

void init_io_extxyz(py::module &m)
{
    py::class_<qmml::parsing_token>(m, "_parsing_token")
        .def("is_int"   , &qmml::parsing_token::is_int   , "True if parsing token holds an integer.")
        .def("is_float" , &qmml::parsing_token::is_float , "True if parsing token holds a floating point value.")
        .def("is_string", &qmml::parsing_token::is_string, "True if parsing token holds a string.")
        .def("intv"     , static_cast<int         const&(qmml::parsing_token::*)() const>(&qmml::parsing_token::intv   ), "Returns integer value of parsing token.")
        .def("floatv"   , static_cast<double      const&(qmml::parsing_token::*)() const>(&qmml::parsing_token::floatv ), "Returns floating point value of parsing token.")
        .def("stringv"  , static_cast<std::string const&(qmml::parsing_token::*)() const>(&qmml::parsing_token::stringv), "Returns string value of parsing token.");

    py::class_<qmml::extxyz_data>(m, "_ExtXYZData")
        .def(py::init<size_t>())
        .def_readwrite("an"  , &qmml::extxyz_data::an  )
        .def_readwrite("xyz" , &qmml::extxyz_data::xyz )
        .def_readwrite("mp"  , &qmml::extxyz_data::mp  )
        .def_readwrite("ap"  , &qmml::extxyz_data::ap  )
        .def_readwrite("addp", &qmml::extxyz_data::addp);

    m.def("_import_extxyz_string", &_import_extxyz_string, "Imports data in extended XYZ format from a string.", py::arg("s"), py::arg("addp") = false);
    m.def("_import_extxyz_file", &_import_extxyz_file, "Imports data in extended XYZ format from a file.", py::arg("fn"), py::arg("addp") = false);
}
