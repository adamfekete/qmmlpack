// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for numerics utilities

#ifndef QMML_PYTHON_NUMERICS_HPP_INCLUDED  // include guard
#define QMML_PYTHON_NUMERICS_HPP_INCLUDED

#include "python.hpp"

// lower triangular part

size_t _lower_triangular_part_nelems(size_t n, size_t m, ptrdiff_t k);
void _lower_triangular_part(py_cdarray_t v, py_cdarray_t M, ptrdiff_t k);

// symmetrize

void _symmetrize(py_cdarray_t M);

// forward and backward substitution

void _forward_substitution_v(py_cdarray_t x, py_cdarray_t L, py_cdarray_t b);
void _forward_substitution_m(py_cdarray_t X, py_cdarray_t L, py_cdarray_t B);
void _backward_substitution_v(py_cdarray_t x, py_cdarray_t U, py_cdarray_t b);
void _backward_substitution_m(py_cdarray_t X, py_cdarray_t U, py_cdarray_t B);
void _forward_backward_substitution_v(py_cdarray_t x, py_cdarray_t L, py_cdarray_t b);
void _backward_forward_substitution_v(py_cdarray_t x, py_cdarray_t U, py_cdarray_t b);

// Python bindings

void init_numerics(py::module &m);

#endif // include guard
