// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for p-norm distances

#ifndef QMML_PYTHON_DISTANCES_PNORM_HPP_INCLUDED  // include guard
#define QMML_PYTHON_DISTANCES_PNORM_HPP_INCLUDED

#include "python.hpp"

// distances_pnorm

// One-norm distance
void _distance_matrix_one_norm_k(py_cdarray_t dd, py_cdarray_t xx);
void _distance_matrix_one_norm_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz);

// Euclidean distance
void _distance_matrix_euclidean_k(py_cdarray_t dd, py_cdarray_t xx);
void _distance_matrix_euclidean_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz);

// Squared Euclidean distance
void _distance_matrix_squared_euclidean_k(py_cdarray_t dd, py_cdarray_t xx);
void _distance_matrix_squared_euclidean_l(py_cdarray_t dd, py_cdarray_t xx, py_cdarray_t zz);

// Python bindings

void init_distances_pnorm(py::module &m);

#endif // include guard
