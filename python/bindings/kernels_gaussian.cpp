// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for Gaussian kernel

#include "kernels_gaussian.hpp"
#include "qmmlpack/kernels_gaussian.hpp"

// kernels_gaussian

void _kernel_matrix_gaussian_d(py_cdarray_t ll, py_cdarray_t dd, double sigma)
{
    auto bufl = ll.request(), bufd = dd.request();

    if(bufl.ndim != 2 || bufd.ndim != 2) throw QMML_ERROR("_kernel_matrix_gaussian_d arguments must be matrices");

    const size_t n = bufl.shape[0];  // number of samples (rows)
    const size_t m = bufl.shape[1];  // number of samples (columns)

    if(bufd.shape[0] != n) throw QMML_ERROR("_kernel_matrix_gaussian_d: L and D must have same number of rows");
    if(bufd.shape[1] != m) throw QMML_ERROR("_kernel_matrix_gaussian_d: L and D must have same number of columns")

    if( sigma <= 0 ) throw QMML_ERROR("_kernel_matrix_gaussian_d: hyperparameter sigma must be positive");

    qmml::kernel_matrix_gaussian_d(static_cast<double*>(bufl.ptr), static_cast<double*>(bufd.ptr), n, m, sigma);
}

void _kernel_matrix_gaussian_k(py_cdarray_t kk, py_cdarray_t xx, double sigma)
{
    auto bufk = kk.request(), bufx = xx.request();

    if(bufk.ndim != 2 || bufx.ndim != 2) throw QMML_ERROR("_kernel_matrix_gaussian_k arguments must be matrices");

    const size_t n = bufk.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufk.shape[1] != n) throw QMML_ERROR("_kernel_matrix_gaussian_k: K must be quadratic");
    if(bufx.shape[0] != n) throw QMML_ERROR("_kernel_matrix_gaussian_k: X and K must have same number of rows")

    if( sigma <= 0 ) throw QMML_ERROR("_kernel_matrix_gaussian_k: hyperparameter sigma must be positive");

    qmml::kernel_matrix_gaussian_k(static_cast<double*>(bufk.ptr), static_cast<double*>(bufx.ptr), n, d, sigma);
}

void _kernel_matrix_gaussian_l(py_cdarray_t ll, py_cdarray_t xx, py_cdarray_t zz, double sigma)
{
    auto bufl = ll.request(), bufx = xx.request(), bufz = zz.request();

    if(bufl.ndim != 2 || bufx.ndim != 2 || bufz.ndim != 2) throw QMML_ERROR("_kernel_matrix_gaussian_l arguments must be matrices");

    const size_t n = bufl.shape[0];  // number of training samples
    const size_t m = bufl.shape[1];  // number of test samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufx.shape[0] != n) throw QMML_ERROR("_kernel_matrix_gaussian_l: X and L must have same number of rows");
    if(bufz.shape[0] != m) throw QMML_ERROR("_kernel_matrix_gaussian_l: Z must have as many rows as L has columns");
    if(bufz.shape[1] != d) throw QMML_ERROR("_kernel_matrix_gaussian_l: X and Z must have same number of dimensions");

    if( sigma <= 0 ) throw QMML_ERROR("_kernel_matrix_gaussian_l: hyperparameter sigma must be positive");

    qmml::kernel_matrix_gaussian_l(static_cast<double*>(bufl.ptr), static_cast<double*>(bufx.ptr), static_cast<double*>(bufz.ptr), n, m, d, sigma);
}

void _kernel_matrix_gaussian_m(py_cdarray_t m, py_cdarray_t xx, double sigma)
{
    auto bufm = m.request(), bufx = xx.request();

    if(bufm.ndim != 1 || bufx.ndim != 2) throw QMML_ERROR("_kernel_matrix_linear_m arguments must be vector and matrix");

    const size_t n = bufx.shape[0];  // number of samples
    const size_t d = bufx.shape[1];  // number of dimensions

    if(bufm.shape[0] != n) throw QMML_ERROR("_kernel_matrix_linear_m: m must be vector of compatible dimension");

    if( sigma <= 0 ) throw QMML_ERROR("_kernel_matrix_gaussian_l: hyperparameter sigma must be positive");

    qmml::kernel_matrix_gaussian_m(static_cast<double*>(bufm.ptr), static_cast<double*>(bufx.ptr), n, d, sigma);
}

// Python bindings

void init_kernels_gaussian(py::module &m)
{
    m.def("_kernel_matrix_gaussian_d", &_kernel_matrix_gaussian_d, "Computes Gaussian kernel matrix D", py::arg("L"), py::arg("D"), py::arg("sigma"));
    m.def("_kernel_matrix_gaussian_k", &_kernel_matrix_gaussian_k, "Computes Gaussian kernel matrix K", py::arg("K"), py::arg("X"), py::arg("sigma"));
    m.def("_kernel_matrix_gaussian_l", &_kernel_matrix_gaussian_l, "Computes Gaussian kernel matrix L", py::arg("L"), py::arg("X"), py::arg("Z"), py::arg("sigma"));
    m.def("_kernel_matrix_gaussian_m", &_kernel_matrix_gaussian_m, "Computes Gaussian kernel vector m", py::arg("m"), py::arg("X"), py::arg("sigma"));
}
