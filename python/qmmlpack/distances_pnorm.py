# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Distance functions based on p-norms.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml

#  #######################
#  #  One-norm distance  #
#  #######################

def distance_one_norm(x, z=None):
    """One-norm distance matrix d(x,z) = ||x-z||_1.

    >>> distance_one_norm(x)
    >>> distance_one_norm(x, z)
    """
    return _distance_one_norm_k(x) if z is None else _distance_one_norm_l(x, z)

def _distance_one_norm_k(xx):
    dd = np.empty((len(xx), len(xx)));
    _qmml._distance_matrix_one_norm_k(dd, xx);
    return dd

def _distance_one_norm_l(xx, zz):
    dd = np.empty((len(xx), len(zz)));
    _qmml._distance_matrix_one_norm_l(dd, xx, zz);
    return dd

class DistanceOneNorm:
    """One-norm distance d(x,z) = ||x-z||_1."""

    def __call__(self, x, z=None):
        return distance_one_norm(x, z)


#  ########################
#  #  Euclidean distance  #
#  ########################

def distance_euclidean(x, z=None):
    """Euclidean distance matrix d(x,z) = ||x-z||_2.

    >>> distance_euclidean(x)
    >>> distance_euclidean(x, z)
    """
    return _distance_euclidean_k(x) if z is None else _distance_euclidean_l(x, z)

def _distance_euclidean_k(xx):
    dd = np.empty((len(xx), len(xx)));
    _qmml._distance_matrix_euclidean_k(dd, xx);
    return dd

def _distance_euclidean_l(xx, zz):
    dd = np.empty((len(xx), len(zz)));
    _qmml._distance_matrix_euclidean_l(dd, xx, zz);
    return dd

class DistanceEuclidean:
    """Euclidean distance d(x,z) = ||x-z||_2."""

    def __call__(self, x, z=None):
        return distance_euclidean(x, z)


#  ################################
#  #  Squared Euclidean distance  #
#  ################################

def distance_squared_euclidean(x, z=None):
    """Squared Euclidean distance matrix d(x,z) = ||x-z||_2^2.

    >>> distance_squared_euclidean(x)
    >>> distance_squared_euclidean(x, z)
    """
    return _distance_squared_euclidean_k(x) if z is None else _distance_squared_euclidean_l(x, z)

def _distance_squared_euclidean_k(xx):
    dd = np.empty((len(xx), len(xx)));
    _qmml._distance_matrix_squared_euclidean_k(dd, xx);
    return dd

def _distance_squared_euclidean_l(xx, zz):
    dd = np.empty((len(xx), len(zz)));
    _qmml._distance_matrix_squared_euclidean_l(dd, xx, zz);
    return dd

class DistanceSquaredEuclidean:
    """Squared Euclidean distance d(x,z) = ||x-z||_2^2."""

    def __call__(self, x, z=None):
        return distance_squared_euclidean(x, z)
