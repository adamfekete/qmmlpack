# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Linear kernel.

Part of qmmlpack library."""

import numpy as np

from .lib import QMMLException, _qmml

def kernel_linear(x, z=None, theta=None, diagonal=False):
    """Linear kernel k(x,z) = <x,z>.

    xx and zz are n x d and m x d matrices of input row vectors, respectively.

    >>> kernel_linear(x)
    >>> kernel_linear(x, z)
    >>> kernel_linear(x, diagonal=True)
    """
    if z is None:
        return _kernel_linear_k(x) if diagonal == False else _kernel_linear_m(x)
    else:
        return _kernel_linear_l(x, z)

def _kernel_linear_k(xx):
    # kernel matrix K of samples versus themselves
    # return np.dot(xx, xx.T)
    kk = np.empty((len(xx), len(xx)))
    _qmml._kernel_matrix_linear_k(kk, xx)
    return kk

def _kernel_linear_l(xx, zz):
    # kernel matrix L of samples versus other samples
    # return np.dot(xx, zz.T)
    ll = np.empty((len(xx), len(zz)))
    _qmml._kernel_matrix_linear_l(ll, xx, zz)
    return ll

def _kernel_linear_m(xx):
    # diagonal m of kernel matrix of samples versus themselves
    # return np.einsum('ij,ij->i', xx, xx)
    m = np.empty((len(xx),))
    _qmml._kernel_matrix_linear_m(m, xx)
    return m
