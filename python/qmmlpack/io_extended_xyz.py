# qmmlpack
# (c) Matthias Rupp, 2006-2016.
# See LICENSE.txt for license.

"""Import and export of finite atomistic systems in extended XYZ format.

Part of qmmlpack library.
"""

# 2016-04-14 MR  Fixed bug for non-EOL-terminated files via wrapper to index access

# Current solution parses using C++, then converts result to Python data structure.
# This was done because returning a string/int/float whenever Python accesses a parsing_token is not straightforward
# (might be impossible; overriding __getattribute__ to dynamically adjust parameters to __xxx__ operators might be possible).
# Disadvantage: Overhead due to conversion, needs double the memory.

import numpy as np

from .lib import QMMLException, _qmml

def _is_filename(s):
    """True if s is a filename (as opposed to first line of XYZ format)."""
    i = 0
    while i < len(s) and s[i] != "\n":
        if s[i] not in "0123456789 \t\v\f\r":
            return True
        i += 1
    return i >= len(s)

class ExtXYZData:
    """Container for data of a molecule in extended XYZ format.

    Interface:
    na   -- number of atoms
    an   -- atomic numbers
    xyz  -- coordinates (original units, usually angstrom)
    mp   -- molecular properties
    ap   -- atomic properties
    addp -- additional properties. Only defined if parsed.

    This data structure is just a container for parsing results; it is not a Molecule class of any sort."""

    def __init__(self, an, xyz=None, mp=None, ap=None, addp=None):
        """Converts results of C++ parsing to Python data structure."""
        if(isinstance(an, _qmml._ExtXYZData)):
            # Initialize from C++ object
            raw = an;
            self.na   = len(raw.an)
            self.an   = np.asarray(raw.an)
            self.xyz  = np.asarray(raw.xyz, dtype=np.float_)
            self.mp   = [self.conv_parsing_token(p) for p in raw.mp]
            self.ap   = [[self.conv_parsing_token(p) for p in a] for a in raw.ap]
            self.addp = [[self.conv_parsing_token(p) for p in a] for a in raw.addp]
        else:
            # Initialize from explicit arguments for an, xyz, mp, ap, addp
            self.na   = len(an)
            self.an   = np.asarray(an)
            self.xyz  = np.asarray(xyz, dtype=np.float_)
            self.mp   = [self.conv_arg_token(p) for p in mp]
            self.ap   = [[self.conv_arg_token(p) for p in a] for a in ap]
            self.addp = [] if addp == None else [[self.conv_arg_token(p) for p in a] for a in addp]

    def conv_parsing_token(self, token):
        """Converts a parsing token to string, integer or floating point value."""
        if token.is_int():
            return token.intv()
        elif token.is_float():
            return token.floatv()
        elif token.is_string():
            return token.stringv()
        else: raise QMMLException("Invalid parsing token encountered.")

    def conv_arg_token(self, arg):
        """Converts passed argument to string, integer or single precision floating point value."""
        if isinstance(arg, float): return float(np.float32(arg))
        else: return arg

def import_extxyz(source, additional_properties = False):
    """Imports all atomistic systems from extended XYZ format.

    If source is a filename, input is read from that file.
    If source is a string in extended XYZ format, input is read directly from the string.

    If additional_properties is True, parses additional property lines after each molecule.
    In this case, molecule blocks have to be terminated by an empty line.
    """
    if isinstance(source, str):
        if _is_filename(source):
            res = _qmml._import_extxyz_file(source, additional_properties)
        else:
            s = source.encode("ascii")
            res = _qmml._import_extxyz_string(s, additional_properties)
    elif type(source) == bytes:
        res = _qmml._import_extxyz_string(source, additional_properties)
    else:
        raise QMMLException("source in import_extxyz(source,...) must be either string or bytes.")

    pyres = [ExtXYZData(m) for m in res]
    del res
    return pyres

_element_data_abbrv = {
      1: 'H ',  2: 'He',  3: 'Li',  4: 'Be',  5: 'B ',  6: 'C ',  7: 'N ',
      8: 'O ',  9: 'F ', 10: 'Ne', 11: 'Na', 12: 'Mg', 13: 'Al', 14: 'Si',
     15: 'P ', 16: 'S ', 17: 'Cl', 18: 'Ar', 19: 'K ', 20: 'Ca', 21: 'Sc',
     22: 'Ti', 23: 'V ', 24: 'Cr', 25: 'Mn', 26: 'Fe', 27: 'Co', 28: 'Ni',
     29: 'Cu', 30: 'Zn', 31: 'Ga', 32: 'Ge', 33: 'As', 34: 'Se', 35: 'Br',
     36: 'Kr', 37: 'Rb', 38: 'Sr', 39: 'Y ', 40: 'Zr', 41: 'Nb', 42: 'Mo',
     43: 'Tc', 44: 'Ru', 45: 'Rh', 46: 'Pd', 47: 'Ag', 48: 'Cd', 49: 'In',
     50: 'Sn', 51: 'Sb', 52: 'Te', 53: 'I ', 54: 'Xe', 55: 'Cs', 56: 'Ba',
     57: 'La', 58: 'Ce', 59: 'Pr', 60: 'Nd', 61: 'Pm', 62: 'Sm', 63: 'Eu',
     64: 'Gd', 65: 'Tb', 66: 'Dy', 67: 'Ho', 68: 'Er', 69: 'Tm', 70: 'Yb',
     71: 'Lu', 72: 'Hf', 73: 'Ta', 74: 'W ', 75: 'Re', 76: 'Os', 77: 'Ir',
     78: 'Pt', 79: 'Au', 80: 'Hg', 81: 'Tl', 82: 'Pb', 83: 'Bi', 84: 'Po',
     85: 'At', 86: 'Rn', 87: 'Fr', 88: 'Ra', 89: 'Ac', 90: 'Th', 91: 'Pa',
     92: 'U ', 93: 'Np', 94: 'Pu', 95: 'Am', 96: 'Cm', 97: 'Bk', 98: 'Cf',
     99: 'Es',100: 'Fm',101: 'Md',102: 'No',103: 'Lr',104: 'Rf',105: 'Db',
    106: 'Sg',107: 'Bh',108: 'Hs',109: 'Mt',110: 'Ds',111: 'Rg',112: 'Cn',
    113:'Uut',114: 'Fl',115:'Uup',116: 'Lv',117:'Uus',118:'Uuo'
}

def _format_extxyz(mol):
    """Formats molecule in extended XYZ format.

    Example:
    >>> _format_exyz_entry(ExtXYZData([7, 7], [[0,0,0],[0,0,1.45]], [N2 40.6], None)
    """
    assert isinstance(mol, ExtXYZData)

    def fmt(tok):
        return {str: '{}', int: '{:d}', float: "{: f}"}[type(tok)].format(tok)

    molpropline = ' '.join([fmt(tok) for tok in mol.mp])

    atomblock = ['{} {: f} {: f} {: f}'.format(_element_data_abbrv[mol.an[i]],
        mol.xyz[i][0], mol.xyz[i][1], mol.xyz[i][2]) for i in range(mol.na)]

    if mol.ap != None:
        for i in range(mol.na):
            if mol.ap[i] != []:
                atomblock[i] += '  ' + ' '.join([fmt(tok) for tok in mol.ap[i]])

    addprops = ''
    if mol.addp != None and mol.addp != []:
        addprops += '\n'.join([' '.join([fmt(tok) for tok in line]) for line in mol.addp]) + '\n\n'

    return str(mol.na) + '\n' + molpropline + '\n' + '\n'.join(atomblock) + '\n' + addprops

def export_extxyz(mols, filename = None):
    """Exports atomistic systems to extended XYZ format.

    mols is either a single ExtXYZData object or a list of them.
    If filename is given, export is to text file of given name, otherwise a corresponding string is returned."""

    if isinstance(mols, ExtXYZData): mols = [mols]
    res = ''.join( [_format_extxyz(m) for m in mols] )

    if filename == None:
        return res
    else:
        f = open(filename, "ba")  # append if exists, otherwise create
        f.write(res.encode('ascii'))
        f.close()
