// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Linear kernel

#include "qmmlpack/base.hpp"
#include "qmmlpack/kernels_linear.hpp"

namespace qmml {

//  /////////////////////
//  //  Linear kernel  //
//  /////////////////////

void kernel_matrix_linear_k(double * const K, double const * const X, const size_t n, const size_t d)
{
    // row-major calls can have non-const overhead in some implementations; use equivalent column-major call instead
    // cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, n, d, 1., X, d, X, d, 0, K, n);
    cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, n, d, 1., X, d, X, d, 0, K, n);
}

void kernel_matrix_linear_l(double * const L, double const * const X, double const * const Z, const size_t n, const size_t m, const size_t d)
{
    // row-major calls can have non-const overhead in some implementations; use equivalent column-major call instead
    // cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, m, d, 1., X, d, Z, d, 0., L, m);
    cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m, n, d, 1., Z, d, X, d, 0., L, m);
}

void kernel_matrix_linear_m(double * const m, double const * const X, const size_t n, const size_t d)
{
    for(size_t i = 0; i < n; ++i)
    {
        m[i] = cblas_ddot(d, &X[i*d], 1, &X[i*d], 1);
    }
}

} // namespace qmmlpack
