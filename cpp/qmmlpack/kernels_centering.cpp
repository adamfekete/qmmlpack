// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Centering of kernel matrices

#include "qmmlpack/base.hpp"
#include "qmmlpack/kernels_centering.hpp"
#include "qmmlpack/numerics.hpp"

#include <cstring>

namespace qmml {

//  /////////////////////////////////
//  //  Centering a kernel matrix  //
//  /////////////////////////////////

// - Potential speed-up possible by using a passed temporary buffer?

void center_kernel_matrix_k(double *const Kc, double const*const K, const size_t n)
{
    double * rowavgs = nullptr;

    try
    {
        rowavgs = new double[n];

        // row averages and total average
        double kavg = 0;
        for(size_t i = 0; i < n; ++i)
        {
            double val = 0;
            for(size_t j = 0; j < n; ++j) val += K[i*n+j];
            rowavgs[i] = val/n;
            kavg += val;
        }
        kavg /= n*n;

        // center kernel matrix
        for(size_t i = 0; i < n; ++i)
            for(size_t j = 0; j < n; ++j)
                Kc[i*n+j] = K[i*n+j] + kavg - rowavgs[i] - rowavgs[j];
                // Exploiting symmtry via Kc[j*n+i] = Kc[i*n+j]; is slower (see benchmarks)

        delete[] rowavgs; rowavgs = nullptr;

        // symmetrize matrix
        // necessary because last bits can be non-symmetrical.
        // for example, CenterKernelMatrix[KernelLinear[RandomReal[{-1, 1}, {10, 5}], {}]] yields differences on the order of 10^-16
        symmetrize(Kc, n);
    }
    catch(std::bad_alloc const&)
    {
        delete[] rowavgs;  // delete[] ignores null pointers
    }
}

void center_kernel_matrix_l(double *const Lc, double const*const K, double const*const L, const size_t n, const size_t m)
{
    double * rowavgs = nullptr;
    double * colavgs = nullptr;

    try
    {
        rowavgs = new double[n];

        // row averages and total average of K
        double kavg = 0;
        for(size_t i = 0; i < n; ++i)
        {
            double val = 0;
            for(size_t j = 0; j < n; ++j) val += K[i*n+j];
            rowavgs[i] = val/n;
            kavg += val;
        }
        kavg /= n*n;

        // column averages of L
        colavgs = new double[m];

        for(size_t j = 0; j < m; ++j)
        {
            double val = 0;
            for(size_t i = 0; i < n; ++i) val += L[i*m+j];
            colavgs[j] = val/n;
        }

        // center kernel matrix
        if(Lc != L) std::memcpy(Lc, L, n*m*sizeof(double));
        for(size_t i = 0; i < n; ++i)
            for(size_t j = 0; j < m; ++j)
                Lc[i*m+j] += kavg - rowavgs[i] - colavgs[j];

        delete[] rowavgs; rowavgs = nullptr;
        delete[] colavgs; colavgs = nullptr;
    }
    catch(std::bad_alloc const&)
    {
        delete[] rowavgs;  // delete[] ignores null pointers
        delete[] colavgs;  // delete[] ignores null pointers
    }
}

void center_kernel_matrix_m(double *const Mc, double const*const K, double const*const L, double const*const M, const size_t n, const size_t m)
{
    double * colavgs = nullptr;

    try
    {
        // total average of K
        double kavg = 0;
        for(size_t i = 0; i < n; ++i)
            for(size_t j = 0; j < n; ++j) kavg += K[i*n+j];
        kavg /= n*n;

        // column averages of L
        colavgs = new double[m];

        for(size_t j = 0; j < m; ++j)
        {
            double val = 0;
            for(size_t i = 0; i < n; ++i) val += L[i*m+j];
            colavgs[j] = val/n;
        }

        // center kernel matrix
        if(Mc != M) std::memcpy(Mc, M, m*m*sizeof(double));
        for(size_t i = 0; i < m; ++i)
            for(size_t j = 0; j < m; ++j)
                Mc[i*m+j] += kavg - colavgs[i] - colavgs[j];

        delete[] colavgs; colavgs = nullptr;
    }
    catch(std::bad_alloc const&)
    {
        delete[] colavgs;  // delete[] ignores null pointers
    }
}

void center_kernel_matrix_mdiag(double *const mc, double const*const K, double const*const L, double const*const mv, const size_t n, const size_t m)
{
    double * colavgs = nullptr;

    try
    {
        // total average of K
        double kavg = 0;
        for(size_t i = 0; i < n; ++i)
            for(size_t j = 0; j < n; ++j)
                kavg += K[i*n+j];
        kavg /= n*n;

        // column averages of L
        colavgs = new double[m];

        for(size_t j = 0; j < m; ++j)
        {
            double val = 0;
            for(size_t i = 0; i < n; ++i) val += L[i*m+j];
            colavgs[j] = val/n;
        }

        // center kernel matrix
        if(mc != mv) std::memcpy(mc, mv, m*sizeof(double));
        for(size_t i = 0; i < m; ++i) mc[i] += kavg - 2*colavgs[i];

        delete[] colavgs; colavgs = nullptr;
    }
    catch(std::bad_alloc const&)
    {
        delete[] colavgs;  // delete[] ignores null pointers
    }
}

} // namespace qmml
