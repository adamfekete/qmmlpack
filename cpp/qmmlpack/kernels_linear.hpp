// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Linear kernel

#ifndef QMMLPACK_KERNELS_LINEAR_HPP_INCLUDED  // include guard
#define QMMLPACK_KERNELS_LINEAR_HPP_INCLUDED

namespace qmml {

//  /////////////////////
//  //  Linear kernel  //
//  /////////////////////

// Linear kernel, kernel matrix K
// K  contiguous memory block of size n * n  * size(double)
// X  contiguous memory block of size n * d  * size(double)
void kernel_matrix_linear_k(double * K, double const * X, size_t n, size_t d);

// Linear kernel, kernel matrix L
// L  contiguous memory block of size n * m  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// Z  contiguous memory block of size m * d  * size(T)
void kernel_matrix_linear_l(double * L, double const * X, double const * Z, size_t n, size_t m, size_t d);

// Linear kernel, kernel vector m
// m  contiguous memory block of size n      * size(T)
// X  contiguous memory block of size n * d  * size(T)
void kernel_matrix_linear_m(double * m, double const * X, size_t n, size_t d);

} // namespace qmml

#endif  // include guard
