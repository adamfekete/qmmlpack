// qmmlpack
// (c) Matthias Rupp, 2006-2018.
// See LICENSE.txt for license.

// Many-body tensor representation

#ifndef QMMLPACK_REPRESENTATIONS_MBTR_HPP_INCLUDED  // include guard
#define QMMLPACK_REPRESENTATIONS_MBTR_HPP_INCLUDED

#include <type_traits>
#include <typeinfo>

#include "qmmlpack/numerics.hpp"

namespace qmml {

//  ///////////////////////////////////////
//  //  Many-Body Tensor Representation  //
//  ///////////////////////////////////////

// See Haoyan Huo, Matthias Rupp: Unified representation for machine learning of molecules and crystals, 2017.

// Contents
// --------

// Geometry functions
// Weighting functions
// Distribution functions
// Element correlation functions
// Element indexing functions
// Atom indexing functions
// Many-body tensor representation

// Design
// ------

// * Individual aspects of equation 3 are encapsulated as functions. These are
//   * Geometry functions g
//   * Weighting functions w
//   * Distribution functions d
//   * Correlation functions c
//   * Element indexing functions e
//   * Atom indexing functions a
// * All functions are implemented as functors obeying a common interface for each type of function
// * An alternative to passing functors would have been the curiously recurring template pattern (CRTP).
//   However, this would have increased coupling between code parts
// * Element indexing functions E could have been split into tensor format/storage (dense, row-sparse) and element indexing (full, symmetry-adapted, ...).
//   However, functionality like size of the resulting tensor depend on both. It is therefore implemented as a single class.
// * Geometry functions can be ill-defined for some atom tuples, for example the dihedral angle for a linear chain of atoms.
//   This information needs to be indicated by the return value (exceptions being potentially slow and clunky),
//   either not-a-number or special value. Not-a-number is problematic as ffast-math compiler option breaks both x != x and std::isnan.
//   Special value was therefore chosen.
// * The replica iteration algorithm always assumes activity in the unit cell proper; this is necessary for strong weighting.
//   Example: NaCl, cut-off radius excludes NaCl, but includes NaNa from neighboring cell. If iteration terminates directly
//   after unit cell proper, interactions are missed.


// Geometry functions
// ------------------

// Returns the constant 1.  Available for all k.
struct mbtr_geomf_unity;

// Returns number of atoms in the system that have the same element type.  Only k = 1.
// For periodic systems, only atoms within the unit cell are considered.
template<typename Int> class mbtr_geomf1_count;

// Returns inverse distance 1 / ||R_a - R_b||.  Only k = 2.
struct mbtr_geomf2_one_over_distance;

// Returns squared inverse distance 1 / ||R_a - R_b||^2.  Only k = 2.
struct mbtr_geomf2_one_over_dot;

// Returns angle in radians between R_a - R_b and R_c - R_b.  Only k = 3.
// Has values in the range [0, pi].
struct mbtr_geomf3_angle;

// Returns cos of angle between R_a - R_b and R_c - R_b.  Only k = 3.
// Has values in the range [-1, 1].
struct mbtr_geomf3_cos_angle;

// Returns <u,v>/<u,u><v,v> for u = R_a - R_b and v = R_c - R_b.  Only k = 3.
struct mbtr_geomf3_dot_over_dotdot;

// Returns dihedral angle between four atoms.  Only k = 4.
// Has values in the range [0, pi].
struct mbtr_geomf4_dihedral_angle;

// Returns cos of dihedral angle between four atoms.  Only k = 4.
// Has values in the range [-1, 1].
struct mbtr_geomf4_cos_dihedral_angle;


// Weighting functions
// -------------------

// Returns the constant 1.  Available for all k.
struct mbtr_weightf_unity;

// Returns result x of geometry function.  Available for all k.
struct mbtr_weightf_identity;

// Returns squared result x^2 of geometry function.  Available for all k.
struct mbtr_weightf_identity_squared;

// Return square root sqrt(x) of geometry function.  Available for all k.
struct mbtr_weightf_identity_root;

// Returns inverse 1/x of geometry function.  Available for all k.
struct mbtr_weightf_one_over_identity;

// Returns 1 if 1/x <= cutoff, 0 otherwise.  Available for all k.
class mbtr_weightf_delta_one_over_identity;

// Returns inverse exponentiated result of geometry function, exp( - 1 / x*ls ).  Available for all k.
// Parameterized by length scale ls of exponential.
class mbtr_weightf_exp_neg_one_over_identity;

// Returns inverse exponentiated result of squared geometry function, exp( - 1 / x^2*ls ).  Available for all k.
// Parameterized by length scale ls of exponential.
class mbtr_weightf_exp_neg_one_over_identity_squared;

// Returns 1/c, where c is number of atoms with same element type. Only k = 1.
// For periodic systems, only atoms within the unit cell are considered.
template<typename Int> class mbtr_weightf1_one_over_count;

// Returns 1/pq, where p and q are norms of R_a - R_b and R_c - R_b, respectively. Only k = 3.
struct mbtr_weightf3_one_over_normnorm;

// Returns 1/pq, where p and q are squared norms of R_a - R_b and R_c - R_b, respectively. Only k = 3.
struct mbtr_weightf3_one_over_dotdot;

// Returns 1/pqr, where p, q, r are norms of R_a - R_b, R_c - R_a, R_b - R_c, respectively. Only k = 3.
struct mbtr_weightf3_one_over_normnormnorm;

// Returns 1/pqr, where p, q, r are squared norms of R_a - R_b, R_c - R_a, R_b - R_c, respectively. Only k = 3.
struct mbtr_weightf3_one_over_dotdotdot;

// Returns inverse exponentiated summed distance exp( - pqr / ls ), where p,q,r are norms of R_a - R_b, R_c - R_a, R_b - R_c, respectively. Only k = 3.
class mbtr_weightf3_exp_neg_one_over_normnormnorm;

// Returns inverse exponentiated summed distance exp( - (p+q+r) / ls ), where p,q,r are norms of R_a - R_b, R_c - R_a, R_b - R_c, respectively. Only k = 3.
class mbtr_weightf3_exp_neg_one_over_sum_normnormnorm;


// Distribution functions
// ----------------------

// Normal distribution.
// Parameterized by standard deviation.
// Default is standard deviation close to zero, that is an effective delta function.
class mbtr_normal_distribution;


// Element correlation functions
// -----------------------------

// Element correlation functions are not implemented yet.

class mbtr_identity_correlation;


// Element indexing functions
// --------------------------

struct mbtr_eindexf_dense_full;
struct mbtr_eindexf_dense_noreversals;
// TODO: struct mbtr_eindexf_rowsparse_full;
// TODO: class mbtr_eindexf_rowsparse_noreversals;


// Atom indexing functions
// -----------------------

template<typename Int> struct mbtr_aindexf_finite_full;
template<typename Int> struct mbtr_aindexf_finite_noreversals;
template<typename Int> struct mbtr_aindexf_periodic_full;
template<typename Int> struct mbtr_aindexf_periodic_noreversals;


// MBTR computation
// ----------------

// Computes many-body tensor representation for a dataset of atomistic systems
//
// * for finite systems, basis vectors should be set to nullptr
// * for periodic systems, atom coordinates are Cartesian coordinates which must be within the unit cell given by basis vectors;
// * there are deliberately no defaults for the template parameters; non-experts should use Mathematica or Python bindings
template<
    int K,       // element rank of the tensor (k-body interactions). Currently, k = 1, 2, 3, 4 are supported
    typename G,  // geometry function g
    typename W,  // weighting function w
    typename D,  // distribution function D
    typename C,  // element correlation function C
    typename E,  // element indexing function
    typename A,  // atom indexing function
    typename Int = long  // integer type used for arrays of sizes and proton numbers
>
class many_body_tensor_representation
{
    public:
        many_body_tensor_representation()
        : mbtr_(nullptr), ns_(0), ssizes_(nullptr), sinds_(nullptr), bases_(nullptr), zz_(nullptr), rr_(nullptr), ne_(0), elems_(nullptr), xmin_(0), deltax_(0), xdim_(0), acc_(1e-3)
        {
            // el2in_ will be initialized once ne_ and elems_ are set
        }

        many_body_tensor_representation(
            double *const      mbtr,          // 1   destination memory tensors are written to; contiguous memory of size sum_i nr_i * xdim, where nr_i is number of rows of i-th tensor
            long const         ns,            // 2   number of atomistic systems in dataset; non-negative
            Int const*const    ssizes,        // 3   number of atoms of each system; vector of length ns
            Int const*const    sinds,         // 4   starting index (into bases, zz, rr) of each system; vector of length ns
            double const*const bases,         // 5   linearized unit cell basis vectors of each system; vector of length ns * 3 * 3 containing basis row vectors b1, b2, b3 (b1x, b1y, b1z, b2x, ..., b3z)
            Int const*const    zz,            // 6   linearized atomic numbers of each system; vector of length sum_i na_i, where na_i is number of atoms of i-th system
            double const*const rr,            // 7   linearized atom coordinates (in Angstrom) of each system; vector of length sum_i na_i * 3
            long const         ne,            // 8   total number of element species in dataset
            Int const*const    elems,         // 9   atomic numbers of element species in dataset in sorted order; vector of length ne
            double const       xmin,          // 10  start of discretized spatial axis [xmin, xmin + xdim * deltax]
            double const       deltax,        // 11  bin size of discretized spatial axis; bins are [xmin, xmin+deltax], [xmin+deltax, xmin+2deltax], ...
            long const         xdim,          // 12  number of bins of discretized spatial axis
            G const&           geomf,         // 13  geometry function
            W const&           weightf,       // 14  weighting function
            D const&           distrf,        // 15  probability distribution
            C const&           corrf   = C(), // 16  element correlation function
            E const&           eindexf = E(), // 17  element indexing function
            A const&           aindexf = A(), // 18  atom indexing function
            double const       acc = 1e-3     // 19  absolute value by which computed tensor is allowed to deviate from exact result in each component
        )
        : mbtr_(mbtr), ns_(ns), ssizes_(ssizes), sinds_(sinds), bases_(bases), zz_(zz), rr_(rr), ne_(ne), elems_(elems), xmin_(xmin), deltax_(deltax), xdim_(xdim), geomf_(geomf), weightf_(weightf), distrf_(distrf), corrf_(corrf), eindexf_(eindexf), aindexf_(aindexf), acc_(acc)
        {
            init_el2in();
        }

        many_body_tensor_representation(many_body_tensor_representation const& m)
        : mbtr_(m.mbtr_), ns_(m.ns_), ssizes_(m.ssizes_), sinds_(m.sinds_), bases_(m.bases_), zz_(m.zz_), rr_(m.rr_), ne_(m.ne_), elems_(m.elems_), xmin_(m.xmin_), deltax_(m.deltax_), xdim_(m.xdim_), geomf_(m.geomf_), weightf_(m.weightf_), distrf_(m.distrf_), corrf_(m.corrf_), eindexf_(m.eindexf_), aindexf_(m.aindexf_), acc_(m.acc_)
        {
            for(long i = 0; i < 118; ++i) el2in_[i] = m.el2in_[i];
        }

        // return type of setters allows chaining: mbtr.mbtr(...).ns(...).ssizes(...)...

        // destination memory to which tensors are written
        // contiguous memory of size sum_i nr_i * xdim, where nr_i is number of rows of i-th tensor
        // nr_i is given by the element indexing function
        many_body_tensor_representation& mbtr(double * const mbtr) { mbtr_ = mbtr; return *this; }
        double * mbtr() const { return mbtr_; }

        // number of systems in dataset; non-negative integer
        many_body_tensor_representation& ns(long const ns) { ns_ = ns; return *this; }
        long ns() const { return ns_; }

        // size of systems - vector of ns non-negative integers
        many_body_tensor_representation& ssizes(Int const*const ssizes) { ssizes_ = ssizes; return *this; }
        Int const* ssizes() const { return ssizes_; }

        // indices of systems - vector of ns non-negative integers
        many_body_tensor_representation& sinds(Int const*const sinds) { sinds_ = sinds; return *this; }
        Int const* sinds() const { return sinds_; }

        // unit cell basis vectors - { b11, b12, b13,  b21, b22, b23,  b31, b32, b33 } (three row vectors)
        many_body_tensor_representation& bases(double const*const bases) { bases_ = bases; return *this; }
        double const* bases() const { return bases_; }

        // linearized atomic numbers of systems - ns * #atoms positive integers
        many_body_tensor_representation& zz(Int const* zz) { zz_ = zz; return *this; }
        Int const* zz() const { return zz_; }

        // linearized atomic coordinates of systems - ns * #atoms * 3 real numbers
        many_body_tensor_representation& rr(double const* rr) { rr_ = rr; return *this; }
        double const* rr() const { return rr_; }

        // number of elements in dataset - non-negative integer
        long ne() const { return ne_; }

        // elements - vector of ne positive integers
        many_body_tensor_representation& elems(long const ne, Int const * elems) { ne_ = ne; elems_ = elems; init_el2in(); return *this; }
        Int const* elems() const { return elems_; }

        // start of discretized spatial axis - real number
        many_body_tensor_representation& xmin(double xmin) { xmin_ = xmin; return *this; }
        double xmin() const { return xmin_; }

        // bin size of discretized spatial axis - positive real number
        many_body_tensor_representation& deltax(double deltax) { deltax_ = deltax; return *this; }
        double deltax() const { return deltax_; }

        // number of bins in discretized spatial axis - positive integer
        many_body_tensor_representation& xdim(long xdim) { xdim_ = xdim; return *this; }
        long xdim() const { return xdim_; }

        // geometry function - type G
        many_body_tensor_representation& geomf(G const& geomf) { geomf_ = geomf; return *this; }
        G& geomf() const { return geomf_; }

        // weighting function - type W
        many_body_tensor_representation& weightf(W const& weightf) { weightf_ = weightf; return *this ;}
        W& weightf() const { return weightf_; }

        // distribution function - type D
        many_body_tensor_representation& distrf(D const& distrf) { distrf_ = distrf; return *this; }
        D& distrf() const { return distrf_; }

        // correlation function - type C
        many_body_tensor_representation& corrf(C const & corrf) { corrf_ = corrf; return *this; }
        C& corrf() const { return corrf_; }

        // element indexing function - type E
        many_body_tensor_representation& eindexf(E const& eindexf) { eindexf_ = eindexf; return *this ;}
        E& eindexf() const { return eindexf_; }

        // atom indexing function - type A
        many_body_tensor_representation& aindexf(A const& aindexf) { aindexf_ = aindexf; return *this; }
        A& aindexf() const { return aindexf_; }

        // accuracy goal (maximum error per component) - positive real number
        many_body_tensor_representation& acc(double const acc) { acc_ = acc; return *this; }
        double acc() const { return acc_; }

        // map from atomic number to 0-based consecutive index
        // will be initialized before any calls to any of the xxxf_ functions
        long const * el2in() const { return el2in_; }

        // tensor element rank
        long k() const { return K; }

        // returns total amount of memory (in numbers of double) required for result tensor (mbtr)
        long mbtr_size() const { return eindexf_.size(*this); }

        // throws if some basic checks for parameter consistency fail
        void check();

        // computes many-body tensor representation
        void compute();

    private:
        double * mbtr_;
        long ns_;
        Int const * ssizes_;
        Int const * sinds_;
        double const * bases_;
        Int const * zz_;
        double const * rr_;
        long ne_;
        Int const * elems_;
        double xmin_;
        double deltax_;
        long xdim_;
        G geomf_;
        W weightf_;
        D distrf_;
        C corrf_;
        E eindexf_;
        A aindexf_;
        double acc_;

        long el2in_[118];  // map from element (proton number) to consecutive index (0..nec-1)

        void init_el2in(void)
        {
            for(long i = 0; i < ne_; ++i) el2in_[elems_[i]] = i;
        }

        // updates tensor row at x = mu weighted by scaling factor sf
        double update_tensor(double * dest, double mu, double sf);

        template<int T = K, std::enable_if_t<T == 1,int> = 0> void compute_system(long s);  // computes tensor for system with index s, version for k = 1
        template<int T = K, std::enable_if_t<T == 2,int> = 0> void compute_system(long s);  // computes tensor for system with index s, version for k = 2
        template<int T = K, std::enable_if_t<T == 3,int> = 0> void compute_system(long s);  // computes tensor for system with index s, version for k = 3
        template<int T = K, std::enable_if_t<T == 4,int> = 0> void compute_system(long s);  // computes tensor for system with index s, version for k = 4
};

// Auxiliary functionality
// -----------------------

// fractional to Cartesian coordinates conversion for periodic systems
inline void fractional_to_cartesian(double * dest, long const ns, double const * ssizes, double const * sinds, double const * bases, double const * rr);

} // namespace qmml





//  //////////////////////  //////////////////////  //////////////////////  //////////////////////  //////////////////////  //////////////////////
//  //  IMPLEMENTATION  //  //  IMPLEMENTATION  //  //  IMPLEMENTATION  //  //  IMPLEMENTATION  //  //  IMPLEMENTATION  //  //  IMPLEMENTATION  //
//  //////////////////////  //////////////////////  //////////////////////  //////////////////////  //////////////////////  //////////////////////

#include <limits>
#include <cstring>
#include <cmath>
#include <algorithm>

#include "qmmlpack/base.hpp"

namespace qmml {

// Auxiliary routines
// ------------------

// Helper class for stateless functors
struct mbtr_stateless
{
    template<typename T> void init(T const &) const {}
    void reset(long) const {}
};

// Geometry functions
// ------------------

// Interface:
//   template<typename T> void init(T const &) - initializes geometry function
//   void reset(long) - signals that system with given index starts being processed
//   double operator(long, double const*) - evaluates geometry function for given atomic number z and coordinate r, case k = 1
//   double operator(long, double const*, long, double const*) - evaluates geometry function, case k = 2
//   double operator(long, double const*, long, double const*, long, double const*) - evaluates geometry function, case k = 3
//   double operator(long, double const*, long, double const*, long, double const*, long, double const*) - evaluates geometry function, case k = 4
// Comments:
// * Geometry functions may depend on the whole atomistic system, to which they have access via init() and reset(); however, they may not change it
// * Passed atoms need not be contained in system's atoms as they might be generated on-the-fly, for example for periodic systems

const double mbtr_undefined = std::numeric_limits<double>::lowest();  // this choice is arbitrary and may change without notice

struct mbtr_geomf_unity : public mbtr_stateless
{
    double operator()(...) { return 1; }
};

template<typename Int>
class mbtr_geomf1_count
{
    public:
        mbtr_geomf1_count() : ssizes_(nullptr), sinds_(nullptr), zz_(nullptr) {}

        template<typename T> void init(T const & mbtr)
        {
            ssizes_ = mbtr.ssizes();
            sinds_  = mbtr.sinds();
            zz_     = mbtr.zz();
        }

        void reset(long const s)
        {
            std::memset(elemcount_, 0, 118*sizeof(elemcount_[0]));
            for(long i = 0; i < ssizes_[s]; ++i) ++elemcount_[zz_[sinds_[s]+i]];
        }

        double operator()(long const za, double const *) const
        {
            return elemcount_[za];
        }

    private:
        Int const * ssizes_;
        Int const * sinds_;
        Int const * zz_;
        long elemcount_[118];
};

struct mbtr_geomf2_one_over_distance : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3]) const
    {
        // 1 / sqrt(<ra-rb,ra-rb>)
        return 1. / std::sqrt
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );
    }
};

struct mbtr_geomf2_one_over_dot : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3]) const
    {
        // 1 / <ra-rb,ra-rb>
        return 1. /
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );
    }
};

struct mbtr_geomf3_angle : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3]) const
    {
        const double dotuv =
        (
            (ra[0] - rb[0]) * (rc[0] - rb[0]) +
            (ra[1] - rb[1]) * (rc[1] - rb[1]) +
            (ra[2] - rb[2]) * (rc[2] - rb[2])
        );

        const double dotuu =  // u = R_a - R_b
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );

        const double dotvv =  // v = R_c - R_b
        (
            (rc[0] - rb[0]) * (rc[0] - rb[0]) +
            (rc[1] - rb[1]) * (rc[1] - rb[1]) +
            (rc[2] - rb[2]) * (rc[2] - rb[2])
        );

        // without the clamp, the result could be outside of [-1,1] by ~2e-16, causing acos to return NaN
        return std::acos( clamp( dotuv / std::sqrt( dotuu * dotvv ), -1., 1.) );
    }
};

struct mbtr_geomf3_cos_angle : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3]) const
    {
        const double dotuv =
        (
            (ra[0] - rb[0]) * (rc[0] - rb[0]) +
            (ra[1] - rb[1]) * (rc[1] - rb[1]) +
            (ra[2] - rb[2]) * (rc[2] - rb[2])
        );

        const double dotuu =  // u = R_a - R_b
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );

        const double dotvv =  // v = R_c - R_b
        (
            (rc[0] - rb[0]) * (rc[0] - rb[0]) +
            (rc[1] - rb[1]) * (rc[1] - rb[1]) +
            (rc[2] - rb[2]) * (rc[2] - rb[2])
        );

        // without the clamp, the result could be outside of [-1,1] by ~2e-16, causing problems later on
        return clamp( dotuv / std::sqrt( dotuu * dotvv ), -1., 1. );  //  [-1,1]
    }
};

struct mbtr_geomf3_dot_over_dotdot : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3]) const
    {
        const double dotuv =
        (
            (ra[0] - rb[0]) * (rc[0] - rb[0]) +
            (ra[1] - rb[1]) * (rc[1] - rb[1]) +
            (ra[2] - rb[2]) * (rc[2] - rb[2])
        );

        const double dotuu =  // u = R_a - R_b
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );

        const double dotvv =  // v = R_c - R_b
        (
            (rc[0] - rb[0]) * (rc[0] - rb[0]) +
            (rc[1] - rb[1]) * (rc[1] - rb[1]) +
            (rc[2] - rb[2]) * (rc[2] - rb[2])
        );

        return dotuv / ( dotuu * dotvv );
    }
};

struct mbtr_geomf4_dihedral_angle : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3], long const, double const rd[3]) const
    {
        const double u[3] =  // Cross[r[[a]]-r[[b]], r[[c]]-r[[b]]]  // verified 2017-05-28 MR
        {
            -(ra[2] - rb[2]) * (rc[1] - rb[1]) + (ra[1] - rb[1]) * (rc[2] - rb[2]),
            +(ra[2] - rb[2]) * (rc[0] - rb[0]) - (ra[0] - rb[0]) * (rc[2] - rb[2]),
            -(ra[1] - rb[1]) * (rc[0] - rb[0]) + (ra[0] - rb[0]) * (rc[1] - rb[1])
        };

        const double v[3] =  // Cross[r[[d]] - r[[c]], r[[b]] - r[[c]]]  // verified 2017-05-28 MR
        {
            - (rd[2] - rc[2]) * (rb[1] - rc[1]) + (rd[1] - rc[1]) * (rb[2] - rc[2]),
            + (rd[2] - rc[2]) * (rb[0] - rc[0]) - (rd[0] - rc[0]) * (rb[2] - rc[2]),
            - (rd[1] - rc[1]) * (rb[0] - rc[0]) + (rd[0] - rc[0]) * (rb[1] - rc[1])
        };

        const double x = // x = <u,u> * <v,v>  can be 0 if ABC or BCD are collinear (lie on a line)
            ( u[0] * u[0] + u[1] * u[1] + u[2] * u[2] ) * ( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] );

        const double dotuv = u[0] * v[0] + u[1] * v[1] + u[2] * v[2] ; // <u,v>

        // if either u or v is zero (due to ABC or BCD being collinear), then <u,v>/|u||v| is 0/0, and thus undefined
        return std::abs(x) < 1e-10 ? mbtr_undefined : std::acos( - dotuv / std::sqrt(x) );
    }
};

struct mbtr_geomf4_cos_dihedral_angle : public mbtr_stateless
{
    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3], long const, double const rd[3]) const
    {
        const double u[3] =  // Cross[r[[a]]-r[[b]], r[[c]]-r[[b]]]
        {
            -(ra[2] - rb[2]) * (rc[1] - rb[1]) + (ra[1] - rb[1]) * (rc[2] - rb[2]),
            +(ra[2] - rb[2]) * (rc[0] - rb[0]) - (ra[0] - rb[0]) * (rc[2] - rb[2]),
            -(ra[1] - rb[1]) * (rc[0] - rb[0]) + (ra[0] - rb[0]) * (rc[1] - rb[1])
        };

        const double v[3] =  // Cross[r[[d]] - r[[c]], r[[b]] - r[[c]]]
        {
            - (rd[2] - rc[2]) * (rb[1] - rc[1]) + (rd[1] - rc[1]) * (rb[2] - rc[2]),
            + (rd[2] - rc[2]) * (rb[0] - rc[0]) - (rd[0] - rc[0]) * (rb[2] - rc[2]),
            - (rd[1] - rc[1]) * (rb[0] - rc[0]) + (rd[0] - rc[0]) * (rb[1] - rc[1])
        };

        const double x = // x = <u,u> * <v,v>  can be 0 if all points lie on a line
            ( u[0] * u[0] + u[1] * u[1] + u[2] * u[2] ) * ( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] );

        const double dotuv = u[0] * v[0] + u[1] * v[1] + u[2] * v[2] ; // <u, v>

        // if either u or v is zero (due to ABC or BCD being collinear), then <u,v>/<u,u><v,v> is 0/0, and thus undefined
        return std::abs(x) < 1e-10 ? mbtr_undefined : - dotuv / std::sqrt(x);
    }
};


// Weighting functions
// -------------------

// Interface:
// Same as geometry function, except for a double before the atom data, the result of the geometry function

struct mbtr_weightf_unity : public mbtr_stateless
{
    public: double operator()(...) const { return 1; }
};

struct mbtr_weightf_identity : public mbtr_stateless
{
    public: double operator()(double const x, ...) const { return x; }
};

struct mbtr_weightf_identity_squared : public mbtr_stateless
{
    public: double operator()(double const x, ...) const { return x * x; }
};

struct mbtr_weightf_identity_root : public mbtr_stateless
{
    public: double operator()(double const x, ...) const { return std::sqrt(x); }
};

struct mbtr_weightf_one_over_identity : public mbtr_stateless
{
    public: double operator()(double const x, ...) const { return 1 / x; }
};

class mbtr_weightf_delta_one_over_identity
{
    public:
        mbtr_weightf_delta_one_over_identity(double const cutoff) : cutoff_(cutoff) { }

        template<typename T> void init(T const &) const {}

        void reset(long const) const {}

        double operator()(double const x, ...) const
        {
            return 1/x <= cutoff_ ? 1 : 0;
        }

    private:
        double const cutoff_;  // positive cut-off
};

class mbtr_weightf_exp_neg_one_over_identity
{
    public:
        mbtr_weightf_exp_neg_one_over_identity(double const ls) : ls_(ls) {}

        template<typename T> void init(T const &) const {}

        void reset(long const) const {}

        double operator()(double const x, ...) const
        {
            return std::exp( - 1. / ( x * ls_ ) );
        }

    private:
        const double ls_;  // length scale of exponential function
};

class mbtr_weightf_exp_neg_one_over_identity_squared
{
    public:
        mbtr_weightf_exp_neg_one_over_identity_squared(double const ls) : ls_(ls) {}

        template<typename T> void init(T const &) const {}

        void reset(long const) const {}

        double operator()(double const x, ...) const
        {
            return std::exp( - 1. / ( x * x * ls_ ) );
        }

    private:
        const double ls_;  // length scale of exponential function
};

template<typename Int>
class mbtr_weightf1_one_over_count
{
    public:
        mbtr_weightf1_one_over_count() : ssizes_(nullptr), sinds_(nullptr), zz_(nullptr) {}

        template<typename T> void init(T const & mbtr)
        {
            ssizes_ = mbtr.ssizes();
            sinds_  = mbtr.sinds();
            zz_     = mbtr.zz();
        }

        void reset(long const s)
        {
            std::memset(elemcount_, 0, 118*sizeof(elemcount_[0]));
            for(long i = 0; i < ssizes_[s]; ++i) ++elemcount_[zz_[sinds_[s]+i]];
        }

        double operator()(double const, long const za, double const *) const
        {
            return 1. / elemcount_[za];
        }

    private:
        Int const * ssizes_;
        Int const * sinds_;
        Int const * zz_;
        long elemcount_[118];
};

struct mbtr_weightf3_one_over_normnorm : public mbtr_stateless
{
    public:
        double operator()(double const x, long const, double const * ra, long const, double const * rb, long const, double const * rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - b
            (
                (rc[0] - rb[0]) * (rc[0] - rb[0]) +
                (rc[1] - rb[1]) * (rc[1] - rb[1]) +
                (rc[2] - rb[2]) * (rc[2] - rb[2])
            );

            return 1. / std::sqrt( dotuu * dotvv );
        }
};

struct mbtr_weightf3_one_over_dotdot : public mbtr_stateless
{
    public:
        double operator()(double const x, long const, double const * ra, long const, double const * rb, long const, double const * rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - b
            (
                (rc[0] - rb[0]) * (rc[0] - rb[0]) +
                (rc[1] - rb[1]) * (rc[1] - rb[1]) +
                (rc[2] - rb[2]) * (rc[2] - rb[2])
            );

            return 1. / ( dotuu * dotvv );
        }
};

struct mbtr_weightf3_one_over_normnormnorm : public mbtr_stateless
{
    public:
        double operator()(double const x, long const, double const * ra, long const, double const * rb, long const, double const * rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - a
            (
                (rc[0] - ra[0]) * (rc[0] - ra[0]) +
                (rc[1] - ra[1]) * (rc[1] - ra[1]) +
                (rc[2] - ra[2]) * (rc[2] - ra[2])
            );

            const double dotww =  // v = b - c
            (
                (rb[0] - rc[0]) * (rb[0] - rc[0]) +
                (rb[1] - rc[1]) * (rb[1] - rc[1]) +
                (rb[2] - rc[2]) * (rb[2] - rc[2])
            );

            return 1. / std::sqrt( dotuu * dotvv * dotww );
        }
};

struct mbtr_weightf3_one_over_dotdotdot : public mbtr_stateless
{
    public:
        double operator()(double const x, long const, double const * ra, long const, double const * rb, long const, double const * rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - a
            (
                (rc[0] - ra[0]) * (rc[0] - ra[0]) +
                (rc[1] - ra[1]) * (rc[1] - ra[1]) +
                (rc[2] - ra[2]) * (rc[2] - ra[2])
            );

            const double dotww =  // v = b - c
            (
                (rb[0] - rc[0]) * (rb[0] - rc[0]) +
                (rb[1] - rc[1]) * (rb[1] - rc[1]) +
                (rb[2] - rc[2]) * (rb[2] - rc[2])
            );

            return 1. / ( dotuu * dotvv * dotww );
        }
};

class mbtr_weightf3_exp_neg_one_over_normnormnorm
{
    public:
        mbtr_weightf3_exp_neg_one_over_normnormnorm(double const ls) : ls_(ls) {}

        template<typename T> void init(T const &) const {}

        void reset(long const) const {}

        double operator()(double const, long const, double const * const ra, long const, double const * const rb, long const, double const * const rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - a
            (
                (rc[0] - ra[0]) * (rc[0] - ra[0]) +
                (rc[1] - ra[1]) * (rc[1] - ra[1]) +
                (rc[2] - ra[2]) * (rc[2] - ra[2])
            );

            const double dotww =  // v = b - c
            (
                (rb[0] - rc[0]) * (rb[0] - rc[0]) +
                (rb[1] - rc[1]) * (rb[1] - rc[1]) +
                (rb[2] - rc[2]) * (rb[2] - rc[2])
            );

            const double prod = std::sqrt(dotuu * dotvv * dotww);

            return std::exp( - prod / ls_ );
        }

    private:
        const double ls_;  // length scale of exponential function
};

class mbtr_weightf3_exp_neg_one_over_sum_normnormnorm
{
    public:
        mbtr_weightf3_exp_neg_one_over_sum_normnormnorm(double const ls) : ls_(ls) {}

        template<typename T> void init(T const &) const {}

        void reset(long const) const {}

        double operator()(double const, long const, double const * const ra, long const, double const * const rb, long const, double const * const rc) const
        {
            const double dotuu =  // u = a - b
            (
                (ra[0] - rb[0]) * (ra[0] - rb[0]) +
                (ra[1] - rb[1]) * (ra[1] - rb[1]) +
                (ra[2] - rb[2]) * (ra[2] - rb[2])
            );

            const double dotvv =  // v = c - a
            (
                (rc[0] - ra[0]) * (rc[0] - ra[0]) +
                (rc[1] - ra[1]) * (rc[1] - ra[1]) +
                (rc[2] - ra[2]) * (rc[2] - ra[2])
            );

            const double dotww =  // v = b - c
            (
                (rb[0] - rc[0]) * (rb[0] - rc[0]) +
                (rb[1] - rc[1]) * (rb[1] - rc[1]) +
                (rb[2] - rc[2]) * (rb[2] - rc[2])
            );

            const double sum = std::sqrt(dotuu) + std::sqrt(dotvv) + std::sqrt(dotww);

            return std::exp( - sum / ls_ );
        }

    private:
        const double ls_;  // length scale of exponential function
};

// Distribution functions
// ----------------------

// Interface:
//   template<T> void init(T const &) - initializes distribution function
//   void reset(double) - signals to distribution function request for new placement at mean mu
//   double range_from() - returns startpoint of range where distribution is non-zero (in units of spatial axis)
//   double range_to() - returns endpoint of range where distribution is non-zero (in units of spatial axis)
//   double cdf(double const x) - returns cumulative distribution function at x
// Comments:
// * reset will be called before range_from, range_to and cdf
// * must satisfy range_from <= range_to

// Potential optimizations include faster computation of the error function / cumulative distribution function (approximations, look-up tables)
// West's algorithm for normal distribution CDF: http://stackoverflow.com/questions/2328258/cumulative-normal-distribution-function-in-c-c
class mbtr_normal_distribution
{
    public:
        mbtr_normal_distribution() : mu_(0), stddev_(1e-10) {}

        mbtr_normal_distribution(double const stddev) : mu_(0), stddev_(stddev)
        {
            if(stddev <= 0.) throw QMML_ERROR("Normal distribution standard deviation must be positive."); // tiny standard deviations are okay
        }

        template<typename T> void init(T const &) const {}

        void reset(double const mu)
        {
            mu_ = mu;
        }

        double range_from() const
        {
            return mu_ - 6*stddev_;  // 6 sigma yields 10^-10 differences to exact solution for 200 bins
        }

        double range_to() const
        {
            return mu_ + 6*stddev_;
        }

        // CDF(z) = 1/2 ( 1 + erf( (z-mu) / (sqrt(2)sigma) ) )
        double cdf(double const x) const
        {
            static constexpr double sqrt2 = 1.4142135623730951;  // square root of 2
            return ( 1. + std::erf( ( x - mu_ ) / ( stddev_ * sqrt2 ) ) ) / 2.;
        }

    private:
        double mu_;  // mean; result from geometry function
        const double stddev_;  // standard deviation sigma of normal distribution
};


// Element correlation functions
// -----------------------------

// Element correlation functions are not implemented yet.

class mbtr_identity_correlation : public mbtr_stateless
{
};


// Element indexing functions
// --------------------------

// Interface:
//   template<T> void init(T const & mbtr) - initializes indexing function
//   void reset(long) - signals to the indexing function that system with given index starts being computed
//   double * dest_addr(long i) - returns memory address of result tensor for system with index i
//   template<T> long size(T const &) - returns total size of result tensors for all systems in dataset (in numbers of doubles) for given MBTR
//   long operator()(long) - returns row index for given element type(s), case k = 1
//   long operator()(long, long) - returns row index for given element type(s), case k = 2
//   long operator()(long, long, long) - returns row index for given element type(s), case k = 3
//   long operator()(long, long, long, long) - returns row index for given element type(s), case k = 4
// Comments:
// * element indexing functions handle memory layout and output format of the tensor
// * init is called once before the computation; reset is called before each system

// Element indexing base class
//
// Code factored out into a base class for "default-style" element indexing functions where tensor has same size for all systems (dense tensor storage format).
// Uses curiously recurring template pattern to statically access derived class's functions.
template<typename Derived> class mbtr_eindexf_dense_base
{
    public:
        mbtr_eindexf_dense_base() : mbtr_(nullptr), ne_(0), el2in_(nullptr), tensor_size_(0) {}

        template<typename T> void init(T const & mbtr)
        {
            mbtr_  = mbtr.mbtr();
            ne_    = mbtr.ne();
            el2in_ = mbtr.el2in();

            // for dense tensors, tensor size is the same for each system
            tensor_size_ = static_cast<Derived *>(this)->size(mbtr) / mbtr.ns();
        }

        // indexing scheme does not depend on system
        void reset(long const) const {}

        double * dest_addr(long const i) const
        {
            return mbtr_ + i * tensor_size_;
        }

        // k = 1
        long operator()(long const za) const
        {
            const long i1 = el2in_[za];

            return static_cast<Derived const *>(this)->index(i1);
        }

        // k = 2
        long operator()(long const za, long const zb)
        {
            const long i1 = el2in_[za];
            const long i2 = el2in_[zb];

            return static_cast<Derived const *>(this)->index(i1, i2);
        }

        // k = 3
        long operator()(long const za, long const zb, long const zc)
        {
            const long i1 = el2in_[za];
            const long i2 = el2in_[zb];
            const long i3 = el2in_[zc];

            return static_cast<Derived const *>(this)->index(i1, i2, i3);
        }

        // k = 4
        long operator()(long const za, long const zb, long const zc, long const zd)
        {
            const long i1 = el2in_[za];
            const long i2 = el2in_[zb];
            const long i3 = el2in_[zc];
            const long i4 = el2in_[zd];

            return static_cast<Derived const *>(this)->index(i1, i2, i3, i4);
        }

    protected:
        double * mbtr_;       // result tensor memory
        long ne_;             // number of elements in element to index map el2in_
        long const * el2in_;  // translation table from atomic number to consecutive 0-based index
        long tensor_size_;    // size (in terms of doubles) of one tensor
};

// Full element indexing for dense result tensor
//
// Each element combination yields one row in the (linearized) result tensor, e.g., HH, HC, CH, CC.
// This is a literal translation of MBTR without any symmetry considerations.
// For many geometry functions, a symmetry-adapted element indexing function can be used to reduce memory and compute time.
struct mbtr_eindexf_dense_full : public mbtr_eindexf_dense_base<mbtr_eindexf_dense_full>
{
    // required memory (in doubles) for whole dataset
    template<typename T> long size(T const & mbtr) const
    {
        return static_cast<long>( std::pow(mbtr.ne(), mbtr.k()) ) * mbtr.xdim() * mbtr.ns();  // ne^k * dim
    }

    // k = 1, i1
    long index(long const i1) const
    {
        return i1;
    }

    // k = 2, i1 * ne + i2
    long index(long const i1, long const i2) const
    {
        return i1 * ne_ + i2;
    }

    // k = 3, i1 * ne^2 + i2 * ne + i3
    long index(long const i1, long const i2, long const i3) const
    {
        // return i1 * ne_ * ne_ + i2 * ne_ + i3;
        return ( i1 * ne_ + i2 ) * ne_ + i3;  // 4 operations instead of 5
    }

    // k = 4, i1 * ne^3 + i2 * ne^2 + i3 * ne + i4
    long index(long const i1, long const i2, long const i3, long const i4) const
    {
        // return i1 * ne_ * ne_ * ne_ + i2 * ne_ * ne_ + i3 * ne_ + i4;
        return ( ( i1 * ne_ + i2 ) * ne_ + i3 ) * ne_ + i4;  // 6 operations instead of 9
    }
};

// No-reversals element indexing for dense result tensor
//
// Element combinations and their reversals are mapped to the same tensor row, e.g., HCO and OCH are mapped to the same row.
// The number of element combinations (nec) is fixed, and all tensors have the same size.
struct mbtr_eindexf_dense_noreversals : public mbtr_eindexf_dense_base<mbtr_eindexf_dense_noreversals>
{
    // required memory (in doubles) for whole dataset
    template<typename T> long size(T const & mbtr) const
    {
        // ( n^k + n^ceil(k/2) ) / 2 ) * xdim * ns  (number of element combinations times number of bins times number of systems)
        return static_cast<long>( 0.5 * ( std::pow(mbtr.ne(), mbtr.k()) + std::pow(mbtr.ne(), std::ceil(0.5*mbtr.k())) ) ) * mbtr.xdim() * mbtr.ns();
    }

    // k = 1
    long index(const long i1) const
    {
        return i1;
    }

    // k = 2
    long index(const long i1, const long i2) const
    {
        return i1 <= i2 ?
               ( i1 * ( 2*ne_ - i1 - 1 ) ) / 2 + i2 :
               ( i2 * ( 2*ne_ - i2 - 1 ) ) / 2 + i1 ;
    }

    // k = 3
    long index(const long i1, const long i2, const long i3) const
    {
        return i1 <= i3 ?
               ( i1 * ( ne_ * ( 2*ne_ - i1 + 1 ) - 2 * (i2 + 1) ) ) / 2 + i3 + i2 * ne_ :  // verified 2017-05-05
               ( i3 * ( ne_ * ( 2*ne_ - i3 + 1 ) - 2 * (i2 + 1) ) ) / 2 + i1 + i2 * ne_ ;
    }

    // k = 4
    long index(const long i1, const long i2, const long i3, const long i4) const
    {
        return
            i1 <  i4 ? ( -( (i1*ne_ + i2) * ( (i1-2*ne_)*ne_ + i2 - 1 ) )/2 + i4 + i3 * (ne_ - i1 - 1) - i1 - 1 + std::max(i3-i2+1, 0L) ) : (
            i1 >  i4 ? ( -( (i4*ne_ + i2) * ( (i4-2*ne_)*ne_ + i2 - 1 ) )/2 + i1 + i3 * (ne_ - i4 - 1) - i4 - 1 + std::max(i3-i2+1, 0L) ) : ( // < i1 == i4
            i2 <= i3 ? ( -( (i1*ne_ + i2) * ( (i1-2*ne_)*ne_ + i2 - 1 ) )/2 + i4 + i3 * (ne_ - i1 - 1) - i1 - 1 + std::max(i3-i2+1, 0L) ) :
                       ( -( (i1*ne_ + i3) * ( (i1-2*ne_)*ne_ + i3 - 1 ) )/2 + i4 + i2 * (ne_ - i1 - 1) - i1 - 1 + std::max(i2-i3+1, 0L) )  // i2 > i3
            ));
    }
};


// Atom indexing functions
// -----------------------

// Interface:
//   template<T> void init(T const & mbtr) - initializes indexing function
//   void reset(long) - signals to the indexing function that system with index s starts being computed
//   static constexpr bool is_periodic - true if indexing function works with periodic systems
//   explicit operator bool() - true if iteration should continue, false if iteration has terminated
//   void operator()(long & za, double ra[3]) - sets atomic number and coordinate of atom a (k = 1)
//   void operator()(long & za, double ra[3], long & zb, double rb[3]) - sets atomic number and coordinate of atoms a, b (k = 2)
//   void operator()(long & za, double ra[3], long & zb, double rb[3], long & zc, double rc[3]) - sets atomic number and coordinate of atoms a, b, c (k = 3)
//   void operator()(long & za, double ra[3], long & zb, double rb[3], long & zc, double rc[3], long & zd, double rd[3]) - sets atomic number and coordinate of atoms a, b, c, d (k = 4)
//   void step(bool) - one iteration step; parameter indicates whether tensor changed for current index (before increasing it)
// Comments:
// * operator() returns current index tuple, step() steps to next tuple
// * atom indexing functions handle iteration over k-tuples of atoms
// * init is called once before the computation; reset is called before each system
// * reset terminates current loop and starts the next one
// * atoms returned by operator() need not be elements of system's specified atoms

// Traits class helps passing types around in conjunction with Curiously Recurring Template Pattern.
// See https://stackoverflow.com/questions/6006614/c-static-polymorphism-crtp-and-using-typedefs-from-derived-classes
template<typename Int> struct mbtr_aindexf_base_traits;

// Atom indexing base class for finite systems
//
// Code factored out into a base class for "default-style" atom iterators for finite systems.
// Uses curiously recurring template pattern to statically access derived class's increment() functions,
// which increment iteration indices a[, b[, c[, d]]] (depending on k) by one.
// This base class is best suited for iteration over a system's given atoms (as opposed to atoms from system replicas).
template<typename Derived> class mbtr_aindexf_finite_base
{
    public:
        static constexpr bool is_periodic = false;

        typedef typename mbtr_aindexf_base_traits<Derived>::Int Int;

        mbtr_aindexf_finite_base() : ssizes_(nullptr), sinds_(nullptr), zz_(nullptr), rr_(nullptr) {}

        template<typename T> void init(T const & mbtr)
        {
            ssizes_ = mbtr.ssizes();
            sinds_ = mbtr.sinds();
            zz_ = mbtr.zz();
            rr_ = mbtr.rr();
            k_ = mbtr.k();
        }

        void reset(long const s)
        {
            s_ = s;
            na_ = ssizes_[s];
            done_ = ( ssizes_[s] < k_ );  // special cases of empty system and insufficient atoms for unique k-tuple
            a_ = 0; b_ = 1; c_ = 2; d_ = 3;  // unique tuples starting value
        }

        explicit operator bool() const { return !done_; }

        void operator()(Int & za, double ra[3])
        {
            const Int ind = sinds_[s_];
            update(za, ra, ind, a_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3])
        {
            const Int ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3], Int & zc, double rc[3])
        {
            const Int ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_);
            update(zc, rc, ind, c_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3], Int & zc, double rc[3], Int & zd, double rd[3])
        {
            const Int ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_);
            update(zc, rc, ind, c_);
            update(zd, rd, ind, d_);
        }

        void step(bool const update)
        {
            switch(k_)
            {
                case 1: static_cast<Derived *>(this)->increment1(); break;
                case 2: static_cast<Derived *>(this)->increment2(); break;
                case 3: static_cast<Derived *>(this)->increment3(); break;
                case 4: static_cast<Derived *>(this)->increment4(); break;
            }
            termination_condition();
        }

    protected:
        Int const * ssizes_;  // number of atoms of each system
        Int const * sinds_;   // index into linearized zz and rr of each system
        Int const * zz_;      // linearized atomic numbers of all systems
        double const * rr_;    // linearized atom coordinates of all systems
        long k_;               // element rank of tensor

        long s_;               // index of system currently iterated over
        Int na_;              // number of atoms of current system
        bool done_;            // true if iteration for current system has finished
        Int a_, b_, c_, d_;   // iteration indices

        // helper setting z and r for atom i in system with index ind
        void update(Int & z, double r[3], Int const ind, long const i) const
        {
            z = zz_[ind + i];
            r[0] = rr_[(ind + i)*3 + 0];
            r[1] = rr_[(ind + i)*3 + 1];
            r[2] = rr_[(ind + i)*3 + 2];
        }

        void termination_condition()
        {
            if( a_ == na_ ) done_ = true;
        }
};

// Full indexing for finite systems
//
// Iterates over all tuples of k unique atoms. For example, for 3 atoms and k = 2: 01, 02, 10, 12, 20, 21.
// This is a literal translation of MBTR without any symmetry corrections.
// For many geometry functions, a symmetry-adapted atom indexing function might be used to reduce compute time.
template<typename Int>
struct mbtr_aindexf_finite_full : public mbtr_aindexf_finite_base<mbtr_aindexf_finite_full<Int>>
{
    typedef typename mbtr_aindexf_base_traits<mbtr_aindexf_finite_full>::Base Base;

    void increment1()
    {
        Int & a_ = Base::a_;

        ++a_;
    }

    void increment2()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & na_ = Base::na_;

        ++b_; if( b_ == a_ ) ++b_;
        if( b_ == na_ )
        {
            ++a_; b_ = 0;
        }
    }

    void increment3()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & na_ = Base::na_;

        ++c_; while( c_ == a_ || c_ == b_ ) ++c_;
        if( c_ == na_ )
        {
            ++b_; if( b_ == a_ ) ++b_;
            if( b_ < na_ )
            {
                c_ = -1; increment3();
            }
            else  // b == na
            {
                ++a_;
                if( a_ != na_ )
                {
                    b_ = 0; c_ = 0; increment3();
                }
            }
        }
    }

    void increment4()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & d_ = Base::d_;
        Int & na_ = Base::na_;

        ++d_; while( d_ == a_ || d_ == b_ || d_ == c_ ) ++d_;
        if( d_ == na_ )
        {
            ++c_; while( c_ == a_ || c_ == b_ ) ++c_;
            if( c_ < na_ )
            {
                d_ = -1; increment4();
            }
            else  // c == na
            {
                ++b_; if( b_ == a_ ) ++b_;
                if( b_ < na_ )
                {
                    c_ = 0; while( c_ == a_ || c_ == b_ ) ++c_;  // at least two other choices than a or b are possible
                    d_ = -1; increment4();
                }
                else  // b == na
                {
                  ++a_;
                  if( a_ != na_ )
                  {
                      b_ = 0;
                      c_ = 1; while( c_ == a_ ) ++c_;
                      d_ = 1; increment4();
                  }
                }
            }
        }
    }
};

// Specialization of traits class
template<typename Int_>
struct mbtr_aindexf_base_traits<mbtr_aindexf_finite_full<Int_>>
{
    typedef mbtr_aindexf_finite_full<Int_> Base;
    typedef Int_ Int;
};

// No-reversals indexing for finite systems
//
// Iterates over all tuples of k unique atoms up to reversals.
// For example, for 3 atoms and k = 2: 01, 02, 12.
// This atom indexing scheme omits reversed atom tuples; e.g., for k = 4, abcd will be iterated over, but dcba will not.
// Geometry functions for which this is appropriate include (inverse) distances, angles, and dihedral angles.
template<typename Int>
struct mbtr_aindexf_finite_noreversals : public mbtr_aindexf_finite_base<mbtr_aindexf_finite_noreversals<Int>>
{
    typedef typename mbtr_aindexf_base_traits<mbtr_aindexf_finite_noreversals>::Base Base;

    void increment1()
    {
        Int & a_ = Base::a_;

        ++a_;
    }

    void increment2()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & na_ = Base::na_;

        ++b_;
        if( b_ == na_ )
        {
            ++a_;
            if( a_ != na_ )
            {
                b_ = a_; increment2();
            }
        }

        // The loop version of the iteration logic:
        // for(long a = 0; a < na; ++a)
        //     for(long b = a + 1; b < na; ++b)
        //         ...
    }

    void increment3()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & na_ = Base::na_;

        ++c_; if( c_ == b_ ) ++c_;  // increment c, maintaining c != b
        if( c_ == na_ )  // if b == na-1, c is still na
        {
            ++b_; if( b_ == a_ ) ++b_;  // invariant c != b maintained by setting c_
            if( b_ < na_ )  // a and b are set, take next step
            {
                c_ = a_; increment3();  // recursively handles c == b and follow-up
            }
            else  // b == na
            {
                b_ = 0;
                ++a_;
                if( a_ != na_ )
                {
                    c_ = a_; increment3();
                }
            }
        }

        // The loop version of the iteration logic:
        // for(long a = 0; a < na; ++a)
        //     for(long b = 0; b < na; ++b)
        //     {
        //         if(b == a) continue;
        //         for(long c = a + 1; c < na; ++c)
        //         {
        //             if(c == b) continue;
        //             ...
    }

    void increment4()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & d_ = Base::d_;
        Int & na_ = Base::na_;

        ++d_; while ( d_ == c_ || d_ == b_ ) ++d_;
        if( d_ == na_ )
        {
            ++c_; while( c_ == a_ || c_ == b_ ) ++c_;
            if( c_ < na_ )
            {
                d_ = a_; increment4();
            }
            else
            {
                ++b_; while( b_ == a_ ) ++b_;
                if( b_ < na_ )
                {
                    c_ = 0; while( c_ == a_ || c_ == b_ ) ++c_;  // at least two choices other than na
                    d_ = a_;
                    increment4();
                }
                else
                {
                    ++a_;
                    if( a_ != na_ )
                    {
                        b_ = 0;
                        c_ = 0; while( c_ == a_ || c_ == b_ ) ++c_;  // at least two choices other than na
                        d_ = a_; increment4();
                    }
                }
            }
        }

        // The loop version of another iteration logic:
        // for(long a = 0; a < na; ++a)
        //     for(long d = a + 1; d < na; ++d)
        //         for(long b = 0; b < na; ++b)
        //         {
        //             if(b == a || b == d) continue;
        //             for(long c = 0; c < na; ++c)
        //             {
        //                 if(c == a || c == b || c == d) continue;
        //                 ...

    }
};

// Specialization of traits class
template<typename Int_>
struct mbtr_aindexf_base_traits<mbtr_aindexf_finite_noreversals<Int_>>
{
    typedef mbtr_aindexf_finite_noreversals<Int_> Base;
    typedef Int_ Int;
};

// Unit cell replica iterator
//
// This helper class encapsulates the logic for iteration over unit cell replicas.
// Iteration is from the inside out and stops as soon as there is no more change (to the tensor).
// Equivalent of three nested for loops that iterate over 0, -1, -2, ..., 1, 2, ... as long as there is activity.
// Implemented as its own class for decoupling of code and resulting better testing.
class mbtr_replica_iterator
{
    public:
        mbtr_replica_iterator() { reset(); }

        void reset()
        {
            reset_x();
            reset_y();
            reset_z();
            state_ = state_t::z;
            done_ = false;
        }

        // Takes one step in current iteration. a indicates whether activity has happened for current iteration variable values.
        void step(bool a)
        {
            typedef direction_t d; typedef state_t s;  // 'using state_t::x', although accepted by LLVM/clang, is invalid C++ (no using for scoped enums)

            switch(state_)
            {
                case s::x:
                    if(a)  // a is true if activity happened in y loop (one level below)
                    {
                        if(dirx_ == d::negative) --x_; else ++x_;  // take a step
                        reset_y(); reset_z();
                        state_ = s::z;
                    }
                    else
                    {
                        if(dirx_ == d::negative && x_ < 0)
                        {
                            dirx_ = d::positive;
                            x_ = 1;
                            reset_y(); reset_z();
                            state_ = s::z;
                        }
                        else
                        {
                            done_ = true;  // terminate loop
                        }
                    }
                    break;
                case s::y:
                    if(a)  // a is true if activity happened in z loop
                    {
                        updatey_ = true;  // remember there was activity at this level
                        if(diry_ == d::negative) --y_; else ++y_;  // take a step
                        reset_z();
                        state_ = s::z;
                    }
                    else
                    {
                        if(diry_ == d::negative && y_ < 0)
                        {
                            diry_ = d::positive;  // switch to positive direction
                            y_ = 1;
                            reset_z();
                            state_ = s::z;
                        }
                        else
                        {
                            state_ = s::x;
                            step(updatey_);
                        }
                    }
                    break;
                case s::z:
                    if(a)
                    {
                        updatez_ = true;  // remember there was activity at this level
                        if(dirz_ == d::negative) --z_; else ++z_;  // take a step
                    }
                    else
                    {
                        if(dirz_ == d::negative && z_ < 0)
                        {
                            dirz_ = d::positive;
                            z_ = 1;
                        }
                        else
                        {
                            state_ = s::y;
                            step(updatez_);
                        }
                    }
                    break;
            }
        }

        explicit operator bool() const { return !done_; };

        // true if current cell is unit cell proper
        bool is_zero() const { return x_ == 0 && y_ == 0 && z_ == 0; }

        // true if lhs and rhs are at the same unit cell replica
        bool operator==(mbtr_replica_iterator const& rhs) const
        {
            return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
        }

        long x() const { return x_; }
        long y() const { return y_; }
        long z() const { return z_; }

    private:
        enum class direction_t : uint_fast8_t { negative, positive };    // Possible directions of iteration along basis vectors
        enum class state_t : uint_fast8_t { x, y, z };                   // Possible active directions of iteration (x, y, z = first, second, third basis vector)

        direction_t dirx_, diry_, dirz_;    // Sign of current direction of iteration (x, y, z)
        bool updatex_, updatey_, updatez_;  // True if activity happened at current level (see state_)
        long x_, y_, z_;                    // Values of iteration variables: 0, -1, -2, ..., 1, 2, ...
        state_t state_;                     // Currently active direction (x, y, z = along first, second, third basis vector)
        bool done_;                         // True if iteration has terminated

        void reset_x() { dirx_ = direction_t::negative; updatex_ = false; x_ = 0; }
        void reset_y() { diry_ = direction_t::negative; updatey_ = false; y_ = 0; }
        void reset_z() { dirz_ = direction_t::negative; updatez_ = false; z_ = 0; }
};

// Atom indexing base class for periodic systems
//
// Code factored out into a base class for atom iterators over periodic systems.
// Uses curiously recurring template pattern to statically access derived class.
// This base class is suited for iteration over replica's of a given system.
//
// * Uniqueness: the tuple (bx, by, bz, b) must be unique (similar for c, d, but not a)

template<typename Derived> class mbtr_aindexf_periodic_base
{
    public:
        static constexpr bool is_periodic = true;

        typedef typename mbtr_aindexf_base_traits<Derived>::Int Int;

        mbtr_aindexf_periodic_base() : ssizes_(nullptr), sinds_(nullptr), zz_(nullptr), rr_(nullptr), bases_(nullptr), basis_(nullptr) {}

        template<typename T> void init(T const & mbtr)
        {
            ssizes_ = mbtr.ssizes();
            sinds_  = mbtr.sinds();
            zz_     = mbtr.zz();
            rr_     = mbtr.rr();
            bases_  = mbtr.bases();
            k_      = mbtr.k();
            basis_  = nullptr;
        }

        void reset(long const s)
        {
            s_     = s;
            na_    = ssizes_[s];
            basis_ = bases_ + s*3*3;
            done_  = ( ssizes_[s] < 1 );  // special case of empty system; otherwise there are always sufficient atoms for unique k-tuples due to replicas
            static_cast<Derived *>(this)->initialize();
        }

        explicit operator bool() const { return !done_; }

        void operator()(Int & za, double ra[3])
        {
            const long ind = sinds_[s_];
            update(za, ra, ind, a_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3])
        {
            const long ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_, bb_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3], Int & zc, double rc[3])
        {
            const long ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_, bb_);
            update(zc, rc, ind, c_, cc_);
        }

        void operator()(Int & za, double ra[3], Int & zb, double rb[3], Int & zc, double rc[3], Int & zd, double rd[3])
        {
            const long ind = sinds_[s_];
            update(za, ra, ind, a_);
            update(zb, rb, ind, b_, bb_);
            update(zc, rc, ind, c_, cc_);
            update(zd, rd, ind, d_, dd_);
        }

        void step(bool const a)
        {
            switch(k_)
            {
                case 1: static_cast<Derived *>(this)->increment1(a); break;
                case 2: static_cast<Derived *>(this)->increment2(a); break;
                case 3: static_cast<Derived *>(this)->increment3(a); break;
                case 4: static_cast<Derived *>(this)->increment4(a); break;
            }
            termination_condition();
        }

    protected:
        // MBTR-derived properties
        Int const * ssizes_;  // number of atoms of each system
        Int const * sinds_;   // index into linearized zz and rr of each system
        Int const * zz_;      // linearized atomic numbers of all systems
        double const * rr_;    // linearized atom coordinates of all systems
        double const * bases_; // unit cell basis vectors
        long k_;               // element rank of tensor

        // iteration-specific properties
        long s_;               // index of system currently iterated over
        Int na_;              // number of atoms of current system
        double const * basis_; // basis vectors of current system
        bool done_;            // true if iteration for current system has finished
        Int a_, b_, c_, d_;   // iteration indices within unit cell replicas
        bool ab_, ac_, ad_;    // activity indicators for replica iterators
        mbtr_replica_iterator bb_, cc_, dd_;  // iteration indices over unit cell replica

        // for updates of first index, which is bound to the unit cell proper
        void update(Int & z, double r[3], long const ind, long const i) const
        {
            z = zz_[ind + i];
            r[0] = rr_[(ind + i)*3 + 0];
            r[1] = rr_[(ind + i)*3 + 1];
            r[2] = rr_[(ind + i)*3 + 2];
        }

        // helper setting z and r for atom i in unit cell replica ii in system with index ind
        void update(Int & z, double r[3], long const ind, long const i, mbtr_replica_iterator ii_) const
        {
            z = zz_[ind + i];
            r[0] = rr_[(ind + i)*3 + 0] + ii_.x() * basis_[0] + ii_.y() * basis_[3] + ii_.z() * basis_[6];
            r[1] = rr_[(ind + i)*3 + 1] + ii_.x() * basis_[1] + ii_.y() * basis_[4] + ii_.z() * basis_[7];
            r[2] = rr_[(ind + i)*3 + 2] + ii_.x() * basis_[2] + ii_.y() * basis_[5] + ii_.z() * basis_[8];
        }

        // a is the inner-most iterator (the most slowly changing variable)
        // iteration terminates once a has assumed all possible values
        void termination_condition()
        {
            if( a_ == na_ ) done_ = true;
        }
};

// Full indexing for periodic systems
//
// Iterates over all tuples of k unique atoms in a periodic system.
// This is a literal translation of MBTR without any symmetry corrections.
// For many geometry functions, a symmetry-adapted atom indexing function can be used to reduce compute time.
//
// * atom loop is inside replica loop as this potentially allows caching of basis vector multiples
template<typename Int>
struct mbtr_aindexf_periodic_full : public mbtr_aindexf_periodic_base<mbtr_aindexf_periodic_full<Int>>
{
    typedef typename mbtr_aindexf_base_traits<mbtr_aindexf_periodic_full>::Base Base;

    void initialize(void)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & d_ = Base::d_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;
        mbtr_replica_iterator & cc_ = Base::cc_;
        mbtr_replica_iterator & dd_ = Base::dd_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;
        bool & ad_ = Base::ad_;

        bb_.reset(); cc_.reset(); dd_.reset();
        ab_ = false; ac_ = false; ad_ = false;
        a_ = 0; b_ = 1; c_ = 2; d_ = 3;  // unique tuple if sufficient atoms in unit cell proper
        switch(na_)  // if insufficient atoms, choose from next replica
        {
            // case 0: done_ is true
            case 1:
                b_ = 0; bb_.step(true);
                c_ = 0; cc_.step(true); cc_.step(true);
                d_ = 0; dd_.step(true); dd_.step(true); dd_.step(true);
                break;
            case 2:
                c_ = 0; cc_.step(true);
                d_ = 1; dd_.step(true);
                break;
            case 3:
                d_ = 0; dd_.step(true);
                break;
            // case 4 and higher: keep unique tuple
        }
    }

    void increment1(bool const)
    {
        Int & a_ = Base::a_;

        ++a_;

        // for a = 0..na-1
        //
        //     ....
    }

    void increment2(bool const a)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;

        bool & ab_ = Base::ab_;

        // remember activity for replica bb
        if(a) ab_ = true;

        // increment b
        ++b_;
        if( a_ == b_ && bb_.is_zero() ) ++b_;  // avoid comparing b with itself; post: b <= na

        // if b is done, increment bb
        if( b_ == na_ )
        {
            bb_.step( ab_ || bb_.is_zero() );  // forcing activity for bb = 0 ensures visit of at least neighboring replicas; post: bb != 0 => a != b;
            ab_ = false;

            if( bb_ )
            {
                b_ = 0;  // always valid as b != a
            }
            else  // if bb is done, increment a
            {
                increment1(false);  // b = 0 can not be a > 0
                bb_.reset(); b_ = -1; increment2(false);
            }
        }

        // for a = 0..na-1
        //
        //     for bb = replicas with activity
        //         for b = 0..na-1
        //
        //             ....
    }

    void increment3(bool const a)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;
        mbtr_replica_iterator & cc_ = Base::cc_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;

        // remember activity for replicas cc and bb
        if(a) ab_ = ac_ = true;

        // increment c
        ++c_;
        while( ( c_ == a_ && cc_.is_zero() ) || ( c_ == b_ && cc_ == bb_ ) ) ++c_;  // avoid comparing c with itself; post: c <= na

        // if c is done, increment cc
        if( c_ == na_ )
        {
            cc_.step( ac_ || cc_.is_zero() );  // forcing activity for cc = 0 ensures visiting at least neighboring replicas; post: cc != 0 => a != c
            ac_ = false;

            if( cc_ )
            {
                c_ = 0;
                if( /* c != a */ c_ == b_ && cc_ == bb_ )
                {
                    if(na_ == 1) ac_ = true;  // special case of one-atom unit cell, enforce continuation; do not touch ab_
                    increment3(false);
                }
            }
            else  // if cc is done, increment b
            {
                increment2(false);
                cc_.reset(); c_ = 0;
                if( ( c_ == a_ && cc_.is_zero() ) || ( c_ == b_ && cc_ == bb_ ) )
                {
                    if(na_ == 1) ac_ = true;
                    increment3(false);
                }
            }
        }

        // for a = 0..na-1
        //
        //     for bb in replicas
        //         for b = 0..na-1
        //
        //             for cc in replicas
        //                 for c = 0..na-1
        //
        //                     ....
    }

    // TODO not tested yet
    void increment4(bool const a)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & d_ = Base::d_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;
        mbtr_replica_iterator & cc_ = Base::cc_;
        mbtr_replica_iterator & dd_ = Base::dd_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;
        bool & ad_ = Base::ad_;

        // remember activity for replicas dd, cc and bb
        if(a) ab_ = ac_ = ad_ = true;

        // increment d
        ++d_;
        while ( ( d_ == a_ && dd_.is_zero() ) || ( d_ == b_ && dd_ == bb_ ) || ( d_ == c_ && dd_ == cc_ ) ) ++d_;  // avoid comparing d with itself; post: d <= na

        // if d is done, increment dd
        if (d_ == na_ )
        {
            dd_.step( ad_ || dd_.is_zero() ); // forcing activity for dd = 0 ensures visiting at least neighboring replicas; post: dd != 0 => a != d
            ad_ = false;

            if( dd_ )
            {
                d_ = 0;
                if( ( d_ == b_ && dd_ == bb_ ) || ( d_ == c_ && dd_ == cc_ ) )
                {
                    if(na_ == 1) ad_ = true;   // special case of one-atom unit cell, enforce continuation; do not touch ab_, ac_
                    increment4(false);
                }
            }
            else  // if dd is done, increment c
            {
                increment3(false);
                dd_.reset(); d_ = 0;
                if( ( d_ == a_ && dd_.is_zero() ) || ( d_ == b_ && dd_ == bb_ ) || ( d_ == c_ && dd_ == cc_ ))
                {
                    if(na_ == 1) ad_ = true;
                    increment4(false);
                }
            }
        }

        // for a = 0..na-1
        //
        //     for bb in replicas
        //         for b = 0..na-1
        //
        //             for cc in replicas
        //                 for c = 0..na-1
        //
        //                     for dd in replicas
        //                         for d = 0..na-1
        //
        //                             ....
    }
};

// Specialization of traits class
template<typename Int_>
struct mbtr_aindexf_base_traits<mbtr_aindexf_periodic_full<Int_>>
{
    typedef mbtr_aindexf_periodic_full<Int_> Base;
    typedef Int_ Int;
};

// No-reversals indexing for periodic systems
//
// Iterates over all tuples of k unique atoms up to reversals in a periodic system.
// This atom indexing scheme omits reversed atom tuples; e.g., for k = 4, abcd will be iterated over, but dcba will not.
// In this example, a,b,c,d can be atoms in different replica.
// Geometry functions for which this is appropriate include (inverse) distances, angles, and dihedral angles.
template<typename Int>
struct mbtr_aindexf_periodic_noreversals : public mbtr_aindexf_periodic_base<mbtr_aindexf_periodic_noreversals<Int>>
{
    typedef typename mbtr_aindexf_base_traits<mbtr_aindexf_periodic_noreversals>::Base Base;

    void initialize()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & d_ = Base::d_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;
        mbtr_replica_iterator & cc_ = Base::cc_;
        mbtr_replica_iterator & dd_ = Base::dd_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;
        bool & ad_ = Base::ad_;

        bb_.reset(); cc_.reset(); dd_.reset();
        ab_ = false; ac_ = false; ad_ = false;
        a_ = 0; b_ = 1; c_ = 2; d_ = 3;  // unique tuple if sufficient atoms in unit cell
        switch(na_)  // if insufficient atoms, choose from next replica
        {
            // case 0: done_ is true
            case 1:
                b_ = 0; bb_.step(true); bb_.step(false);
                c_ = 0; cc_.step(true); cc_.step(false); cc_.step(true);
                d_ = 0; dd_.step(true); dd_.step(false); dd_.step(true); dd_.step(true);
                break;
            case 2:
                c_ = 0; cc_.step(true); cc_.step(false);
                d_ = 1; dd_.step(true); dd_.step(false);
                break;
            case 3:
                d_ = 0; dd_.step(true); dd_.step(false);
                break;
        }
    }

    // checks whether any dimension of iterator is negative
    bool negative(mbtr_replica_iterator const & i) const
    {
        return i.z() < 0 || i.y() < 0 || i.x() < 0;
    }

    void increment1(bool const)
    {
        Int & a_ = Base::a_;

        ++a_;

        // for a = 0..na-1
    }

    void increment2(bool const a)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;

        bool & ab_ = Base::ab_;

        // remember activity for replica bb
        if(a) ab_ = true;

        // increment b
        ++b_;

        // if b is done, increment bb
        if( b_ == na_ )
        {
            bb_.step( ab_ || bb_.is_zero() );  // force activity for unit cell proper
            if( negative(bb_) ) bb_.step(false);  // only non-negative quadrant
            ab_ = false; b_ = 0;

            if( !bb_ ) // if bb is done, increment a
            {
                increment1(false);
                bb_.reset();
                b_ = a_ + 1;
                if( b_ == na_ )
                {
                    bb_.step(true); bb_.step(false); b_ = 0;
                }
            }
        }

        // for a = 0..na-1
        //     for bb = replicas with activity
        //         for b = 0..na-1
        //             ....
    }

    // This helper differs from pairwise increment in how b_ is set (starting from 0 due to different symmetries in the k=3 case)
    void increment2b()
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;

        bool & ab_ = Base::ab_;

        // increment b
        ++b_;
        while( bb_.is_zero() && b_ == a_ ) ++b_;

        // if b is done, increment bb
        if( b_ == na_ )
        {
            bb_.step( ab_ || bb_.is_zero() );  // force activity for unit cell proper
            if( negative(bb_) ) bb_.step(false);  // only non-negative quadrant
            ab_ = false; b_ = 0;

            if( !bb_ ) // if bb is done, increment a
            {
                increment1(false);
                bb_.reset();
                // b remains 0
                if( b_ == a_ ) increment2(false);
            }
        }
    }

    void increment3(bool const a)
    {
        Int & a_ = Base::a_;
        Int & b_ = Base::b_;
        Int & c_ = Base::c_;
        Int & na_ = Base::na_;

        mbtr_replica_iterator & bb_ = Base::bb_;
        mbtr_replica_iterator & cc_ = Base::cc_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;

        // remember activity for replicas bb and cc
        if(a) ab_ = ac_ = true;

        // increment c
        ++c_;
        while( bb_ == cc_ && b_ == c_ ) ++c_;  // c can not be a due to reversals exclusion

        // if c is done, increment cc
        if( c_ == na_ )
        {
            cc_.step( ac_ || cc_.is_zero() );  // force activity for unit cell proper
            if( negative(cc_) ) cc_.step(false);  // only non-negative quadrant
            ac_ = false; c_ = 0;

            if( cc_ )
            {
                // cc is not a as cc was increased
                if( cc_ == bb_ )
                {
                    c_ = -1;
                    increment3(na_ == 1);
                }
            }
            else  // if cc is done, increment b
            {
                increment2b();
                cc_.reset();
                c_ = a_;
                increment3(false);
            }
        }

        // for a = 0..na-1
        //
        //     for bb in replicas
        //         for b = 0..na-1
        //
        //             for cc in replicas
        //                 for c = 0..na-1
        //
        //                     ....
    }

    void increment4(bool const a)
    {
        throw QMML_ERROR("4-body iteration not implemented for no-reversal atom indexing.");  // TODO

//        Int & a_ = Base::a_;
//        Int & b_ = Base::b_;
//        Int & c_ = Base::c_;
//        Int & d_ = Base::d_;
//        Int & na_ = Base::na_;

//        mbtr_replica_iterator & bb_ = Base::bb_;
//        mbtr_replica_iterator & cc_ = Base::cc_;
//        mbtr_replica_iterator & dd_ = Base::dd_;

        bool & ab_ = Base::ab_;
        bool & ac_ = Base::ac_;
        bool & ad_ = Base::ad_;

        // remember activity for replicas bb, cc and dd
        if(a) ab_ = ac_ = ad_ = true;
    }

    // void debug(bool const a, char const * const pre = "", char const * const post = "") const
    // {
    //     std::cout << pre;
    //     std::cout << " " << a_;
    //     if(k_ >= 2) std::cout << "  " << (bb_.x() >= 0 ? "+" : "") << bb_.x() << " " << (bb_.y() >= 0 ? "+" : "") << bb_.y() << " " << (bb_.z() >= 0 ? "+" : "") << bb_.z() << "  " << b_ << " " << (ab_ ? "y" : "n");
    //     if(k_ >= 3) std::cout << "  " << (cc_.x() >= 0 ? "+" : "") << cc_.x() << " " << (cc_.y() >= 0 ? "+" : "") << cc_.y() << " " << (cc_.z() >= 0 ? "+" : "") << cc_.z() << "  " << c_ << " " << (ad_ ? "y" : "n");
    //     if(k_ >= 4) std::cout << "  " << (dd_.x() >= 0 ? "+" : "") << dd_.x() << " " << (dd_.y() >= 0 ? "+" : "") << dd_.y() << " " << (dd_.z() >= 0 ? "+" : "") << dd_.z() << "  " << d_ << " " << (ad_ ? "y" : "n");
    //     std::cout << "  (" << (a ? "y" : "n") << ")" << std::endl << post << std::flush;
    // }
};

// Specialization of traits class
template<typename Int_>
struct mbtr_aindexf_base_traits<mbtr_aindexf_periodic_noreversals<Int_>>
{
    typedef mbtr_aindexf_periodic_noreversals<Int_> Base;
    typedef Int_ Int;
};


// Many-body tensor representation
// -------------------------------

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::check()
{
    if(mbtr_   == nullptr) throw QMML_ERROR("MBTR destination memory not set");
    if(ssizes_ == nullptr) throw QMML_ERROR("MBTR system sizes not specified");
    if(sinds_  == nullptr) throw QMML_ERROR("MBTR system indices not specified");
    if(A::is_periodic && bases_ == nullptr) throw QMML_ERROR("MBTR unit cell vectors not specified for periodic systems");
    if(zz_     == nullptr) throw QMML_ERROR("MBTR atomic numbers not specified");
    if(rr_     == nullptr) throw QMML_ERROR("MBTR atomic coordinates not specified");
    if(elems_  == nullptr) throw QMML_ERROR("MBTR dataset elements not specified");

    // Element correlation matrices are currently not implemented.
    // Only valid choice at the moment is Dirac delta (identity matrix).
    if(typeid(corrf_) != typeid(mbtr_identity_correlation)) throw QMML_ERROR("Element correlation functions not yet supported.");
};

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::compute()
{
    // basic tests for parameter consistency
    check();

    // Set up all functions
    geomf_  .init(*this);
    weightf_.init(*this);
    distrf_ .init(*this);
    corrf_  .init(*this);
    eindexf_.init(*this);
    aindexf_.init(*this);

    // zero result memory
    std::memset(mbtr_, 0, ( eindexf_.dest_addr(ns_) - mbtr_ ) * sizeof(mbtr_[0]) );  // requires initialized eindexf

    // Loop over all systems
    for(long s = 0; s < ns_; ++s)
    {
        // signal that a new system is being processed
        geomf_  .reset(s);
        weightf_.reset(s);
        // distribution function is reset for each tensor update
        corrf_  .reset(s);
        eindexf_.reset(s);
        aindexf_.reset(s);

        // call appropriate member function depending on k to iterate over atom tuples of current system, updating tensor
        compute_system(s);
    }
}

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
double many_body_tensor_representation<K,G,W,D,C,E,A, Int>::update_tensor(double * dest, double const mu, double const sf)
{
    if( mu == mbtr_undefined ) return false;

    distrf_.reset(mu);  // reset distribution function before any calls to it

    // set up range iterators
    const double range_from = distrf_.range_from();  // in units of spatial axis
    const double range_to = distrf_.range_to();  // in units of spatial axis

    long jfrom = static_cast<long>( std::floor( ( range_from - xmin_ ) / deltax_ ) );  // TODO: removed -1 here; add test
    jfrom = jfrom < 0 ? 0 : ( jfrom >= xdim_ ? xdim_-1 : jfrom );  // bracket/clip

    long jto = static_cast<long>( std::ceil( ( range_to - xmin_ ) / deltax_ ) );  // TODO: removed -1 here; add test
    jto = jto < 0 ? 0 : ( jto >= xdim_ ? xdim_-1 : jto );  // bracket/clip

    double dlast = distrf_.cdf( xmin_ + jfrom * deltax_ );
    double xmax = 0;

    for(long j = jfrom; j <= jto; ++j)
    {
        const double dnext = distrf_.cdf( xmin_ + (j+1) * deltax_ );
        const double delta = (dnext - dlast) * sf;
        dlast = dnext;
        dest[j] += delta;
        xmax = std::max(xmax, delta);  // only contributions inside tensor range count; TODO: could replace max by sum (divided by #steps). Faster?
    }

    return xmax;
}

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
template<int T, std::enable_if_t<T == 1, int>>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::compute_system(long const s)
{
    while(aindexf_)
    {
        // obtain next atom k-tuple
        Int za; double ra[3];
        aindexf_(za, ra);

        // geometry and weighting function
        const double mu = geomf_  (    za, ra);
        const double sf = weightf_(mu, za, ra);

        // update tensor
        const long rowind = eindexf_(za);  // tensor element index
        const double delta = update_tensor(eindexf_.dest_addr(s) + rowind*xdim_, mu, sf);
        const bool update = (delta > acc_);

        // iterate to next atom tuple
        aindexf_.step(update);
    }
}

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
template<int T, std::enable_if_t<T == 2, int>>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::compute_system(long const s)
{
    while(aindexf_)
    {
        // obtain next atom tuple
        Int za, zb; double ra[3], rb[3];
        aindexf_(za, ra, zb, rb);

        // geometry and weighting function
        const double mu = geomf_  (    za, ra, zb, rb);
        const double sf = weightf_(mu, za, ra, zb, rb);

        // update tensor
        const long rowind = eindexf_(za, zb);  // tensor element index
        const double delta = update_tensor(eindexf_.dest_addr(s) + rowind*xdim_, mu, sf);
        const bool update = (delta > acc_);

        // iterate to next atom tuple
        aindexf_.step(update);
    }
}

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
template<int T, std::enable_if_t<T == 3, int>>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::compute_system(long const s)
{
    while(aindexf_)
    {
        // obtain next atom tuple
        Int za, zb, zc; double ra[3], rb[3], rc[3];
        aindexf_(za, ra, zb, rb, zc, rc);

        // geometry and weighting function
        const double mu = geomf_  (    za, ra, zb, rb, zc, rc);
        const double sf = weightf_(mu, za, ra, zb, rb, zc, rc);

        // update tensor
        const long rowind = eindexf_(za, zb, zc);  // tensor element index
        const double delta = update_tensor(eindexf_.dest_addr(s) + rowind*xdim_, mu, sf);
        const bool update = (delta > acc_);

        // iterate to next atom tuple
        aindexf_.step(update);
    }
}

template<int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
template<int T, std::enable_if_t<T == 4, int>>
void many_body_tensor_representation<K,G,W,D,C,E,A,Int>::compute_system(long const s)
{
    while(aindexf_)
    {
        // obtain next atom tuple
        Int za, zb, zc, zd; double ra[3], rb[3], rc[3], rd[3];
        aindexf_(za, ra, zb, rb, zc, rc, zd, rd);

        // geometry and weighting function
        const double mu = geomf_  (    za, ra, zb, rb, zc, rc, zd, rd);
        const double sf = weightf_(mu, za, ra, zb, rb, zc, rc, zd, rd);

        // update tensor
        const long rowind = eindexf_(za, zb, zc, zd);  // tensor element index
        const double delta = update_tensor(eindexf_.dest_addr(s) + rowind*xdim_, mu, sf);
        const bool update = (delta > acc_);

        // iterate to next atom tuple
        aindexf_.step(update);
    }
}

// Auxiliary functionality
// -----------------------

inline void fractional_to_cartesian(double * const dest, long const ns, double const * const ssizes, double const * const sinds, double const * const bases, double const * const rr)
{
    for(long s = 0; s < ns; ++s)
        for(long i = 0; i < ssizes[s]; ++i)
        {
            long const cind = (sinds[s]+i)*3;  // coordinate index
            long const bind = s*9;  // basis index
            dest[cind+0] = rr[cind+0]*bases[bind+0] + rr[cind+1]*bases[bind+3] + rr[cind+2]*bases[bind+6];
            dest[cind+1] = rr[cind+0]*bases[bind+1] + rr[cind+1]*bases[bind+4] + rr[cind+2]*bases[bind+7];
            dest[cind+2] = rr[cind+0]*bases[bind+2] + rr[cind+1]*bases[bind+5] + rr[cind+2]*bases[bind+8];
        }
}

}  // namespace qmml

#endif  // include guard
