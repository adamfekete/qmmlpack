// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Utility routines for testing kernels.

#ifndef QMMLPACK_TEST_KERNELS_HPP_INCLUDED  // include guard
#define QMMLPACK_TEST_KERNELS_HPP_INCLUDED

#include <random>
#include "util.hpp"

//  ////////////////////////////////////////////
//  //  Testing kernel matrices for symmetry  //
//  ////////////////////////////////////////////

template<typename T, typename U> bool test_kernel_matrix_d_symmetry(T kernel, U distance, const size_t n, const size_t d)
{
    // random input matrix
    double *const xx = new double[n*d];
    for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < d; ++j) xx[i*d+j] = random_uniform_real(-1, 1);

    // distance matrix
    double *const dd = new double[n*n];
    distance(dd, xx, n, d);

    // kernel matrix
    double *const kk = new double[n*n];
    kernel(kk, dd, n, n);

    // test symmetry
    const bool result = is_symmetric_matrix(kk, n);

    delete[] kk;
    delete[] dd;
    delete[] xx;

    return result;
}

template<typename T, typename U, typename E> bool test_kernel_matrix_d_symmetry(T kernel, U distance, const size_t n, const size_t d, const E theta)
{
    auto k = [&](auto kk_, auto xx_, auto n_, auto d_) { return kernel(kk_, xx_, n_, d_, theta); };
    return test_kernel_matrix_d_symmetry(k, distance, n, d);
}

template<typename T> bool test_kernel_matrix_k_symmetry(T kernel, const size_t n, const size_t d)
{
    // random input matrix
    double *const xx = new double[n*d];
    for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < d; ++j) xx[i*d+j] = random_uniform_real(-1, 1);

    // kernel matrix
    double *const kk = new double[n*n];
    kernel(kk, xx, n, d);

    // test symmetry
    const bool result = is_symmetric_matrix(kk, n);

    delete[] kk;
    delete[] xx;

    return result;
}

template<typename T, typename E> bool test_kernel_matrix_k_symmetry(T kernel, const size_t n, const size_t d, const E theta)
{
    auto k = [&](auto kk_, auto xx_, auto n_, auto d_) { return kernel(kk_, xx_, n_, d_, theta); };
    return test_kernel_matrix_k_symmetry(k, n, d);
}

#endif  // include guard
