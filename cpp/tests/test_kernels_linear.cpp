// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for linear kernel

#include "catch/catch.hpp"
#include "qmmlpack/kernels_linear.hpp"
#include "util.hpp"
#include "test_kernels.hpp"

using namespace qmml;

TEST_CASE("Linear kernel", "[kernels]")
{
    SECTION( "K" )
    {
        // one sample
        {
            const double xx[1][3] { {1, 2, 3} };
            const double rr[1][1] { {14} };
            double kk[1][1];

            kernel_matrix_linear_k(&kk[0][0], &xx[0][0], 1, 3);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 1) );
        }

        // two samples
        {
            const double xx[2][2] { {1, -1}, {2, 3} };
            const double rr[2][2] { {2, -1}, {-1, 13} };
            double kk[2][2];

            kernel_matrix_linear_k(&kk[0][0], &xx[0][0], 2, 2);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // random 1d samples
        {
            const double xx[5][1]
            {
                {-3.5578999445797557},
                {-2.2354209763725947},
                { 6.5842430275922650},
                {-1.7508029405309670},
                { 3.4757634012503330}
            };
            const double rr[5][5]
            {
                { 12.658652015640628,   7.953404167948478 , -23.426077902970164,   6.229181685085201 , -12.366418412680904},
                {  7.953404167948478,   4.997106941606605 , -14.718554977414751,   3.9137816187577443, - 7.76979441606315 },
                {-23.426077902970164, -14.718554977414751 ,  43.35225624639736 , -11.527712053879055 ,  22.885270940242883},
                {  6.229181685085201,   3.9137816187577443, -11.527712053879055,   3.065310936571881 , - 6.085376783498999},
                {-12.366418412680904, - 7.76979441606315  ,  22.885270940242883, - 6.085376783498999 ,  12.080931221471284}
            };
            double kk[5][5];

            kernel_matrix_linear_k(&kk[0][0], &xx[0][0], 5, 1);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 5*5) );
        }

        // target memory not zero
        {
            const double xx[2][1] { {1}, {3} };
            const double rr[2][2] { {1, 3}, {3, 9} };
            double kk[2][2] { {90, 91}, {92, 93} };

            kernel_matrix_linear_k(&kk[0][0], &xx[0][0], 2, 1);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // Symmetry
        {
            test_kernel_matrix_k_symmetry(kernel_matrix_linear_k,    2,   1);
            test_kernel_matrix_k_symmetry(kernel_matrix_linear_k,   20,  10);
            test_kernel_matrix_k_symmetry(kernel_matrix_linear_k,  100, 500);
            test_kernel_matrix_k_symmetry(kernel_matrix_linear_k, 1000, 100);
        }
    }

    SECTION("L")
    {
        // non-const matrix inputs
        {
            const double xx[2][2] { {  1,  2 }, { 3, 4 } };
            const double zz[1][2] { { -1, -2 } };
            const double rr[2][1] { { -5 }, { -11 } };
            double ll[2][1];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 1, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*1) );
        }

        // one sample each, d = 2
        {
            const double xx[1][2] { { 1, -1 } };
            const double zz[1][2] { { 2,  3 } };
            const double rr[1][1] { { -1 } };
            double ll[1][1];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // one sample each, d = 3
        {
            const double xx[1][3] { {  1, 2,  3 } };
            const double zz[1][3] { {  2, 0, -1 } };
            const double rr[1][1] { { -1 } };
            double ll[1][1];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 3);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // two samples versus one
        {
            const double xx[2][2] { {  1,  2 }, { 3, 4 } };
            const double zz[1][2] { { -1, -2 } };
            const double rr[2][1] { { -5 }, { -11 } };
            double ll[2][1];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 1, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*1) );
        }

        // one samples versus two
        {
            const double xx[1][2] { { 3, 1 } };
            const double zz[2][2] { { 5, 6 }, { 0, 1 } };
            const double rr[1][2] { { 21, 1 } };
            double ll[1][2];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 2, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*2) );
        }

        // two samples versus two
        {
            const double xx[2][2] { {  1,  2 }, {  3,  5 } };
            const double zz[2][2] { { -2, -1 }, { -4, -3 } };
            const double rr[2][2] { { -4, -10 }, { -11, -27 } };
            double ll[2][2];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 2, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*2) );
        }

        // three samples versus two
        {
            const double xx[3][2] { {  1, -1 }, {  2,  3 }, { -1, 2 } };
            const double zz[2][2] { { -1, -1 }, {  4,  5 } };
            const double rr[3][2] { {  0, -1 }, { -5, 23 }, { -1, 6 } };
            double ll[3][2];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2);
            REQUIRE( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        {
            const double xx[3][2] { { -2, - 1 }, { - 4, - 3 }, { - 6, - 5 } };
            const double zz[2][2] { {  1,   2 }, {   3,   4 } };
            const double rr[3][2] { { -4, -10 }, { -10, -24 }, { -16, -38 } };
            double ll[3][2];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // two samples versus three
        {
            const double xx[2][2] { {  1,  2 }, {  3,  4 } };
            const double zz[3][2] { { -2, -1 }, { -4, -3 }, { -6, -5 } };
            const double rr[2][3] { { -4, -10, -16 }, { -10, -24, -38 } };
            double ll[2][3];

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 3, 2);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }

        // target memory not zero
        {
            const double xx[2][1] { {1}, {2} };
            const double zz[3][1] { {3}, {4}, {5} };
            const double rr[2][3] { {3, 4, 5}, {6, 8, 10} };
            double ll[2][3] { {90, 91, 92}, {93, 94, 95} };

            kernel_matrix_linear_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 3, 1);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }
    }

    SECTION("M")
    {
        // one sample
        {
            const double xx[1][3] { {1, 2, 3} };
            const double res[1] { 14 };
            double m[1];

            kernel_matrix_linear_m(&m[0], &xx[0][0], 1, 3);
            CHECK( equal_abs(&m[0], &res[0], 1) );
        }

        // two samples
        {
            const double xx[2][2] { { 1, -1 }, { 2, 3 } };
            const double res[2] { 2, 13 };
            double m[2];

            kernel_matrix_linear_m(&m[0], &xx[0][0], 2, 2);
            CHECK( equal_abs(&m[0], &res[0], 2) );
         }

        // target memory not zero
        {
            const double xx[2][1] { {1}, {-2} };
            const double rr[2] { 1, 4 };
            double m[2] { 90, 91 };

            kernel_matrix_linear_m(&m[0], &xx[0][0], 2, 1);
            CHECK( equal_abs(&m[0], &rr[0], 2) );
        }
    }
}
