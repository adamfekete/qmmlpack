// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for Laplacian kernel

#include "catch/catch.hpp"
#include "qmmlpack/kernels_laplacian.hpp"
#include "qmmlpack/distances_pnorm.hpp"
#include "util.hpp"
#include "test_kernels.hpp"

using namespace qmml;

TEST_CASE("Laplacian kernel", "[kernels]")
{
    SECTION( "D" )
    {
        // one sample
        {
            const double dd[1][1] { { 0 } };
            const double rr[1][1] { { 1 } };
            double kk[1][1];

            kernel_matrix_laplacian_d(&kk[0][0], &dd[0][0], 1, 1, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 1*1) );
        }

        // two samples
        {
            // X = { { 1, -1 }, { 2, 3 } }
            const double dd[2][2] { {0, 5}, {5, 0} };
            const double rr[2][2] { { 1, std::exp(-5.) }, { std::exp(-5.), 1 } };
            double kk[2][2];

            kernel_matrix_laplacian_d(&kk[0][0], &dd[0][0], 2, 2, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // two samples
        {
            // X = { { 1, 2 }, { 3, 4 } }
            const double dd[2][2] { {0, 4}, {4, 0} };
            const double rr[2][2] { { 1, std::exp(-2.) }, { std::exp(-2.), 1 } };
            double kk[2][2];

            kernel_matrix_laplacian_d(&kk[0][0], &dd[0][0], 2, 2, 2.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // Symmetry
        {
            test_kernel_matrix_d_symmetry(kernel_matrix_laplacian_d, distance_matrix_one_norm_k,    2,   1,   1);
            test_kernel_matrix_d_symmetry(kernel_matrix_laplacian_d, distance_matrix_one_norm_k,   20,  10,   6);
            test_kernel_matrix_d_symmetry(kernel_matrix_laplacian_d, distance_matrix_one_norm_k,  100, 500, 331);
            test_kernel_matrix_d_symmetry(kernel_matrix_laplacian_d, distance_matrix_one_norm_k, 1000, 100,  67);
        }
    }

    SECTION( "E" )
    {
        // one vs one
        {
            // X = { { 1 } }, Z = { { 2 } }
            const double dd[1][1] { { 1 } };
            const double rr[1][1] { { std::exp(-1.) } };
            double ll[1][1];

            kernel_matrix_laplacian_d(&ll[0][0], &dd[0][0], 1, 1, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // three vs two
        {
            // X = { {  1, -1 }, { 2, 3} , { -1, 2 } }, Z = { { -1, -1 }, { 4, 5} }
            const double dd[3][2] { { 2, 9 }, { 7, 4 }, { 3, 8 } };
            const double rr[3][2] { { std::exp(-4), std::exp(-18) }, { std::exp(-14), std::exp(-8) }, { std::exp(-6), std::exp(-16) } };
            double ll[3][2];

            kernel_matrix_laplacian_d(&ll[0][0], &dd[0][0], 3, 2, 0.5);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // two vs three
        {
            // X = { {  1, 2 }, { -1, 3} }, Z = { { -2, 1 }, {  0, 1} , { -1, 2 } }
            const double dd[2][3] { { 4, 2, 2 }, { 3, 3, 1 } };
            const double rr[2][3] { { std::exp(-12), std::exp(-6), std::exp(-6) }, { std::exp(-9), std::exp(-9), std::exp(-3) } };
            double ll[2][3];

            kernel_matrix_laplacian_d(&ll[0][0], &dd[0][0], 2, 3, 1/3.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }
    }

    SECTION( "K" )
    {
        // one sample
        {
            const double xx[1][1] { { 1 } };
            const double rr[1][1] { { 1 } };
            double kk[1][1];

            kernel_matrix_laplacian_k(&kk[0][0], &xx[0][0], 1, 1, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 1*1) );
        }

        // two samples
        {
            const double xx[2][2] { { 1, -1 }, { 2, 3 } };
            const double rr[2][2] { { 1, std::exp(-15) }, { std::exp(-15), 1 } };
            double kk[2][2];

            kernel_matrix_laplacian_k(&kk[0][0], &xx[0][0], 2, 2, 1/3.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // two samples
        {
            const double xx[2][2] { { 1, 2 }, { 3, 4 } };
            const double rr[2][2] { { 1, std::exp(-4) }, { std::exp(-4), 1 } };
            double kk[2][2];

            kernel_matrix_laplacian_k(&kk[0][0], &xx[0][0], 2, 2, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // Symmetry
        {
            test_kernel_matrix_k_symmetry(kernel_matrix_laplacian_k,    2,   1,   1);
            test_kernel_matrix_k_symmetry(kernel_matrix_laplacian_k,   20,  10,   6);
            test_kernel_matrix_k_symmetry(kernel_matrix_laplacian_k,  100, 500, 331);
            test_kernel_matrix_k_symmetry(kernel_matrix_laplacian_k, 1000, 100,  67);
        }
    }

    SECTION( "L" )
    {
        // one versus one
        {
            const double xx[1][1] { { 1 } };
            const double zz[1][1] { { 2 } };
            const double rr[1][1] { { std::exp(-2) } };
            double ll[1][1];

            kernel_matrix_laplacian_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 1, 0.5);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // one versus one
        {
            const double xx[1][2] { { 1, -1 } };
            const double zz[1][2] { { 2,  3 } };
            const double rr[1][1] { { std::exp(-5) } };
            double ll[1][1];

            kernel_matrix_laplacian_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // three versus two
        {
            const double xx[3][2] { {  1, -1 }, { 2, 3} , { -1, 2 } };
            const double zz[2][2] { { -1, -1 }, { 4, 5} };
            const double rr[3][2] { { std::exp(-2), std::exp(-9) }, { std::exp(-7), std::exp(-4) }, { std::exp(-3), std::exp(-8) } };
            double ll[3][2];

            kernel_matrix_laplacian_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // three versus two
        {
            const double xx[3][2] { { -2, 1 }, {  0, 1} , { -1, 2 } };
            const double zz[2][2] { {  1, 2 }, { -1, 3} };
            const double rr[3][2] { { std::exp(-4), std::exp(-3) }, { std::exp(-2), std::exp(-3) }, { std::exp(-2), std::exp(-1) } };
            double ll[3][2];

            kernel_matrix_laplacian_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // two versus three
        {
            const double xx[2][2] { {  1, 2 }, { -1, 3 } };
            const double zz[3][2] { { -2, 1 }, {  0, 1 }, { -1, 2} };
            const double rr[2][3] { { std::exp(-8), std::exp(-4),  std::exp(-4) }, { std::exp(-6), std::exp(-6), std::exp(-2) } };
            double ll[2][3];

            kernel_matrix_laplacian_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 3, 2, 0.5);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }
    }

    SECTION( "M" )
    {
        // one sample, length 1
        {
            const double xx[1][1] { { 1 } };
            const double rr[1] { 1 };
            double mm[1];

            kernel_matrix_laplacian_m(&mm[0], &xx[0][0], 1, 1, 10.);
            CHECK( equal_abs(&mm[0], &rr[0], 1) );
        }

        // one sample, length 3
        {
            const double xx[1][3] { { 1, 2, 3 } };
            const double rr[1] { 1 };
            double mm[1];

            kernel_matrix_laplacian_m(&mm[0], &xx[0][0], 1, 3, 1.);
            CHECK( equal_abs(&mm[0], &rr[0], 1) );
        }

        // three samples
        {
            const double xx[3][2] { { 1, 2 }, { 3, 4 }, { 5, 6} };
            const double rr[3] { 1, 1, 1 };
            double mm[3];

            kernel_matrix_laplacian_m(&mm[0], &xx[0][0], 3, 2, 1.);
            CHECK( equal_abs(&mm[0], &rr[0], 3) );
        }
    }
}
