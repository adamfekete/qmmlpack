// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for Gaussian kernel

#include "catch/catch.hpp"
#include "qmmlpack/kernels_gaussian.hpp"
#include "qmmlpack/distances_pnorm.hpp"
#include "util.hpp"
#include "test_kernels.hpp"

using namespace qmml;

TEST_CASE("Gaussian kernel", "[kernels]")
{
    SECTION( "D" )
    {
        // one sample
        {
            const double dd[1][1] { { 0 } };
            const double rr[1][1] { { 1 } };
            double kk[1][1];

            kernel_matrix_gaussian_d(&kk[0][0], &dd[0][0], 1, 1, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 1*1) );
        }

        // two samples
        {
            // X = { { 1, -1 }, { 2, 3 } }
            const double dd[2][2] { {0, 17}, {17, 0} };
            const double rr[2][2] { { 1, std::exp(-17./2) }, { std::exp(-17./2), 1 } };
            double kk[2][2];

            kernel_matrix_gaussian_d(&kk[0][0], &dd[0][0], 2, 2, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // two samples
        {
            // X = { { 1, 2 }, { 3, 4 } }
            const double dd[2][2] { {0, 8}, {8, 0} };
            const double rr[2][2] { { 1, 1/std::exp(1.) }, { 1/std::exp(1), 1 } };
            double kk[2][2];

            kernel_matrix_gaussian_d(&kk[0][0], &dd[0][0], 2, 2, 2.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // Symmetry
        {
            test_kernel_matrix_d_symmetry(kernel_matrix_gaussian_d, distance_matrix_squared_euclidean_k,    2,   1,  1);
            test_kernel_matrix_d_symmetry(kernel_matrix_gaussian_d, distance_matrix_squared_euclidean_k,   20,  10,  2);
            test_kernel_matrix_d_symmetry(kernel_matrix_gaussian_d, distance_matrix_squared_euclidean_k,  100, 500, 13);
            test_kernel_matrix_d_symmetry(kernel_matrix_gaussian_d, distance_matrix_squared_euclidean_k, 1000, 100,  6);
        }
    }

    SECTION( "E" )
    {
        // one vs one
        {
            // X = { { 1 } }, Z = { { 2 } }
            const double dd[1][1] { { 1 } };
            const double rr[1][1] { { std::exp(-1.) } };
            double ll[1][1];

            kernel_matrix_gaussian_d(&ll[0][0], &dd[0][0], 1, 1, std::sqrt(0.5));
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // three vs two
        {
            // X = { {  1, -1 }, { 2, 3} , { -1, 2 } }, Z = { { -1, -1 }, { 4, 5} }
            const double dd[3][2] { { 4, 45 }, { 25, 8 }, { 9, 34 } };
            const double rr[3][2] { { std::exp(-2), std::exp(-45/2.) }, { std::exp(-25/2.), std::exp(-4) }, { std::exp(-9/2.), std::exp(-17.) } };
            double ll[3][2];

            kernel_matrix_gaussian_d(&ll[0][0], &dd[0][0], 3, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // two vs three
        {
            // X = { {  1, 2 }, { -1, 3} }, Z = { { -2, 1 }, {  0, 1} , { -1, 2 } }
            const double dd[2][3] { { 10, 2, 4 }, { 5, 5, 1 } };
            const double rr[2][3] { { std::exp(-10), std::exp(-2), std::exp(-4) }, { std::exp(-5), std::exp(-5), std::exp(-1) } };
            double ll[2][3];

            kernel_matrix_gaussian_d(&ll[0][0], &dd[0][0], 2, 3, std::sqrt(0.5));
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }
    }

    SECTION( "K" )
    {
        // one sample
        {
            const double xx[1][1] { { 1 } };
            const double rr[1][1] { { 1 } };
            double kk[1][1];

            kernel_matrix_gaussian_k(&kk[0][0], &xx[0][0], 1, 1, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 1*1) );
        }

        // two samples
        {
            const double xx[2][2] { { 1, -1 }, { 2, 3 } };
            const double rr[2][2] { { 1, std::exp(-17./2) }, { std::exp(-17./2), 1 } };
            double kk[2][2];

            kernel_matrix_gaussian_k(&kk[0][0], &xx[0][0], 2, 2, 1.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // two samples
        {
            const double xx[2][2] { { 1, 2 }, { 3, 4 } };
            const double rr[2][2] { { 1, 1/std::exp(1) }, { 1/std::exp(1), 1 } };
            double kk[2][2];

            kernel_matrix_gaussian_k(&kk[0][0], &xx[0][0], 2, 2, 2.);
            CHECK( equal_abs(&kk[0][0], &rr[0][0], 2*2) );
        }

        // Symmetry
        {
            test_kernel_matrix_k_symmetry(kernel_matrix_gaussian_k,    2,   1,  1);
            test_kernel_matrix_k_symmetry(kernel_matrix_gaussian_k,   20,  10,  2);
            test_kernel_matrix_k_symmetry(kernel_matrix_gaussian_k,  100, 500, 13);
            test_kernel_matrix_k_symmetry(kernel_matrix_gaussian_k, 1000, 100,  6);
        }
    }

    SECTION( "L" )
    {
        // one versus one
        {
            const double xx[1][1] { { 1 } };
            const double zz[1][1] { { 2 } };
            const double rr[1][1] { { std::exp(-1) } };
            double ll[1][1];

            kernel_matrix_gaussian_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 1, std::sqrt(0.5));
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // one versus one
        {
            const double xx[1][2] { { 1, -1 } };
            const double zz[1][2] { { 2,  3 } };
            const double rr[1][1] { { std::exp(-17/2.) } };
            double ll[1][1];

            kernel_matrix_gaussian_l(&ll[0][0], &xx[0][0], &zz[0][0], 1, 1, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 1*1) );
        }

        // three versus two
        {
            const double xx[3][2] { {  1, -1 }, { 2, 3} , { -1, 2 } };
            const double zz[2][2] { { -1, -1 }, { 4, 5} };
            const double rr[3][2] { { std::exp(-2), std::exp(-45/2.) }, { std::exp(-25/2.), std::exp(-4) }, { std::exp(-9/2.), std::exp(-17.) } };
            double ll[3][2];

            kernel_matrix_gaussian_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2, 1.);
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // three versus two
        {
            const double xx[3][2] { { -2, 1 }, {  0, 1} , { -1, 2 } };
            const double zz[2][2] { {  1, 2 }, { -1, 3} };
            const double rr[3][2] { { std::exp(-10), std::exp(-5) }, { std::exp(-2), std::exp(-5) }, { std::exp(-4), std::exp(-1) } };
            double ll[3][2];

            kernel_matrix_gaussian_l(&ll[0][0], &xx[0][0], &zz[0][0], 3, 2, 2, std::sqrt(0.5));
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 3*2) );
        }

        // two versus three
        {
            const double xx[2][2] { {  1, 2 }, { -1, 3 } };
            const double zz[3][2] { { -2, 1 }, {  0, 1 }, { -1, 2} };
            const double rr[2][3] { { std::exp(-10), std::exp(-2),  std::exp(-4) }, { std::exp(-5), std::exp(-5), std::exp(-1) } };
            double ll[2][3];

            kernel_matrix_gaussian_l(&ll[0][0], &xx[0][0], &zz[0][0], 2, 3, 2, std::sqrt(0.5));
            CHECK( equal_abs(&ll[0][0], &rr[0][0], 2*3) );
        }
    }

    SECTION( "M" )
    {
        // one sample, length 1
        {
            const double xx[1][1] { { 1 } };
            const double rr[1] { 1 };
            double mm[1];

            kernel_matrix_gaussian_m(&mm[0], &xx[0][0], 1, 1, 10.);
            CHECK( equal_abs(&mm[0], &rr[0], 1) );
        }

        // one sample, length 3
        {
            const double xx[1][3] { { 1, 2, 3 } };
            const double rr[1] { 1 };
            double mm[1];

            kernel_matrix_gaussian_m(&mm[0], &xx[0][0], 1, 3, 1.);
            CHECK( equal_abs(&mm[0], &rr[0], 1) );
        }

        // three samples
        {
            const double xx[3][2] { { 1, 2 }, { 3, 4 }, { 5, 6} };
            const double rr[3] { 1, 1, 1 };
            double mm[3];

            kernel_matrix_gaussian_m(&mm[0], &xx[0][0], 3, 2, 1.);
            CHECK( equal_abs(&mm[0], &rr[0], 3) );
        }
    }
}
