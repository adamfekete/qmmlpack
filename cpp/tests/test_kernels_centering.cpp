// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for centering kernel matrices.

#include "catch/catch.hpp"
#include "qmmlpack/kernels_centering.hpp"
#include "qmmlpack/kernels_linear.hpp"
#include "qmmlpack/kernels_gaussian.hpp"
#include "util.hpp"

using namespace qmml;

TEST_CASE("center_kernel_matrix", "[kernels]")
{
    SECTION( "simple examples" )
    {
        // four corners of a square in the plane (linear kernel)
        {
            // K
            const double X  [4][2] { {  0,  0 }, { 2,  0 }, { 2, 2 }, {  0, 2 } };  // training input, not centered
            const double Xc [4][2] { { -1, -1 }, { 1, -1 }, { 1, 1 }, { -1, 1 } };  // training input, centered

            double K [4][4]; kernel_matrix_linear_k(&K [0][0], &X [0][0], 4, 2);
            double Kc[4][4]; kernel_matrix_linear_k(&Kc[0][0], &Xc[0][0], 4, 2);

            double Kr[4][4];

            center_kernel_matrix_k(&Kr[0][0], &K[0][0], 4);
            CHECK( equal_abs(&Kr[0][0], &Kc[0][0], 4*4) );

            // L
            const double XX [5][2] { {  1,  0 }, { 2,  1 }, { 1, 2 }, {  0, 1 }, { 1, 1 } };  // test input, not centered
            const double XXc[5][2] { {  0, -1 }, { 1,  0 }, { 0, 1 }, { -1, 0 }, { 0, 0 } };  // test input, centered

            double L [4][5]; kernel_matrix_linear_l(&L [0][0], &X [0][0], &XX [0][0], 4, 5, 2);
            double Lc[4][5]; kernel_matrix_linear_l(&Lc[0][0], &Xc[0][0], &XXc[0][0], 4, 5, 2);

            double Lr[4][5];

            center_kernel_matrix_l(&Lr[0][0], &K[0][0], &L[0][0], 4, 5);
            CHECK( equal_abs(&Lr[0][0], &Lc[0][0], 4*5) );

            // M
            double M [5][5]; kernel_matrix_linear_k(&M [0][0], &XX [0][0], 5, 2);
            double Mc[5][5]; kernel_matrix_linear_k(&Mc[0][0], &XXc[0][0], 5, 2);

            double Mr[5][5];

            center_kernel_matrix_m(&Mr[0][0], &K[0][0], &L[0][0], &M[0][0], 4, 5);
            CHECK( equal_abs(&Mr[0][0], &Mc[0][0], 5*5) );

            // diag(M)
            double m [5]; kernel_matrix_linear_m(&m [0], &XX [0][0], 5, 2);
            double mc[5]; kernel_matrix_linear_m(&mc[0], &XXc[0][0], 5, 2);

            double mr[5];

            center_kernel_matrix_mdiag(&mr[0], &K[0][0], &L[0][0], &m[0], 4, 5);
            CHECK( equal_abs(&mr[0], &mc[0], 5) );
        }

        // already centered matrix
        {
            const double X [4][2] { { 1, 0 }, { 0, 1 }, { -1, 0 }, {  0, -1 } };  // training input, already centered
            const double XX[5][2] { { 0, 0 }, { 1, 1 }, { -1, 1 }, { -1, -1 }, { 1, -1 } };  // test input

            // K
            double K [4][4]; kernel_matrix_linear_k(&K [0][0], &X [0][0], 4, 2);
            double Kr[4][4];

            center_kernel_matrix_k(&Kr[0][0], &K[0][0], 4);
            CHECK( equal_abs(&Kr[0][0], &K[0][0], 4*4) );

            // L
            double L [4][5]; kernel_matrix_linear_l(&L [0][0], &X [0][0], &XX [0][0], 4, 5, 2);
            double Lr[4][5];

            center_kernel_matrix_l(&Lr[0][0], &K[0][0], &L[0][0], 4, 5);
            CHECK( equal_abs(&Lr[0][0], &L[0][0], 4*5) );

            // M
            double M [5][5]; kernel_matrix_linear_k(&M [0][0], &XX [0][0], 5, 2);
            double Mr[5][5];

            center_kernel_matrix_m(&Mr[0][0], &K[0][0], &L[0][0], &M[0][0], 4, 5);
            CHECK( equal_abs(&Mr[0][0], &M[0][0], 5*5) );

            // diag(M)
            double m [5]; kernel_matrix_linear_m(&m [0], &XX [0][0], 5, 2);
            double mr[5];

            center_kernel_matrix_mdiag(&mr[0], &K[0][0], &L[0][0], &m[0], 4, 5);
            CHECK( equal_abs(&mr[0], &m[0], 5) );
        }
    }

    SECTION( "linear kernel", "[kernels]")
    {
        // explicit centering
        {
            const double X[5][7] // random 5 x 7 matrix via RandomVariate[UniformDistribution[{-1, 1}], {5, 7}]
            {
                { 0.4083490992163403, -0.4722036310884134, -0.7133467805491098,  0.2247836461102847, -0.719503409363579, -0.1069898523366137, -0.05469955030632},
                { 0.4958288968614499, -0.3747408826635814, -0.9999212253004539,  0.5205869933588789,  0.519199845236859, -0.0018685879162445, -0.24011250592445},
                {-0.6215029121172009,  0.1311954712172514, -0.6608474349547375, -0.8874563863238518, -0.438787870208581, -0.1874539932402448, -0.41225051671516},
                { 0.0263832300394972, -0.9191075197464689, -0.3601131791319156, -0.7209818217983011, -0.920836529436206, -0.6400415960479342, -0.82831651693080},
                { 0.5370991473711153,  0.0917310483499074, -0.6163690913164408, -0.9216996195139551,  0.176330121310050,  0.8725666376878318, -0.03316446640701}
            };
            const double Xc[5][7] // centered via Standardize[xx, Mean, 1 &]
            {
                { 0.23911760694209994, -0.16357852830215242, -0.043227238298578330,  0.58173708374367350, -0.44278384087128764, -0.094232373965972630,  0.25900916095042800},
                { 0.32659740458720950, -0.06611577987732042, -0.329801683049922400,  0.87754043099226780,  0.79591941372915040,  0.010888890454396572,  0.07359620533229799},
                {-0.79073440439144130,  0.43982057400351240,  0.009272107295793974, -0.53050294869046300, -0.16206830171628960, -0.174696514869603750, -0.09854180545841201},
                {-0.14284826223474320, -0.61048241696020790,  0.310006363118615900, -0.36402838416491223, -0.64411696094391460, -0.627284117677293100, -0.51460780567405200},
                { 0.36786765509687490,  0.40035615113616840,  0.053750450934090700, -0.56474618188056620,  0.45304968980234140,  0.885324116058472900,  0.28054424484973800}
            };
            double K [5][5]; kernel_matrix_linear_k(&K [0][0], &X [0][0], 5, 7);
            double Kc[5][5]; kernel_matrix_linear_k(&Kc[0][0], &Xc[0][0], 5, 7);
            double Kr[5][5];

            center_kernel_matrix_k(&Kr[0][0], &K[0][0], 5);
            CHECK( equal_abs(&Kr[0][0], &Kc[0][0], 5*5) );
        }
    }

    SECTION( "symmetry", "[kernels]")
    {
        // from linear kernel
        {
            const size_t n = 100; const size_t d = 50;

            double *const xx = new double[n*d];
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < d; ++j) xx[i*d+j] = random_uniform_real(-1, 1);
            double kk[n][n];
            kernel_matrix_linear_k(&kk[0][0], xx, n, d);
            delete[] xx;

            double rr[n][n];
            center_kernel_matrix_k(&rr[0][0], &kk[0][0], n);

            CHECK( is_symmetric_matrix(&rr[0][0], n) );
        }

        // from Gaussian kernel
        {
            const size_t n = 100; const size_t d = 50;

            double *const xx = new double[n*d];
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < d; ++j) xx[i*d+j] = random_uniform_real(-1, 1);
            double kk[n][n];
            kernel_matrix_gaussian_k(&kk[0][0], xx, n, d, 5.);
            delete[] xx;

            double rr[n][n];
            center_kernel_matrix_k(&rr[0][0], &kk[0][0], n);

            CHECK( is_symmetric_matrix(&rr[0][0], n) );
        }
    }
}
