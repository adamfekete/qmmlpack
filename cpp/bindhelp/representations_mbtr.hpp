// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// C++ binding helpers for many-body tensor representation
// This source file to be directly included from bindings C++ source files (.cpp)

// The parametrization of the MBTR needs to be translated from a dynamic language (Mathematica, Python) to compile-time C++ calls.
// This happens stagewise for each template parameter. Invalid combinations are catched early on to avoid instantiating too many templates,
// reducing object file size from 28 MB to 4 MB, with corresponding reduction in compile time.

#ifndef QMML_CPP_BINDINGS_REPRESENTATIONS_MANYBODYTENSOR_HPP_INCLUDED  // include guard
#define QMML_CPP_BINDINGS_REPRESENTATIONS_MANYBODYTENSOR_HPP_INCLUDED

#include <map>
#include <vector>
#include <type_traits>
#include <sstream>

#include "qmmlpack/representations_mbtr.hpp"

namespace bh {

using std::string;
using std::enable_if_t;
using namespace qmml;

template<typename Int=long>
struct mbtr_params
{
    mbtr_params
    (
        long const           k       ,
        double * const       mbtr    ,
        long const           ns      ,
        Int const*const      ssizes  ,
        Int const*const      sinds   ,
        double const*const   bases   ,
        Int const*const      zz      ,
        double const*const   rr      ,
        long const           ne      ,
        Int const*const      elems   ,
        double const         xmin    ,
        double const         deltax  ,
        long const           xdim    ,
        string const &       geomfs  ,
        long const           geomfn  ,
        double const * const geomfp  ,
        string const &       weightfs,
        long const           weightfn,
        double const * const weightfp,
        string const &       distrfs ,
        long const           distrfn ,
        double const * const distrfp ,
        string const &       corrfs  ,
        long const           corrfn  ,
        double const * const corrfp  ,
        string const &       eindexfs,
        long const           eindexfn,
        double const * const eindexfp,
        string const &       aindexfs,
        long const           aindexfn,
        double const * const aindexfp,
        double const         acc
    )
    : k(k), mbtr(mbtr), ns(ns), ssizes(ssizes), sinds(sinds), bases(bases), zz(zz), rr(rr), ne(ne), elems(elems), xmin(xmin), deltax(deltax), xdim(xdim),
      geomfs(geomfs), geomfn(geomfn), geomfp(geomfp), weightfs(weightfs), weightfn(weightfn), weightfp(weightfp), distrfs(distrfs), distrfn(distrfn), distrfp(distrfp),
      corrfs(corrfs), corrfn(corrfn), corrfp(corrfp), eindexfs(eindexfs), eindexfn(eindexfn), eindexfp(eindexfp), aindexfs(aindexfs), aindexfn(aindexfn), aindexfp(aindexfp),
      acc(acc)
    {}

    long const           k;
    double *             mbtr;
    long const           ns;
    Int const*const      ssizes;
    Int const*const      sinds;
    double const*const   bases;
    Int const*const      zz;
    double const*const   rr;
    long const           ne;
    Int const*const      elems;
    double const         xmin;
    double const         deltax;
    long const           xdim;
    string const &       geomfs;
    long const           geomfn;
    double const * const geomfp;
    string const &       weightfs;
    long const           weightfn;
    double const * const weightfp;
    string const &       distrfs;
    long const           distrfn;
    double const * const distrfp ;
    string const &       corrfs;
    long const           corrfn;
    double const * const corrfp;
    string const &       eindexfs;
    long const           eindexfn;
    double const * const eindexfp;
    string const &       aindexfs;
    long const           aindexfn;
    double const * const aindexfp;
    double const         acc;

    // return values
    long mbtr_ndim;       // number of dimensions of result tensor
    long mbtr_shape[6];   // shape of result tensor. at most 1 (n_s) + 1 (ddim) + k, where k <= 4.
    long mbtr_strides[6]; // strides (in bytes)
    long mbtr_size;       // size of result memory block in number of doubles

    std::string debug() const
    {
        using std::vector; using std::endl;
        std::ostringstream s;
        char const* sep;

        s << "MBTR parametrization:" << endl;
        s << "  k = " << k << endl;
        s << "  geomf   = " << geomfs  ; if(geomfn   > 0) { s << " ("; for(int i = 0; i < geomfn  ; ++i) s << geomfp  [i] << (i == geomfn  -1 ? "" : " "); s << ")"; }; s << endl;
        s << "  weightf = " << weightfs; if(weightfn > 0) { s << " ("; for(int i = 0; i < weightfn; ++i) s << weightfp[i] << (i == weightfn-1 ? "" : " "); s << ")"; }; s << endl;
        s << "  distrf  = " << distrfs ; if(distrfn  > 0) { s << " ("; for(int i = 0; i < distrfn ; ++i) s << distrfp [i] << (i == distrfn -1 ? "" : " "); s << ")"; }; s << endl;
        s << "  corrf   = " << corrfs  ; if(corrfn   > 0) { s << " ("; for(int i = 0; i < corrfn  ; ++i) s << corrfp  [i] << (i == corrfn  -1 ? "" : " "); s << ")"; }; s << endl;
        s << "  eindexf = " << eindexfs; if(eindexfn > 0) { s << " ("; for(int i = 0; i < eindexfn; ++i) s << eindexfp[i] << (i == eindexfn-1 ? "" : " "); s << ")"; }; s << endl;
        s << "  aindexf = " << aindexfs; if(aindexfn > 0) { s << " ("; for(int i = 0; i < aindexfn; ++i) s << aindexfp[i] << (i == aindexfn-1 ? "" : " "); s << ")"; }; s << endl;
        s << endl;
        s << "  ns = "  << ns << endl;
        s << "  ne = " << ne << "; els = "; for(auto i : vector<Int>(elems, elems+ne)) s << i << " "; s << endl;
        s << "  ssizes = ("; sep = ""; for(auto i : vector<Int>(ssizes, ssizes+ns)) { s << sep << i; sep = " "; }; s << ")" << endl;
        s << "  sinds  = ("; sep = ""; for(auto i : vector<Int>(sinds , sinds +ns)) { s << sep << i; sep = " "; }; s << ")" << endl;
        s << "  zz = "; for(long i = 0; i < ns; ++i) { s << endl << "    "; for(long j = 0; j < ssizes[i]; ++j) s << zz[sinds[i]+j] << " "; } s << endl;
        s << "  rr = "; for(long i = 0; i < ns; ++i) { s << endl << "    "; for(long j = 0; j < ssizes[i]; ++j) { s << rr[(sinds[i]+j)*3+0] << "  " << rr[(sinds[i]+j)*3+1] << "  " << rr[(sinds[i]+j)*3+2]; if(!(i == ns-1 && j == ssizes[i]-1)) s << endl << "  "; } } s << endl;
        if(bases == nullptr) s << "no basis set specified." << endl; else { s << "  bases = "; for(long i = 0; i < ns; ++i) { s << endl << "    "; for(long j = 0; j < 3; ++j) { s << bases[(sinds[i]+j)*3+0] << " " << bases[(sinds[i]+j)*3+1] << " " << bases[(sinds[i]+j)*3+2]; if(!(i == ns-1 && j == 2)) s << endl << "    "; } } s << endl; }
        s << endl;
        s << "  mbtr_size    = " << mbtr_size << endl;
        s << "  mbtr_ndim    = " << mbtr_ndim << endl;
        s << "  mbtr_shape   = ("; sep = ""; for(long i = 0; i < mbtr_ndim; ++i) { s << sep << mbtr_shape  [i]; sep = " "; }; s << ")" << endl;
        s << "  mbtr_strides = ("; sep = ""; for(long i = 0; i < mbtr_ndim; ++i) { s << sep << mbtr_strides[i]; sep = " "; }; s << ")" << endl;
        s << endl;
        s << "  mbtr = "; for(auto i : vector<double>(mbtr, mbtr+mbtr_size)) s << i << " "; s << endl;

        return s.str();
    }
};

// invalid parametrizations of the atom indexing function end here
template<bool V, int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
enable_if_t<!V, void> call_mbtr(mbtr_params<Int> &, G const &, W const &, D const &, C const &, E const &, A const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of atom indexing function.");
}

template<bool V, int K, typename G, typename W, typename D, typename C, typename E, typename A, typename Int>
enable_if_t<V, void> call_mbtr(mbtr_params<Int> & p, G const & geomf, W const & weightf, D const & distrf, C const & corrf, E const & eindexf, A const & aindexf)
{
    many_body_tensor_representation<K, G, W, D, C, E, A, Int> mbtr(p.mbtr, p.ns, p.ssizes, p.sinds, p.bases, p.zz, p.rr, p.ne, p.elems, p.xmin, p.deltax, p.xdim, geomf, weightf, distrf, corrf, eindexf, aindexf, p.acc);
    p.mbtr_size = mbtr.mbtr_size();

    // if no result memory is provided, set it up
    if( p.mbtr == nullptr )
    {
        p.mbtr = new double[p.mbtr_size];
        mbtr.mbtr(p.mbtr);
    }

    mbtr.compute();
}

// invalid parametrizations of the element indexing function end here
template<bool V, int K, typename G, typename W, typename D, typename C, typename E, typename Int>
enable_if_t<!V, void> call_mbtr_select_aindexf(mbtr_params<Int> &, G const &, W const &, D const &, C const &, E const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of element indexing function.");
}

template<bool V, int K, typename G, typename W, typename D, typename C, typename E, typename Int>
enable_if_t<V, void> call_mbtr_select_aindexf(mbtr_params<Int> & p, G const & geomf, W const & weightf, D const & distrf, C const & corrf, E const & eindexf)
{
    enum values { undefined = 0, finite_full, finite_noreversals, periodic_full, periodic_noreversals };
    static std::map<const string, values> map {
        { "finite_full"         , finite_full          },
        { "finite_noreversals"  , finite_noreversals   },
        { "periodic_full"       , periodic_full        },
        { "periodic_noreversals", periodic_noreversals }
    };
    const long pownek = static_cast<long>(std::pow(p.ne, K));  // ne ^ K
    switch( map[p.aindexfs] )
    {
        case finite_full:
            p.mbtr_ndim = 3;
            p.mbtr_shape[0] = p.ns;
            p.mbtr_shape[1] = pownek;
            p.mbtr_shape[2] = p.xdim;
            p.mbtr_strides[0] = p.mbtr_shape[1] * p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[1] = p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[2] = sizeof(double);
            if(p.bases != nullptr) throw QMML_ERROR("mbtr: finite atom indexing function specified for periodic systems");
            call_mbtr<V, K, G, W, D, C, E, mbtr_aindexf_finite_full<Int>, Int>(p, geomf, weightf, distrf, corrf, eindexf, mbtr_aindexf_finite_full<Int>());
            break;
        case finite_noreversals:
            p.mbtr_ndim = 3;
            p.mbtr_shape[0] = p.ns;
            p.mbtr_shape[1] = static_cast<long>( 0.5 * ( pownek + std::pow(p.ne, std::ceil(0.5*K)) ) );
            p.mbtr_shape[2] = p.xdim;
            p.mbtr_strides[0] = p.mbtr_shape[1] * p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[1] = p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[2] = sizeof(double);
            if(p.bases != nullptr) throw QMML_ERROR("mbtr: finite atom indexing function specified for periodic systems");
            call_mbtr<V, K, G, W, D, C, E, mbtr_aindexf_finite_noreversals<Int>, Int>(p, geomf, weightf, distrf, corrf, eindexf, mbtr_aindexf_finite_noreversals<Int>());
            break;
        case periodic_full:
            p.mbtr_ndim = 3;
            p.mbtr_shape[0] = p.ns;
            p.mbtr_shape[1] = pownek;
            p.mbtr_shape[2] = p.xdim;
            p.mbtr_strides[0] = p.mbtr_shape[1] * p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[1] = p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[2] = sizeof(double);
            if(p.bases == nullptr) throw QMML_ERROR("mbtr: periodic atom indexing function specified for finite systems");
            call_mbtr<V, K, G, W, D, C, E, mbtr_aindexf_periodic_full<Int>, Int>(p, geomf, weightf, distrf, corrf, eindexf, mbtr_aindexf_periodic_full<Int>());
            break;
        case periodic_noreversals:
            p.mbtr_ndim = 3;
            p.mbtr_shape[0] = p.ns;
            p.mbtr_shape[1] = static_cast<long>( 0.5 * ( pownek + std::pow(p.ne, std::ceil(0.5*K)) ) );
            p.mbtr_shape[2] = p.xdim;
            p.mbtr_strides[0] = p.mbtr_shape[1] * p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[1] = p.mbtr_shape[2] * sizeof(double);
            p.mbtr_strides[2] = sizeof(double);
            if(p.bases == nullptr) throw QMML_ERROR("mbtr: periodic atom indexing function specified for finite systems");
            call_mbtr<V, K, G, W, D, C, E, mbtr_aindexf_periodic_noreversals<Int>, Int>(p, geomf, weightf, distrf, corrf, eindexf, mbtr_aindexf_periodic_noreversals<Int>());
            break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for atom indexing function.");
    }
}

// invalid parametrizations of the correlation function end here
template<bool V, int K, typename G, typename W, typename D, typename C, typename Int>
enable_if_t<!V, void> call_mbtr_select_eindexf(mbtr_params<Int> const &, G const &, W const &, D const &, C const &, D const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of correlation function.");
}

template<bool V, int K, typename G, typename W, typename D, typename C, typename Int>
enable_if_t<V, void> call_mbtr_select_eindexf(mbtr_params<Int> & p, G const & geomf, W const & weightf, D const & distrf, C const & corrf)
{
    enum values { undefined = 0, dense_full, dense_noreversals };
    static std::map<const string, values> map {
        { "dense_full"       , dense_full        },
        { "dense_noreversals", dense_noreversals }
    };
    switch( map[p.eindexfs] )
    {
        case dense_full:
            call_mbtr_select_aindexf<V, K, G, W, D, C, mbtr_eindexf_dense_full, Int>(p, geomf, weightf, distrf, corrf, mbtr_eindexf_dense_full()); break;
        case dense_noreversals:
            call_mbtr_select_aindexf<V, K, G, W, D, C, mbtr_eindexf_dense_noreversals, Int>(p, geomf, weightf, distrf, corrf, mbtr_eindexf_dense_noreversals()); break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for element indexing function.");
    }
}

// invalid parametrizations of the distribution function end here
template<bool V, int K, typename G, typename W, typename D, typename Int>
enable_if_t<!V, void> call_mbtr_select_corrf(mbtr_params<Int> const &, G const &, W const &, D const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of distribution function.");
}

template<bool V, int K, typename G, typename W, typename D, typename Int>
enable_if_t<V, void> call_mbtr_select_corrf(mbtr_params<Int> & p, G const & geomf, W const & weightf, D const & distrf)
{
    enum values { undefined = 0, identity };
    static std::map<const string, values> map {
        { "identity", identity }
    };
    switch( map[p.corrfs] )
    {
        case identity:
            call_mbtr_select_eindexf<V, K, G, W, D, mbtr_identity_correlation, Int>(p, geomf, weightf, distrf, mbtr_identity_correlation()); break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for correlation function.");
    }
}

// invalid parametrizations of the weighting function end here
template<bool V, int K, typename G, typename W, typename Int>
enable_if_t<!V, void> call_mbtr_select_distrf(mbtr_params<Int> const &, G const &, W const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of weighting function.");
}

template<bool V, int K, typename G, typename W, typename Int>
enable_if_t<V, void> call_mbtr_select_distrf(mbtr_params<Int> & p, G const & geomf, W const & weightf)
{
    enum values { undefined = 0, normal };
    static std::map<const string, values> map {
        { "normal", normal }
    };
    switch( map[p.distrfs] )
    {
        case normal:
            {
                if(p.distrfn != 1) throw QMML_ERROR("Many-body tensor representation: distribution function normal requires one parameter");
                mbtr_normal_distribution distrf(*p.distrfp);
                call_mbtr_select_corrf<V, K, G, W, mbtr_normal_distribution, Int>(p, geomf, weightf, distrf);
            }
            break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for distribution function.");
    }
}

// invalid parametrizations of the geometry function end here
template<bool V, int K, typename G, typename Int>
enable_if_t<!V, void> call_mbtr_select_weightf(mbtr_params<Int> const &, G const &)
{
    throw QMML_ERROR("Many-body tensor representation: Invalid parametrization of geometry function.");
}

template<bool V, int K, typename G, typename Int>
enable_if_t<V, void> call_mbtr_select_weightf(mbtr_params<Int> & p, G const & geomf)
{
    enum values { undefined = 0, unity, identity, identity_squared, identity_root, one_over_identity, delta_one_over_identity, exp_neg_one_over_identity, exp_neg_one_over_identity_squared, one_over_count, one_over_normnorm, one_over_dotdot, one_over_normnormnorm, one_over_dotdotdot, exp_neg_one_over_normnormnorm, exp_neg_one_over_sum_normnormnorm };
    static std::map<const string, values> map {
        { "unity"                , unity },
        { "identity"             , identity },
        { "identity^2"           , identity_squared },
        { "identity_root"        , identity_root },
        { "1/identity"           , one_over_identity },
        { "delta_1/identity"     , delta_one_over_identity },
        { "exp_-1/identity"      , exp_neg_one_over_identity },
        { "exp_-1/identity^2"    , exp_neg_one_over_identity_squared },
        { "1/count"              , one_over_count },
        { "1/normnorm"           , one_over_normnorm },
        { "1/dotdot"             , one_over_dotdot },
        { "1/normnormnorm"       , one_over_normnormnorm },
        { "1/dotdotdot"          , one_over_dotdotdot },
        { "exp_-1/normnormnorm"  , exp_neg_one_over_normnormnorm },
        { "exp_-1/norm+norm+norm", exp_neg_one_over_sum_normnormnorm }
    };
    switch( map[p.weightfs] )
    {
        case unity:
            call_mbtr_select_distrf<V, K, G, mbtr_weightf_unity, Int>(p, geomf, mbtr_weightf_unity());
            break;
        case identity:
            call_mbtr_select_distrf<V, K, G, mbtr_weightf_identity, Int>(p, geomf, mbtr_weightf_identity());
            break;
        case identity_squared:
            call_mbtr_select_distrf<V, K, G, mbtr_weightf_identity_squared, Int>(p, geomf, mbtr_weightf_identity_squared());
            break;
        case identity_root:
            call_mbtr_select_distrf<V, K, G, mbtr_weightf_identity_root, Int>(p, geomf, mbtr_weightf_identity_root());
            break;
        case one_over_identity:
            call_mbtr_select_distrf<V, K, G, mbtr_weightf_one_over_identity, Int>(p, geomf, mbtr_weightf_one_over_identity());
            break;
        case delta_one_over_identity:
            {
                if(p.weightfn != 1) throw QMML_ERROR("Many-body tensor representation: weighting function delta_one_over_identity requires one parameter");
                mbtr_weightf_delta_one_over_identity weightf(*p.weightfp);
                call_mbtr_select_distrf<V, K, G, mbtr_weightf_delta_one_over_identity, Int>(p, geomf, weightf);
            }
            break;
        case exp_neg_one_over_identity:
            {
                if(p.weightfn != 1) throw QMML_ERROR("Many-body tensor representation: weighting function exp_neg_one_over_identity requires one parameter");
                mbtr_weightf_exp_neg_one_over_identity weightf(*p.weightfp);
                call_mbtr_select_distrf<V, K, G, mbtr_weightf_exp_neg_one_over_identity, Int>(p, geomf, weightf);
            }
            break;
        case exp_neg_one_over_identity_squared:
            {
                if(p.weightfn != 1) throw QMML_ERROR("Many-body tensor representation: weighting function exp_neg_one_over_identity_squared requires one parameter");
                mbtr_weightf_exp_neg_one_over_identity_squared weightf(*p.weightfp);
                call_mbtr_select_distrf<V, K, G, mbtr_weightf_exp_neg_one_over_identity_squared, Int>(p, geomf, weightf);
            }
            break;
        case one_over_count:
            call_mbtr_select_distrf<V && K == 1, K, G, mbtr_weightf1_one_over_count<Int>, Int>(p, geomf, mbtr_weightf1_one_over_count<Int>());
            break;
        case one_over_normnorm:
            call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_one_over_normnorm, Int>(p, geomf, mbtr_weightf3_one_over_normnorm());
            break;
        case one_over_dotdot:
            call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_one_over_dotdot, Int>(p, geomf, mbtr_weightf3_one_over_dotdot());
            break;
        case one_over_normnormnorm:
            call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_one_over_normnormnorm, Int>(p, geomf, mbtr_weightf3_one_over_normnormnorm());
            break;
        case one_over_dotdotdot:
            call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_one_over_dotdotdot, Int>(p, geomf, mbtr_weightf3_one_over_dotdotdot());
            break;
        case exp_neg_one_over_normnormnorm:
            {
                if(p.weightfn != 1) throw QMML_ERROR("Many-body tensor representation: weighting function exp_neg_one_over_normnormnorm requires one parameter");
                mbtr_weightf3_exp_neg_one_over_normnormnorm weightf(*p.weightfp);
                call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_exp_neg_one_over_normnormnorm, Int>(p, geomf, weightf);
            }
            break;
        case exp_neg_one_over_sum_normnormnorm:
            {
                if(p.weightfn != 1) throw QMML_ERROR("Many-body tensor representation: weighting function exp_neg_one_over_sum_normnormnorm requires one parameter");
                mbtr_weightf3_exp_neg_one_over_sum_normnormnorm weightf(*p.weightfp);
                call_mbtr_select_distrf<V && K == 3, K, G, mbtr_weightf3_exp_neg_one_over_sum_normnormnorm, Int>(p, geomf, weightf);
            }
            break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for weighting function.");
    }
}

template<int K, typename Int>
void call_mbtr_select_geomf(mbtr_params<Int> & p)
{
    enum values { undefined = 0, unity, count, one_over_distance, one_over_dot, angle, cos_angle, dot_over_dotdot, dihedral_angle, cos_dihedral_angle };
    static std::map<const string, values> map {
        { "unity"       , unity },
        { "count"       , count },
        { "1/distance"  , one_over_distance },
        { "1/dot"       , one_over_dot },
        { "angle"       , angle },
        { "cos_angle"   , cos_angle },
        { "dot/dotdot"  , dot_over_dotdot },
        { "dihedral"    , dihedral_angle },
        { "cos_dihedral", cos_dihedral_angle }
    };
    switch( map[p.geomfs] )
    {
        case unity:
            call_mbtr_select_weightf<true, K, mbtr_geomf_unity, Int>(p, mbtr_geomf_unity());
            break;
        case count:
            call_mbtr_select_weightf<K==1, K, mbtr_geomf1_count<Int>, Int>(p, mbtr_geomf1_count<Int>());
            break;
        case one_over_distance:
            call_mbtr_select_weightf<K==2, K, mbtr_geomf2_one_over_distance, Int>(p, mbtr_geomf2_one_over_distance());
            break;
        case one_over_dot:
            call_mbtr_select_weightf<K==2, K, mbtr_geomf2_one_over_dot, Int>(p, mbtr_geomf2_one_over_dot());
            break;
        case angle:
            call_mbtr_select_weightf<K==3, K, mbtr_geomf3_angle, Int>(p, mbtr_geomf3_angle());
            break;
        case cos_angle:
            call_mbtr_select_weightf<K==3, K, mbtr_geomf3_cos_angle, Int>(p, mbtr_geomf3_cos_angle());
            break;
        case dot_over_dotdot:
            call_mbtr_select_weightf<K==3, K, mbtr_geomf3_dot_over_dotdot, Int>(p, mbtr_geomf3_dot_over_dotdot());
            break;
        case dihedral_angle:
            call_mbtr_select_weightf<K==4, K, mbtr_geomf4_dihedral_angle, Int>(p, mbtr_geomf4_dihedral_angle());
            break;
        case cos_dihedral_angle:
            call_mbtr_select_weightf<K==4, K, mbtr_geomf4_cos_dihedral_angle, Int>(p, mbtr_geomf4_cos_dihedral_angle());
            break;
        case undefined:
        default:
            throw QMML_ERROR("Unknown string identifier for geometry function.");
    }
}

// translates dynamic specification of geometry function, weighting function, ..., atom indexing function into static calls
template<typename Int>
void call_many_body_tensor(mbtr_params<Int> & p)
{
    switch(p.k)
    {
        case 1: call_mbtr_select_geomf<1, Int>(p); break;
        case 2: call_mbtr_select_geomf<2, Int>(p); break;
        case 3: call_mbtr_select_geomf<3, Int>(p); break;
        case 4: call_mbtr_select_geomf<4, Int>(p); break;
        default: throw QMML_ERROR("Only up to 4-body terms are supported.");
    }
}

}  // namespace bh

#endif // include guard
