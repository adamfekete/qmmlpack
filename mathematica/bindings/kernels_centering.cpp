// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings for centering of kernel matrices.

#include "mathematica.hpp"
#include "qmmlpack/kernels_centering.hpp"

//  ///////////////////////
//  //  Kernel matrix K  //
//  ///////////////////////

//
//  Centers a kernel matrix of samples versus themselves.
//
//  Centering is done in feature space.
//
//  Input:
//    [0] {Real,2,"Constant"} n x n kernel matrix K.
//  Output:
//    [0] {Real,2} n x n centered kernel matrix.
//

EXTERN_C DLLEXPORT int qmml_center_kernel_matrix_k(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) { libdata->Message("qmml_center_kernel_matrix_k: wrong number of arguments."); return LIBRARY_FUNCTION_ERROR; }

    MTensor K = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(K) != 2 ) { libdata->Message("qmml_center_kernel_matrix_k: Input matrix K has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Kdims = libdata->MTensor_getDimensions(K);
    const mint n = Kdims[0];
    if( Kdims[1] != n ) { libdata->Message("qmml_center_kernel_matrix_k: Input matrix K not quadratic."); return LIBRARY_DIMENSION_ERROR; }

    MTensor Kc;
    const mint Kcdims[2] = {n, n};
    int err; if( ( err = libdata->MTensor_new(MType_Real, 2, Kcdims, &Kc) ) ) { libdata->Message("qmml_center_kernel_matrix_k: creation of result matrix failed."); return err; }

    // Calculation
    try
    {
        qmml::center_kernel_matrix_k
        (
            libdata->MTensor_getRealData(Kc),
            libdata->MTensor_getRealData(K),
            n
        );
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, Kc);
    return LIBRARY_NO_ERROR;
}


//  ///////////////////////
//  //  Kernel matrix L  //
//  ///////////////////////

//
//  Centers a kernel matrix of samples versus other samples.
//
//  Centering is done in feature space.
//
//  Input:
//    [0] {Real,2,"Constant"} n x n kernel matrix K.
//    [1] {Real,2,"Constant"} n x m kernel matrix L.
//  Output:
//    [0] {Real,2} n x m centered kernel matrix.
///

EXTERN_C DLLEXPORT int qmml_center_kernel_matrix_l(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) { libdata->Message("qmml_center_kernel_matrix_l: wrong number of arguments."); return LIBRARY_FUNCTION_ERROR; }

    MTensor K = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(K) != 2 ) { libdata->Message("qmml_center_kernel_matrix_l: Input matrix K has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Kdims = libdata->MTensor_getDimensions(K);
    const mint n = Kdims[0];
    if( Kdims[1] != n ) { libdata->Message("qmml_center_kernel_matrix_l: Input matrix K is not quadratic."); return LIBRARY_DIMENSION_ERROR; }

    MTensor L = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(L) != 2 ) { libdata->Message("qmml_center_kernel_matrix_l: Input matrix L has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint m = Ldims[1];
    if( Ldims[0] != n ) { libdata->Message("qmml_center_kernel_matrix_l: Input matrix L has incompatible dimensions."); return LIBRARY_DIMENSION_ERROR; }

    MTensor Lc;
    const mint Lcdims[2] = {n, m};
    int err; if( ( err = libdata->MTensor_new(MType_Real, 2, Lcdims, &Lc) ) ) { libdata->Message("qmml_center_kernel_matrix_l: creating result matrix failed."); return err; }

    // Calculation
    try
    {
        qmml::center_kernel_matrix_l
        (
            libdata->MTensor_getRealData(Lc),
            libdata->MTensor_getRealData(K),
            libdata->MTensor_getRealData(L),
            n, m
        );
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, Lc);
    return LIBRARY_NO_ERROR;
}


//  ///////////////////////
//  //  Kernel matrix M  //
//  ///////////////////////

//
//  Centers a kernel matrix of samples versus themselves, with respect to other samples.
//
//  Centering is done in feature space.
//
//  Input:
//    [0] {Real,2,"Constant"} n x n kernel matrix K.
//    [1] {Real,2,"Constant"} n x m kernel matrix L.
//    [2] {Real,2,"Constant"} m x m kernel matrix M.
//  Output:
//    [0] {Real,2} m x m centered kernel matrix.
//

EXTERN_C DLLEXPORT int qmml_center_kernel_matrix_m(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    if( argc != 3 ) { libdata->Message("qmml_center_kernel_matrix_m: wrong number of arguments."); return LIBRARY_FUNCTION_ERROR; }

    MTensor K = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(K) != 2 ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix K has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Kdims = libdata->MTensor_getDimensions(K);
    const mint n = Kdims[0];
    if( Kdims[1] != n ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix K is not quadratic."); return LIBRARY_DIMENSION_ERROR; }

    MTensor L = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(L) != 2 ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix L has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint m = Ldims[1];
    if( Ldims[0] != n ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix L has incompatible dimensions."); return LIBRARY_DIMENSION_ERROR; }

    MTensor M = MArgument_getMTensor(args[2]);
    if( libdata->MTensor_getRank(M) != 2 ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix M has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Mdims = libdata->MTensor_getDimensions(M);
    if( Mdims[0] != m || Mdims[1] != m ) { libdata->Message("qmml_center_kernel_matrix_m: input matrix M has incompatible dimensions."); return LIBRARY_DIMENSION_ERROR; }

    MTensor Mc;
    const mint Mcdims[2] = {m, m};
    int err; if( ( err = libdata->MTensor_new(MType_Real, 2, Mcdims, &Mc) ) ) { libdata->Message("qmml_center_kernel_matrix_m: creation of result matrix failed."); return err; }

    // Calculation
    try
    {
        qmml::center_kernel_matrix_m
        (
            libdata->MTensor_getRealData(Mc),
            libdata->MTensor_getRealData(K),
            libdata->MTensor_getRealData(L),
            libdata->MTensor_getRealData(M),
            n, m
        );
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, Mc);
    return LIBRARY_NO_ERROR;
}


//  ////////////////////////////////
//  //  Kernel matrix M diagonal  //
//  ////////////////////////////////

//
//  Centers diagonal of a kernel matrix of samples versus themselves, with respect to other samples.
//
//  Centering is done in feature space.
//
//  Input:
//    [0] {Real,2,"Constant"} n x n kernel matrix K.
//    [1] {Real,2,"Constant"} n x m kernel matrix L.
//    [2] {Real,1,"Constant"} m x 1 diagonal of kernel matrix M.
//  Output:
//    [0] {Real,2} m x 1 diagonal of centered kernel matrix.
//

EXTERN_C DLLEXPORT int qmml_center_kernel_matrix_mdiag(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    if( argc != 3 ) { libdata->Message("qmml_center_kernel_matrix_mdiag: wrong number of arguments."); return LIBRARY_FUNCTION_ERROR; }

    MTensor K = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(K) != 2 ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input matrix K has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Kdims = libdata->MTensor_getDimensions(K);
    const mint n = Kdims[0];
    if( Kdims[1] != n ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input matrix K is not quadratic."); return LIBRARY_DIMENSION_ERROR; }

    MTensor L = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(L) != 2 ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input matrix L has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Ldims = libdata->MTensor_getDimensions(L);
    const mint m = Ldims[1];
    if( Ldims[0] != n ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input matrix L has incompatible dimensions."); return LIBRARY_DIMENSION_ERROR; }

    MTensor M = MArgument_getMTensor(args[2]);
    if( libdata->MTensor_getRank(M) != 1 ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input vector m has wrong rank."); return LIBRARY_RANK_ERROR; }
    mint const* Mdims = libdata->MTensor_getDimensions(M);
    if( Mdims[0] != m ) { libdata->Message("qmml_center_kernel_matrix_mdiag: input vector m has incompatible dimensions."); return LIBRARY_DIMENSION_ERROR; }

    MTensor Mc;
    const mint Mcdims[1] = {m};
    int err; if( ( err = libdata->MTensor_new(MType_Real, 1, Mcdims, &Mc) ) ) { libdata->Message("qmml_center_kernel_matrix_mdiag: creation of result matrix failed."); return err; }

    // Calculation
    try
    {
        qmml::center_kernel_matrix_mdiag
        (
            libdata->MTensor_getRealData(Mc),
            libdata->MTensor_getRealData(K),
            libdata->MTensor_getRealData(L),
            libdata->MTensor_getRealData(M),
            n, m
        );
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, Mc);
    return LIBRARY_NO_ERROR;
}
