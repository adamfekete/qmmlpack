// qmmlpack
// (c) Matthias Rupp, 2006-2018.
// See LICENSE.txt for license.

// Mathematica bindings for many-body tensor representation.

#include "mathematica.hpp"
#include "bindhelp/representations_mbtr.hpp"

#include <cmath>
#include <vector>

//  ///////////////////////////////////////
//  //  Many-Body Tensor Representation  //
//  ///////////////////////////////////////

//
//  Many-body tensor representation (dense matrix version)
//
//  Input:
//    [ 0] {Integer,0} k
//    [ 1] {Integer,1} vector of molecule sizes
//    [ 2] {Integer,1} vector of starting indices (1-based)
//    [ 3] {Integer,1} linearized atomic numbers z
//    [ 4] {Real,2} linearized atomic coordinates r
//    [ 5] {Real,3} basis vectors; pass {{{}}} if finite system
//    [ 6] {Integer,1} elements
//    [ 7] {Real,0} xmin
//    [ 8] {Real,0} dx
//    [ 9] {Integer,0} xdim
//    [10] "UTF8String" geometry function
//    [11] {Real,1} parameters of geometry function
//    [12] "UTF8String" weighting function
//    [13] {Real,1} parameters of weighting function
//    [14] "UTF8String" distribution function
//    [15] {Real,1} parameters of distribution function
//    [16] "UTF8String" correlation function
//    [17] {Real,1} parameters of correlation function
//    [18] "UTF8String" element indexing function
//    [19] {Real,1} parameters of element indexing function
//    [20] "UTF8String" atom indexing function
//    [21] {Real,1} parameters of atom indexing function
//    [22] {Real,0} target accuracy
//    [23] {"Boolean",0} whether to flatten output tensor
//  Output:
//    [0] {Real,_} many-body tensor
//

EXTERN_C DLLEXPORT int qmml_many_body_tensor(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    // --------------------

    // number of arguments
    if( argc != 24 ) { libdata->Message("mbtrinvnarg"); return LIBRARY_FUNCTION_ERROR; }

    // 0: rank (integer) k
    const mint k = MArgument_getInteger(args[0]);
    if( k <= 0 ) { libdata->Message("mbtrinvk"); return LIBRARY_RANK_ERROR; }

    // 1: molecule sizes (vector) sizes
    MTensor sizes = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(sizes) != 1 ) { libdata->Message("mbtrinvsizes"); return LIBRARY_RANK_ERROR; }
    mint const* sizesdims = libdata->MTensor_getDimensions(sizes);
    mint * sizes_data = libdata->MTensor_getIntegerData(sizes);

    // number of molecules
    const mint ns = sizesdims[0];

    // 2: molecule indices (1-based) (vector) inds
    MTensor inds = MArgument_getMTensor(args[2]);
    if( libdata->MTensor_getRank(inds) != 1 ) { libdata->Message("mbtrinvinds"); return LIBRARY_RANK_ERROR; }
    mint const* indsdims = libdata->MTensor_getDimensions(inds);
    if( indsdims[0] != ns ) { libdata->Message("mbtrinvinds"); return LIBRARY_DIMENSION_ERROR; }
    mint * inds_data = libdata->MTensor_getIntegerData(inds);

    // 3: linearized atomic numbers (vector) zz
    MTensor zz = MArgument_getMTensor(args[3]);
    if( libdata->MTensor_getRank(zz) != 1 ) { libdata->Message("mbtrinvz"); return LIBRARY_RANK_ERROR; }
    mint const* zzdims = libdata->MTensor_getDimensions(zz);
    if( zzdims[0] != inds_data[ns-1] + sizes_data[ns-1] - 1 ) { libdata->Message("mbtrinvz"); return LIBRARY_DIMENSION_ERROR; }
    mint * zz_data = libdata->MTensor_getIntegerData(zz);

    // 4: linearized atomic coordinates (matrix) rr
    MTensor rr = MArgument_getMTensor(args[4]);
    if( libdata->MTensor_getRank(rr) != 2 ) { libdata->Message("mbtrinvr"); return LIBRARY_RANK_ERROR; }
    mint const* rrdims = libdata->MTensor_getDimensions(rr);
    if( rrdims[0] != inds_data[ns-1] + sizes_data[ns-1] - 1 || rrdims[1] != 3 ) { libdata->Message("mbtrinvr"); return LIBRARY_DIMENSION_ERROR; }
    mreal * rr_data = libdata->MTensor_getRealData(rr);

    // 5: basis vectors (matrix) bases
    MTensor bases = MArgument_getMTensor(args[5]);
    if( libdata->MTensor_getRank(bases) != 3 ) { libdata->Message("mbtrinvb"); return LIBRARY_RANK_ERROR; }
    mint const* basesdims = libdata->MTensor_getDimensions(bases);
    if( !( basesdims[0] == 1 || (basesdims[0] == ns && basesdims[1] == 3 && basesdims[2] == 3) ) ) { libdata->Message("mbtrinvb"); return LIBRARY_DIMENSION_ERROR; }
    mreal * bases_data = libdata->MTensor_getRealData(bases);

    // finite or periodic
    const bool periodic = ( basesdims[1] == 3 );

    // 6: elements (vector) els
    MTensor els = MArgument_getMTensor(args[6]);
    if( libdata->MTensor_getRank(els) != 1 ) { libdata->Message("mbtrinvels"); return LIBRARY_RANK_ERROR; }
    mint const* elsdims = libdata->MTensor_getDimensions(els);
    if( elsdims[0] < 1 ) { libdata->Message("mbtrinvels"); return LIBRARY_DIMENSION_ERROR; }
    mint * els_data = libdata->MTensor_getIntegerData(els);

    // number of elements
    const mint ne = elsdims[0];

    // 7: dxmin
    const mreal xmin = MArgument_getReal(args[7]);

    // 8: dx
    const mreal deltax = MArgument_getReal(args[8]);
    if( deltax <= 0. ) { libdata->Message("mbtrinvdiscr"); return LIBRARY_FUNCTION_ERROR; }

    // 9: xdim
    const mint xdim = MArgument_getInteger(args[9]);
    if( xdim <= 0 ) { libdata->Message("mbtrinvdiscr"); return LIBRARY_FUNCTION_ERROR; }

    // 10: geometry function
    char *const geomf = MArgument_getUTF8String(args[10]);

    // 11: parameters
    MTensor geomfp = MArgument_getMTensor(args[11]);
    if( libdata->MTensor_getRank(geomfp) != 1 ) { libdata->Message("mbtrinvgeomf"); return LIBRARY_RANK_ERROR; }
    mint const* geomfpdims = libdata->MTensor_getDimensions(geomfp);
    mreal * geomfp_data = libdata->MTensor_getRealData(geomfp);

    // 12: weighting function
    char *const weightf = MArgument_getUTF8String(args[12]);

    // 13: parameters
    MTensor weightfp = MArgument_getMTensor(args[13]);
    if( libdata->MTensor_getRank(weightfp) != 1 ) { libdata->Message("mbtrinvweightf"); return LIBRARY_RANK_ERROR; }
    mint const* weightfpdims = libdata->MTensor_getDimensions(weightfp);
    mreal * weightfp_data = libdata->MTensor_getRealData(weightfp);

    // 14: distribution function
    char *const distrf = MArgument_getUTF8String(args[14]);

    // 15: parameters
    MTensor distrfp = MArgument_getMTensor(args[15]);
    if( libdata->MTensor_getRank(distrfp) != 1 ) { libdata->Message("mbtrinvdistrf"); return LIBRARY_RANK_ERROR; }
    mint const* distrfpdims = libdata->MTensor_getDimensions(distrfp);
    mreal * distrfp_data = libdata->MTensor_getRealData(distrfp);

    // 16: correlation function
    char *const corrf = MArgument_getUTF8String(args[16]);

    // 17: parameters
    MTensor corrfp = MArgument_getMTensor(args[17]);
    if( libdata->MTensor_getRank(corrfp) != 1 ) { libdata->Message("mbtrinvcorrf"); return LIBRARY_RANK_ERROR; }
    mint const* corrfpdims = libdata->MTensor_getDimensions(corrfp);
    mreal * corrfp_data = libdata->MTensor_getRealData(corrfp);

    // 18: element indexing function
    char *const eindexf = MArgument_getUTF8String(args[18]);

    // 19: parameters
    MTensor eindexfp = MArgument_getMTensor(args[19]);
    if( libdata->MTensor_getRank(eindexfp) != 1 ) { libdata->Message("mbtrinveindexf"); return LIBRARY_RANK_ERROR; }
    mint const* eindexfpdims = libdata->MTensor_getDimensions(eindexfp);
    mreal * eindexfp_data = libdata->MTensor_getRealData(eindexfp);

    // 20: atom indexing function
    char *const aindexf = MArgument_getUTF8String(args[20]);

    // 21: parameters
    MTensor aindexfp = MArgument_getMTensor(args[21]);
    if( libdata->MTensor_getRank(aindexfp) != 1 ) { libdata->Message("mbtrinvaindexf"); return LIBRARY_RANK_ERROR; }
    mint const* aindexfpdims = libdata->MTensor_getDimensions(aindexfp);
    mreal * aindexfp_data = libdata->MTensor_getRealData(aindexfp);

    // 22: accuracy
    const mreal acc = MArgument_getReal(args[22]);
    if( acc <= 0. ) { libdata->Message("mbtrinvacc"); return LIBRARY_FUNCTION_ERROR; }

    // 23: flatten
    const bool flatten = MArgument_getBoolean(args[23]);

    // Result
    MTensor mbtr;
    mint mbtrdims[6];

    // MBTR call
    // ---------

    try
    {
        // convert to 0-based indices
        std::vector<mint> inds0(ns); for(size_t i = 0; i < ns; ++i) inds0[i] = inds_data[i] - 1;

        auto params = bh::mbtr_params<mint>(
            k,
            nullptr,
            ns,
            sizes_data,
            inds0.data(),
            periodic ? bases_data : nullptr,
            zz_data, rr_data,
            ne, els_data,
            xmin, deltax, xdim,
            geomf  , geomfpdims  [0], geomfp_data,
            weightf, weightfpdims[0], weightfp_data,
            distrf , distrfpdims [0], distrfp_data,
            corrf  , corrfpdims  [0], corrfp_data,
            eindexf, eindexfpdims[0], eindexfp_data,
            aindexf, aindexfpdims[0], aindexfp_data,
            acc
        );

        bh::call_many_body_tensor(params);

        libdata->UTF8String_disown(geomf);
        libdata->UTF8String_disown(weightf);
        libdata->UTF8String_disown(distrf);
        libdata->UTF8String_disown(corrf);
        libdata->UTF8String_disown(eindexf);
        libdata->UTF8String_disown(aindexf);

        if(params.mbtr_ndim < 2 || params.mbtr_shape[0] != ns) { libdata->Message("mbtrint"); return LIBRARY_NUMERICAL_ERROR; }

        if(flatten)
            { mbtrdims[0] = ns; mbtrdims[1] = params.mbtr_size / ns; }
        else
            for(auto i = 0; i < params.mbtr_ndim; ++i) mbtrdims[i] = params.mbtr_shape[i];

        int err; if( (err = libdata->MTensor_new(MType_Real, flatten ? 2 : params.mbtr_ndim, mbtrdims, &mbtr)) ) { libdata->Message("mbtrresfail"); return err; }
        mreal * mbtr_data = libdata->MTensor_getRealData(mbtr);
        std::memcpy(mbtr_data, params.mbtr, params.mbtr_size*sizeof(double));
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, mbtr);
    return LIBRARY_NO_ERROR;
}
