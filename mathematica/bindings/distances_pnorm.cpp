// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings for distance functions based on p-norms.

#include "mathematica.hpp"
#include "qmmlpack/distances_pnorm.hpp"


//  ////////////////////////////////
//  //  One-norm distance matrix  //
//  ////////////////////////////////

//
//  Computes 1-norm distance between vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x n distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_one_norm_k(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor D;
    const mint Ddims[2] = { n, n };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_one_norm_k(dd, xx, n, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}

//
//  Computes 1-norm distance between two sets of vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//    [1] {Real,2,"Constant"} m x d matrix Z of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x m distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_one_norm_l(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor Z = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(Z) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Zdims = libdata->MTensor_getDimensions(Z);
    const mint m = Zdims[0];
    if( Zdims[1] != d ) return LIBRARY_RANK_ERROR;

    MTensor D;
    const mint Ddims[2] = { n, m };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double const*const zz = libdata->MTensor_getRealData(Z);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_one_norm_l(dd, xx, zz, n, m, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}


//  /////////////////////////////////
//  //  Euclidean distance matrix  //
//  /////////////////////////////////

//
//  Computes Euclidean distance between vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x n distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_euclidean_k(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor D;
    const mint Ddims[2] = { n, n };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_euclidean_k(dd, xx, n, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}

//
//  Computes Euclidean distance between two sets of vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//    [1] {Real,2,"Constant"} m x d matrix Z of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x m distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_euclidean_l(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor Z = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(Z) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Zdims = libdata->MTensor_getDimensions(Z);
    const mint m = Zdims[0];
    if( Zdims[1] != d ) return LIBRARY_RANK_ERROR;

    MTensor D;
    const mint Ddims[2] = { n, m };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double const*const zz = libdata->MTensor_getRealData(Z);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_euclidean_l(dd, xx, zz, n, m, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}


//  /////////////////////////////////////////
//  //  Squared Euclidean distance matrix  //
//  /////////////////////////////////////////

//
//  Computes squared Euclidean distance between vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x n distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_squared_euclidean_k(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor D;
    const mint Ddims[2] = { n, n };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_squared_euclidean_k(dd, xx, n, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}

//
//  Computes squared Euclidean distance between two sets of vectors.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//    [1] {Real,2,"Constant"} m x d matrix Z of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x m distance matrix D
//

EXTERN_C DLLEXPORT int qmml_distance_matrix_squared_euclidean_l(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor Z = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(Z) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Zdims = libdata->MTensor_getDimensions(Z);
    const mint m = Zdims[0];
    if( Zdims[1] != d ) return LIBRARY_RANK_ERROR;

    MTensor D;
    const mint Ddims[2] = { n, m };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ddims, &D)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double const*const zz = libdata->MTensor_getRealData(Z);
        double *const dd = libdata->MTensor_getRealData(D);
        qmml::distance_matrix_squared_euclidean_l(dd, xx, zz, n, m, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, D);
    return LIBRARY_NO_ERROR;
}
