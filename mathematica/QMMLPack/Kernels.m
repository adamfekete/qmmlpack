(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(* Import and export formats *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ************************  *)
(*  *  CenterKernelMatrix  *  *)
(*  ************************  *)

If[Not[ValueQ[CenterKernelMatrix::usage]], CenterKernelMatrix::usage =
"CenterKernelMatrix[K] centers kernel matrix K in feature space.
CenterKernelMatrix[K,L] centers kernel matrix L with respect to K.
CenterKernelMatrix[K,L,M] centers kernel matrix M with respect to K.
CenterKernelMatrix[K,L,m] called with a vector, it computes only the diagonal of centered M.

Uses double centering formula (I-(1/n)U)K(I-(1/n)U), where U is the n x n matrix of all ones.
For training versus test samples, it computes (I-(1/n)U)(L-(1/n)KU), where U are two all-one matrices of appropriate dimensions.
For test versus test samples, it computes M-(1/n)L.T U-(1/n)UL+(1/n^2)UKU, where U are all-one matrices of appropriate dimensions.

Returns a centered kernel matrix. All samples are centered with respect to training samples, i.e., the mean of the training samples is subtracted in feature space."];

Begin["`Private`"]

centerKernelMatrixK = LibraryFunctionLoad[libQMMLPack, "qmml_center_kernel_matrix_k", {{Real, 2, "Constant"}}, {Real, 2}];
centerKernelMatrixL = LibraryFunctionLoad[libQMMLPack, "qmml_center_kernel_matrix_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];
centerKernelMatrixM = LibraryFunctionLoad[libQMMLPack, "qmml_center_kernel_matrix_m", {{Real, 2, "Constant"}, {Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];
centerKernelMatrixMdiag = LibraryFunctionLoad[libQMMLPack, "qmml_center_kernel_matrix_mdiag", {{Real, 2, "Constant"}, {Real, 2, "Constant"}, {Real, 1, "Constant"}}, {Real, 1}];

CenterKernelMatrix[{{}}, ___] := Indeterminate;  (* Special case of no training samples *)

CenterKernelMatrix[K_?MatrixQ] := centerKernelMatrixK[K];
CenterKernelMatrix[K_?MatrixQ, L_?MatrixQ] := centerKernelMatrixL[K, L];
CenterKernelMatrix[K_?MatrixQ, L_?MatrixQ, M_?MatrixQ] := centerKernelMatrixM[K, L, M];
CenterKernelMatrix[K_?MatrixQ, L_?MatrixQ, m_?VectorQ] := centerKernelMatrixMdiag[K, L, m];

End[]


(*  *******************  *)
(*  *  Linear kernel  *  *)
(*  *******************  *)

If[Not[ValueQ[KernelLinear::usage]], KernelLinear::usage =
"KernelLinear[X,{}] returns the linear kernel matrix for the vectors in X.
KernelLinear[X,Z,{}] returns the linear kernel matrix between the vectors in X and Z.
KernelLinear[X,{},Diagonal->True] returns the diagonal of KernelLinear[X].
X and Z are matrices containing input samples as rows."];

Begin["`Private`"]

kernelLinearK = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_linear_k", {{Real, 2, "Constant"}}, {Real, 2}];
kernelLinearL = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_linear_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];
kernelLinearm = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_linear_m", {{Real, 2, "Constant"}}, {Real, 1}];

KernelLinear[X_?MatrixQ, {}] := kernelLinearK[X];  (* Dot[X, Transpose[X]]; *)
KernelLinear[X_?MatrixQ, Z_?MatrixQ, {}] := kernelLinearL[X, Z];  (* Dot[X, Transpose[Z]]; *)
KernelLinear[X_?MatrixQ, {}, Diagonal->True] := kernelLinearm[X];  (* Power[Map[Norm, X], 2]; *)

End[]


(*  *********************  *)
(*  *  Gaussian kernel  *  *)
(*  *********************  *)

If[Not[ValueQ[KernelGaussian::usage]], KernelGaussian::usage =
"KernelGaussian[X,{s}] returns the Gaussian kernel matrix K for the vectors in X using length scale s.
KernelGaussian[X,Z,{s}] returns the Gaussian kernel matrix L between the vectors in X and Z.
KernelGaussian[D,{s},Distance->True] accepts a matrix D of squared Euclidean distances.
KernelGaussian[X,{s},Diagonal->True] returns the diagonal of the kernel matrix.
X and Z are matrices containing input samples as rows. The length scale s is a positive real number."];

Begin["`Private`"]

(* Negative \[Sigma] parameters would be possible (the square removes the sign),
but are not supported as this would bring no advantage, but possibly confusion.
Special case sigma = 0 is excluded as well. *)

kernelGaussianK = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_gaussian_k", {{Real, 2, "Constant"}, Real}, {Real, 2}];
kernelGaussianL = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_gaussian_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}, Real}, {Real, 2}];
kernelGaussianm = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_gaussian_m", {{Real, 2, "Constant"}, Real}, {Real, 1}];
kernelGaussianD = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_gaussian_d", {{Real, 2, "Constant"}, Real}, {Real, 2}];

Options[KernelGaussian] = { Diagonal -> False, Distance -> False };

KernelGaussian[X_?MatrixQ, {s_?Positive}, opts:OptionsPattern[]] := Module[{odiag,odist},
    {odiag, odist} = {OptionValue[Diagonal], OptionValue[Distance]};
    Assert[BooleanQ[odiag] && BooleanQ[odist]];

    Switch[{odiag,odist},
        {False, False}, kernelGaussianK[X, s],
        {False, True}, kernelGaussianD[X, s],
        {True, False}, kernelGaussianm[X, s],  (* ConstantArray[1., Length[X]] *)
        {True, True}, ConstantArray[1., Length[X]]
    ]
];

KernelGaussian[X_?MatrixQ, Z_?MatrixQ, {s_?Positive}] := kernelGaussianL[X, Z, s];

End[]


(*  **********************  *)
(*  *  Laplacian kernel  *  *)
(*  **********************  *)

If[Not[ValueQ[KernelLaplacian::usage]], KernelLaplacian::usage =
"KernelLaplacian[X,{s}] returns the Laplacian kernel matrix K for the vectors in X using length scale s.
KernelLaplacian[X,Z,{s}] returns the Laplacian kernel matrix L between the vectors in X and Z.
KernelLaplacian[D,{s},Distance->True] accepts a matrix D of one-norm distances.
KernelLaplacian[X,{s},Diagonal->True] returns the diagonal of the kernel matrix.
X and Z are matrices containing input samples as rows. The length scale s is a positive real number."];

Begin["`Private`"]

kernelLaplacianK = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_laplacian_k", {{Real, 2, "Constant"}, Real}, {Real, 2}];
kernelLaplacianL = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_laplacian_l", {{Real, 2, "Constant"}, {Real, 2, "Constant"}, Real}, {Real, 2}];
kernelLaplacianm = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_laplacian_m", {{Real, 2, "Constant"}, Real}, {Real, 1}];
kernelLaplacianD = LibraryFunctionLoad[libQMMLPack, "qmml_kernel_matrix_laplacian_d", {{Real, 2, "Constant"}, Real}, {Real, 2}];

Options[KernelLaplacian] = { Diagonal -> False, Distance -> False };

KernelLaplacian[X_?MatrixQ, {s_?Positive}, opts:OptionsPattern[]] := Module[{odiag,odist},
    {odiag, odist} = {OptionValue[Diagonal], OptionValue[Distance]};
    Assert[BooleanQ[odiag] && BooleanQ[odist]];

    Switch[{odiag,odist},
        {False, False}, kernelLaplacianK[X, s],
        {False, True}, kernelLaplacianD[X, s],
        {True, False}, kernelLaplacianm[X, s],  (* ConstantArray[1., Length[X]] *)
        {True, True}, ConstantArray[1., Length[X]]
    ]
];

KernelLaplacian[X_?MatrixQ, Z_?MatrixQ, {s_?Positive}] := kernelLaplacianL[X, Z, s];

End[]

EndPackage[]
