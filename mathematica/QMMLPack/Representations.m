(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2017.    *)
(*  See LICENSE.txt for license. *)

(* Representations *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]

(*  ******************  *)
(*  *  Package-wide  *  *)
(*  ******************  *)

If[Not[ValueQ[Unit::usage]], Unit::usage =
"Unit is an option for representations that specifies the unit of atomic coordinates passed."];


(*  *******************  *)
(*  *  CoulombMatrix  *  *)
(*  *******************  *)

If[Not[ValueQ[Post::usage]], Post::usage =
"Post is an option for CoulombMatrix representation that specifies a post-processing function applied to each matrix element (after sorting)."];

If[Not[ValueQ[CutoffRadius::usage]], CutoffRadius::usage =
"CutoffRadius is an option for CoulombMatrix representation of atoms that specifies the radius around the central atom inside of which atoms are considered."];

If[Not[ValueQ[CoulombMatrix::usage]], CoulombMatrix::usage =
"CoulombMatrix[z,r] computes Coulomb matrix representation of a molecule. [1]
CoulombMatrix[z,r,ind] computes Coulomb matrices of atoms in their environment for given indices. [2]
CoulombMatrix[] returns a CoulombMatrixFunction that can be used to repeatedly compute Coulomb matrices with same settings.

Parameters:
  z - atomic numbers of atoms.
  r - Cartesian coordinates of atoms.
  ind - All, index, or list of indices.

Options:
  Unit [\"Angstroms\"] - Unit of atom coordinates (\"Picometers\", \"Angstroms\", or \"BohrRadius\", with synonyms \"pm\", \"A\", \"a0\").
  Padding [False] - Pads matrix with rows and columns of 0s for a given total number of rows/columns. If fewer than number of atoms, matrix is trimmed.
  Flatten [False] - True \"vectorizes\" matrix, effectively returning Flatten[LowerTriangularPart[...]]. Passing k effectively returns Flatten[LowerTriangularPart[...,k]].
  Sort [True] - If and how to sort the matrix. False does not sort, resulting in input order.
      For molecule Coulomb matrices:
      - True sorts by row norms.
      - Ordering function p(cm) returning sorting permutation (like Ordering[]).
      For atom Coulomb matrices:
      - True sorts by Euclidean distance to center atom.
      - \"ZWeightedEuclideanDistance\" divides Euclidean distances by atom number.
      - Ordering function p(dm,z,i) accepting distance matrix dm, atomic numbers z, and index i, returning sorting permutation (like Ordering[]).
  Post [None] - Post-processing function applied to each Coulomb matrix entry (after sorting), called with parameters cm_ij, Z_i, Z_j, d_ij, i, j.
  CutoffRadius [Infinity] - For atom Coulomb matrices, atoms farther than cut-off radius from central atom are ignored. Same unit as atom coordinates.

Returns:
  Coulomb matrix, or, if ind is a list, list of corresponding atom Coulomb matrices.

Remarks:
  Order of steps is matrix calculation, sorting, post processing, padding, flattening.
  In particular, post processing is applied after sorting.
  Atom Coulomb matrices for given ind are still computed considering all atoms.

Examples:
  CoulombMatrix[{1,1}, {{0,0,0},{0.74,0,0}}] == {{0.5,0.715},{0.715,0.5}}
  CoulombMatrix[{1,1}, {{0,0,0},{74,0,0}},Unit->\"Picometers\",Padding->3,Flatten->True] == {0.5,0.715,0.5,0.,0.,0.}
  cmf = CoulombMatrix[Unit->\"Picometers\",Padding->3,Flatten->True]; cmf[{1,1}, {{0,0,0},{74,0,0}}] == {0.5,0.715,0.5,0.,0.,0.}

References:
[1] M. Rupp, A. Tkatchenko, K.-R. M\[UDoubleDot]ller, O.A. von Lilienfeld: Fast and Accurate Modeling of Molecular Atomization Energies with Machine Learning, Physical Review Letters 108(5): 058301, 2012. DOI 10.1103/PhysRevLett.108.058301
[2] M. Rupp, R. Ramakrishnan, O.A. von Lilienfeld: Machine Learning for Quantum Mechanical Properties of Atoms in Molecules, Journal of Physical Chemistry Letters 6(16): 3309-3313, 2015. DOI 10.1021/acs.jpclett.5b01456
"];

If[Not[ValueQ[CoulombMatrixFunction::usage]], CoulombMatrixFunction::usage =
"CoulombMatrixFunction[state_] represents a function f returned by CoulombMatrix[].

Uses:
  f[z, r] computes molecule Coulomb matrices.
  f[z, r, ind] computes atom Coulomb matrices.

Remarks:
  CoulombMatrixFunction relates to CoulombMatrix as NearestFunction relates to Nearest.
"];

Begin["`Private`"]

CoulombMatrix::error = "``";

Options[CoulombMatrix] = {Unit->"Angstroms", Padding->False, Flatten->False, Sort->True, Post->None, CutoffRadius->Infinity};

(* Pattern on first argument is necessary to have later CoulombMatrix[opts] pattern match. *)
CoulombMatrix[z_?VectorQ, r_?MatrixQ, opts:OptionsPattern[]] := Module[{cf, padding, vect, vectk, sort, postf, cutoffradius},
    Catch[
        {cf, padding, vect, vectk, sort, postf, cutoffradius} = coulombMatrixParseOptions[{opts}];
        moleculeCoulombMatrix[z, r, cf, padding, vect, vectk, sort, postf]
    ]
];

CoulombMatrix[z_?VectorQ, r_?MatrixQ, ind_, opts:OptionsPattern[]] := Module[{cf, padding, vect, vectk, sort, postf, cutoffradius},
    Catch[
        {cf, padding, vect, vectk, sort, postf, cutoffradius} = coulombMatrixParseOptions[{opts}];
        atomCoulombMatrix[z, r, ind, cf, padding, vect, vectk, sort, postf, cutoffradius]
    ]
];

coulombMatrixParseOptions[opts:OptionsPattern[]] := Module[
    {sortf, cf, padding, vect, vectk, sort, postf, cutoffradius, ounit, opadding, oflatten, osort, opost, ocutoffradius},

    {ounit, opadding, oflatten, osort, opost, ocutoffradius} = OptionValue[CoulombMatrix, opts, {Unit, Padding, Flatten, Sort, Post, CutoffRadius}];

    (* Unit conversion factor *)
    cf = Switch[ounit,
        "pm" | "Picometers", 0.01889726124559, (* CODATA 2010 *)
        "A" | "Angstroms" | "angstrom" | "\[ARing]ngstr\[ODoubleDot]m", 1.889726124559, (* CODATA 2010 *)
        "BohrRadius" | "bohr" | "a0" | "a.u.", 1.,
        _, Message[CoulombMatrix::error, "Unknown unit."]; Throw[$Failed]
        ];

    (* Padding *)
    padding = Switch[opadding,
        False | None, None,
        _?(IntegerQ[#] && # >= 1 &), opadding,
        _, Message[CoulombMatrix::error, "Invalid padding specification."]; Throw[$Failed]
    ];

    (* Flatten *)
    {vect, vectk} = Switch[oflatten,
        False | None, {False, 0},
        True, {True, 0},
        _?(IntegerQ[#] &), {True, oflatten},
        _, Message[CoulombMatrix::error, "Invalid flattening specification."]; Throw[$Failed]
    ];

    (* Sorting *)
    sortf[cm_] := Reverse[Ordering[Map[Norm, cm]]];  (* molecule Coulomb matrix *)
    sortf[dm_, z_, i_] := Ordering[dm[[i]]];  (* atom Coulomb matrix *)
    sort = Switch[osort,
        False | None, False,  (* no sorting, flag instead of Range[n] for performance *)
        True, sortf,
        "ZWeightedEuclideanDistance", Function[{dm,z,i}, Ordering[dm[[i]]/z]],
        _, osort, (* ordering function *)
        _, Message[CoulombMatrix::error, "Invalid sorting specification."]; Throw[$Failed]
    ];

    (* Post-processing function *)
    postf = opost;

    (* Cut-off radius *)
    cutoffradius = Switch[ocutoffradius,
        _?(NumericQ[#] && Positive[#] &), ocutoffradius,
        Infinity, Infinity,
        _, Message[CoulombMatrix::error, "Invalid cut-off radius specification."]; Throw[$Failed]
    ];

    {cf, padding, vect, vectk, sort, postf, cutoffradius}
];

(* Molecule Coulomb matrix *)

moleculeCoulombMatrix[z_, r_, cf_, padding_, vect_, vectk_, sort_, postf_] := Module[
    {n, dm, cm, order, i, j},

    n = Length[z];
    Assert[n > 0 && Dimensions[r] == {n, 3}];

    (* Coulomb matrix entries *)
    dm = DistanceEuclidean[r*cf];
    cm = ConstantArray[0., {n, n}];
    For[i = 1, i <= n, ++i, For[j = 1, j <= n, ++j,
        cm[[i,j]] = If[i == j, 0.5*z[[i]]^2.4, z[[i]]*z[[j]]/dm[[i,j]]];
    ];];

    (* Sorting *)
    If[Not[sort === False],
        order = sort[cm];
        cm = cm[[order, order]];
    ];

    (* Post-processing *)
    If[Not[postf === None],
        If[sort === False, order = Range[n]];
        cm = MapIndexed[With[{i=#2[[1]],j=#2[[2]],oi=order[[#2[[1]]]],oj=order[[#2[[2]]]]}, postf[#1, z[[oi]], z[[oj]], dm[[oi,oj]], i, j]] &, cm, {2}];
    ];

    (* Padding *)
    If[Not[padding === None], cm = PadRight[cm, {padding,padding}]];

    (* Flattening *)
    If[vect, cm = LowerTriangularPart[cm, vectk]];

    cm
];

(* Atom Coulomb matrix *)

atomCoulombMatrix[z_, r_, ind_, cf_, padding_, vect_, vectk_, sort_, postf_, cutoffradius_] := Module[
    {},
    Abort[]
]

(* CoulombMatrixFunction *)

CoulombMatrix[opts:OptionsPattern[]] := Module[{options},
    Catch[
        options = coulombMatrixParseOptions[{opts}];
        CoulombMatrixFunction[options]
    ]
];

CoulombMatrixFunction[state_][z_, r_] := Module[{cf, padding, vect, vectk, sort, postf, cutoffradius},
    {cf, padding, vect, vectk, sort, postf, cutoffradius} = state;
    moleculeCoulombMatrix[z, r, cf, padding, vect, vectk, sort, postf]
];

CoulombMatrixFunction[state_][z_, r_, ind_] := Abort[];

End[]


(*  *************************************  *)
(*  *  Many-Body Tensor Representation  *  *)
(*  *************************************  *)

(* molecule = finite system, bulk crystal = periodic system *)

(* Indexing function depends on \"Elements\", which specifies the universe ("Grundgesamtheit") of available elements *)

(* Discretization parameters can not be determined automatically in a data-dependent fashion
   as resulting representations (e.g., for training and for validation set) would be incompatible.
   There can be a separate routine to determine them. A simple (not data-dependent) Automatic setting is possible. *)

If[Not[ValueQ[ManyBodyTensor::usage]], ManyBodyTensor::usage =
"ManyBodyTensor[z,r,d,kgwdcea] computes many-body-tensor representation for a set of finite atomistic systems (molecules, clusters).
ManyBodyTensor[z,r,b,d,kgwdcea] for a set of periodic systems (bulk crystal); atom positions must be Cartesian, not fractional.

Parameters:
  z - atomic numbers of atoms.
  r - Cartesian coordinates of atoms.
  b - cell lattice vectors {a,b,c} of unit cell.
  d - discretization parameters {xmin, deltax, xdim}
  kgwdcea - parametrization {k, geomf, weightf, distrf, corrf, eindexf, aindexf},
    where k+1 is tensor rank and functions can be specified as strings or pairs of string and parameter vector.

Parametrization:
  Geometry functions: 'unity', 'count', '1/distance', '1/dot', 'angle', 'cos_angle', 'dot/dotdot', 'dihedral_angle', 'cos_dihedral_angle'
  Weighting functions: 'unity', 'identity, 'identity_squared', 'identity_root', '1/identity', 'delta_1/identity', 'exp_-1/identity', 'exp_-1/identity_squared', '1/count', '1/normnorm', '1/dotdot', '1/normnormnorm', '1/dotdotdot'
  Distribution functions: 'normal'
  Correlation functions: 'identity'
  Element indexing functions: 'dense_full', 'dense_noreversals'
  Atom indexing functions: 'finite_full', 'finite_noreversals', 'periodic_full', 'periodic_noreversals'

Options:
  \"Elements\" [Automatic] - List of chemical elements (proton numbers) to use. By default those occurring in z.
  AccuracyGoal [1e-3] - accuracy to which MBTR is computed
  Flatten [False] - For \"Dense\" format, returns a matrix for convenience by appropriate concatenation.

Returns:
  Many-body tensor

Examples:
  See unit tests.
"];

Begin["`Private`"]

qmmlManyBodyTensor = LibraryFunctionLoad[libQMMLPack, "qmml_many_body_tensor", {
    {Integer, 0, "Constant"}, (* k *)
    {Integer, 1, "Constant"}, (* sizes *)
    {Integer, 1, "Constant"}, (* 1-based starting indices *)
    {Integer, 1, "Constant"}, (* linearized atomic numbers z *)
    {Real   , 2, "Constant"}, (* linearized atomic coordinates r *)
    {Real   , 3, "Constant"}, (* basis vectors; {{}} if none *)
    {Integer, 1, "Constant"}, (* elements *)
    {Real   , 0, "Constant"}, (* xmin *)
    {Real   , 0, "Constant"}, (* dx *)
    {Integer, 0, "Constant"}, (* xdim *)
    "UTF8String", {Real, 1, "Constant"},  (* geometry function and parameters *)
    "UTF8String", {Real, 1, "Constant"},  (* weighting function and parameters *)
    "UTF8String", {Real, 1, "Constant"},  (* distribution function and parameters *)
    "UTF8String", {Real, 1, "Constant"},  (* correlation function and parameters *)
    "UTF8String", {Real, 1, "Constant"},  (* element indexing function and parameters *)
    "UTF8String", {Real, 1, "Constant"},  (* atom indexing function and parameters *)
    {Real        , 0, "Constant"}, (* accuracy *)
    {"Boolean"   , 0, "Constant"}  (* flatten *)
}, {Real, _}];

LibraryFunction::mbtrinvnarg    = "MBTR: invalid number of arguments";
LibraryFunction::mbtrinvk       = "MBTR: invalid tensor rank specification";
LibraryFunction::mbtrinvsizes   = "MBTR: invalid specification of system sizes.";
LibraryFunction::mbtrinvinds    = "MBTR: invalid specification of system indices.";
LibraryFunction::mbtrinvz       = "MBTR: invalid specification of atomic numbers.";
LibraryFunction::mbtrinvr       = "MBTR: invalid specification of atom coordinates.";
LibraryFunction::mbtrinvb       = "MBTR: invalid specification of basis vectors.";
LibraryFunction::mbtrinvdiscr   = "MBTR: invalid specification of discretization.";
LibraryFunction::mbtrinvgeomf   = "MBTR: invalid specification of geometry function.";
LibraryFunction::mbtrinvweightf = "MBTR: invalid specification of weighting function.";
LibraryFunction::mbtrinvdistrf  = "MBTR: invalid specification of distribution function.";
LibraryFunction::mbtrinvcorrf   = "MBTR: invalid specification of correlation function.";
LibraryFunction::mbtrinveindexf = "MBTR: invalid specification of element indexing function.";
LibraryFunction::mbtrinvaindexf = "MBTR: invalid specification of atom indexing function.";
LibraryFunction::mbtrinvacc     = "MBTR: invalid specification of accuracy goal.";
LibraryFunction::mbtrint        = "MBTR: internal error.";
LibraryFunction::mbtrresfail    = "MBTR: failure to create result tensor.";

ManyBodyTensor::error = "``";

Options[ManyBodyTensor] = {
    "Elements"->Automatic,
    AccuracyGoal->3,
    Flatten->False
};

(* Finite systems *)
ManyBodyTensor[z_?(Depth[#]-1 == 2 &), r_?(Depth[#]-1 == 3 &), d_?(ListQ[#] && Length[#] == 3 &), kgwdcea_?ListQ, opts:OptionsPattern[]] := ManyBodyTensor[z, r, None, d, kgwdcea, {opts}];

(* Periodic systems *)
ManyBodyTensor[z_?(Depth[#]-1 == 2 &), r_?(Depth[#]-1 == 3 &), b_?(# === None || ArrayQ[#] && Rest[Dimensions[#]] == {3,3} &), d_?(ListQ[#] && Length[#] == 3 &), kgwdcea_?ListQ, opts:OptionsPattern[]] := Module[
    {k, sizes, inds, zz, rr, basis, elements, xmin, dx, xdim, geomf, geomfp, weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten},
    Catch[Check[
        {k, sizes, inds, zz, rr, basis, elements, xmin, dx, xdim, geomf, geomfp, weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten} = mbtrParseOptions[z, r, b, d, kgwdcea, {opts}];
        qmmlManyBodyTensor[k, sizes, inds, zz, rr, basis, elements, xmin, dx, xdim, geomf, geomfp, weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten],
    (Message[ManyBodyTensor::error, $MessageList]; Throw[$Failed]), "QMMLPackLibrary"]]
];

mbtrElements = {"H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", \
    "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", \
    "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", \
    "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", \
    "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", \
    "Uut", "Fl", "Uup", "Lv", "Uus", "Uuo"};

mbtrParseOptions[z_, r_, b_, d_, kgwdcea_, opts:OptionsPattern[]] := Module[  (* b is None for molecules *)
    {k, sizes, inds, zz, rr, basis, elements, xmin, dx, xdim, geomf, geomfp, weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten,
     n, parseParam, oelements, odiscretization, obroadening, ogeometry, oweighting, oformat, oflatten},

    (* Options *)
    {oelements, oaccuracygoal, oflatten} = OptionValue[ManyBodyTensor, opts, {"Elements", AccuracyGoal, Flatten}];

    n = Length[z];

    (* Atomic numbers *)
    If[Not[ MatchQ[z, {{__Integer} ..}] && Min[z] >= 1 ], (Message[ManyBodyTensor::error, "Invalid argument for z."]; Throw[$Failed])];
    zz = Flatten[z];

    (* Atomic coordinates *)
    If[Not[MatchQ[r, {{{__Real} ..} ..}] && Depth[r] == 4],
        If[MatchQ[r, {{{__?NumericQ} ..} ..}] && Depth[r] == 4,
            (Message[ManyBodyTensor::error, "r must contain only real numbers."]; Throw[$Failed]),
            (Message[ManyBodyTensor::error, "Invalid argument for r."]; Throw[$Failed])
        ]
    ];
    rr = Flatten[r, 1];

    (* Basis (lattice vectors) *)
    basis = If[b === None, {{{}}},
        If[MatchQ[b, {{{__Real} ..} ..}] && Dimensions[b] == {n,3,3}, b,
            If[MatchQ[b, {{{__?NumericQ} ..} ..}] && Dimensions[b] != {3,3},
                (Message[ManyBodyTensor::error, "b must contain only real numbers."]; Throw[$Failed]),
                (Message[ManyBodyTensor::error, "Invalid argument for b."]; Throw[$Failed])
            ]
        ]
    ];

    (* Sizes and indices *)
    sizes = Map[Length, z];
    inds = Most[FoldList[Plus, 1, sizes]];

    (* Discretization *)
    {xmin, dx, xdim} = d;
    If[dx <= 0, (Message[ManyBodyTensor::error, "\[CapitalDelta]x must be positive."]; Throw[$Failed])];
    If[Not[IntegerQ[xdim] || xdim < 1], (Message[ManyBodyTensor["xdim must be a positive integer."]]; Throw[$Failed])];

    (* Parametrization *)
    If[Length[kgwdcea] != 7, (Message[ManyBodyTensor::error, "Parametrization has wrong size."]; Throw[$Failed])];
    {k, geomf, weightf, distrf, corrf, eindexf, aindexf} = kgwdcea;

    (* k *)
    If[Not[ IntegerQ[k] && k >= 1 ], (Message[ManyBodyTensor::error, "Invalid argument for rank k."]; Throw[$Failed])];

    (* Geometry, weighting, distribution, correlation, element indexing, atom indexing functions *)
    parseParam[arg_, name_] := If[StringQ[arg], {arg, {}},
        If[ListQ[arg] && Length[arg] == 2 && StringQ[arg[[1]]] && VectorQ[arg[[2]], NumberQ], arg,
        (Message[ManyBodyTensor::error, "Invalid parametrization for " <> name <> " function."]; Throw[$Failed])]];
    {geomf  , geomfp  } = parseParam[geomf  , "geometry"];
    {weightf, weightfp} = parseParam[weightf, "weighting"];
    {distrf , distrfp } = parseParam[distrf , "distribution"];
    {corrf  , corrfp  } = parseParam[corrf  , "correlation"];
    {eindexf, eindexfp} = parseParam[eindexf, "element indexing"];
    {aindexf, aindexfp} = parseParam[aindexf, "atom indexing"];

    eindexf = "dense_" <> eindexf;
    aindexf = If[basis === {{{}}}, "finite_", "periodic_"] <> aindexf;

    (* Elements *)
    elements = Switch[oelements,
        Automatic, Union[Flatten[z]],
        _?(VectorQ[#, IntegerQ] && Min[#] >= 1 &), Union[oelements], (* sorted, no duplicates *)
        _?(VectorQ[#, (StringQ[#] && MemberQ[mbtrElements, #] &)] &), Union[Flatten[Map[Position[mbtrElements, #, Heads->False] &, oelements]]],
        _, (Message[ManyBodyTensor::error, "Invalid value for \"Elements\" option"]; Throw[$Failed])
    ]; (* elements is now sorted in ascending order, with no duplicates *)
    If[Not[SubsetQ[elements, Union[Flatten[z]]]], (Message[ManyBodyTensor::error, "\"Elements\" must cover all elements occurring in z."]; Throw[$Failed])];

    (* AccuracyGoal *)
    acc = If[IntegerQ[oaccuracygoal], 10.^-oaccuracygoal, (Message[ManyBodyTensor::error, "Invalid value for AccuracyGoal option."]; Throw[$Failed])];

    (* Flattening *)
    flatten = If[MemberQ[{False, True}, oflatten], oflatten, (Message[ManyBodyTensor::error, "Invalid value for Flatten option."]; Throw[$Failed])];

    {k, sizes, inds, zz, rr, basis, elements, xmin, dx, xdim, geomf, geomfp, weightf, weightfp, distrf, distrfp, corrf, corrfp, eindexf, eindexfp, aindexf, aindexfp, acc, flatten}
];

End[]

EndPackage[]
