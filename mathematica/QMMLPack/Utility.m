(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2017.    *)
(*  See LICENSE.txt for license. *)

(* Regression models *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  *****************  *)
(*  *  ElementData  *  *)
(*  *****************  *)

(* Wolfram already defines ElementData, but ElementData[el, "AtomicNumber"] can be 50x slower than a look-up table. *)
(* QMMLPack therefore provides a version that can be used as a fast replacement for conversion between atomic numbers and element symbols. *)
(* MR 2017-08-10: Decided against patching built-in ElementData[] as that seems bad practice. *)
(*                Providing a non-conflicting QMMLPack`ElementData[] seems not possible as both names would be on the $ContextPath. *)

elementData::usage =
"elementData[element, \"property\"] gives the value of property for element

Parameters:
  element - Atomic number of abbreviation.
  property - Any of \"AtomicNumber\", \"Abbreviation\".
";

Begin["`Private`"]

elementDataAbrv2An = <|
    "H" ->   1, "He"->   2, "Li"->   3, "Be"->   4, "B" ->   5, "C" ->   6, "N" ->   7, "O" ->   8, "F" ->   9,
    "Ne"->  10, "Na"->  11, "Mg"->  12, "Al"->  13, "Si"->  14, "P" ->  15, "S" ->  16, "Cl"->  17, "Ar"->  18,
    "K" ->  19, "Ca"->  20, "Sc"->  21, "Ti"->  22, "V" ->  23, "Cr"->  24, "Mn"->  25, "Fe"->  26, "Co"->  27,
    "Ni"->  28, "Cu"->  29, "Zn"->  30, "Ga"->  31, "Ge"->  32, "As"->  33, "Se"->  34, "Br"->  35, "Kr"->  36,
    "Rb"->  37, "Sr"->  38, "Y" ->  39, "Zr"->  40, "Nb"->  41, "Mo"->  42, "Tc"->  43, "Ru"->  44, "Rh"->  45,
    "Pd"->  46, "Ag"->  47, "Cd"->  48, "In"->  49, "Sn"->  50, "Sb"->  51, "Te"->  52, "I" ->  53, "Xe"->  54,
    "Cs"->  55, "Ba"->  56, "La"->  57, "Ce"->  58, "Pr"->  59, "Nd"->  60, "Pm"->  61, "Sm"->  62, "Eu"->  63,
    "Gd"->  64, "Tb"->  65, "Dy"->  66, "Ho"->  67, "Er"->  68, "Tm"->  69, "Yb"->  70, "Lu"->  71, "Hf"->  72,
    "Ta"->  73, "W" ->  74, "Re"->  75, "Os"->  76, "Ir"->  77, "Pt"->  78, "Au"->  79, "Hg"->  80, "Tl"->  81,
    "Pb"->  82, "Bi"->  83, "Po"->  84, "At"->  85, "Rn"->  86, "Fr"->  87, "Ra"->  88, "Ac"->  89, "Th"->  90,
    "Pa"->  91, "U" ->  92, "Np"->  93, "Pu"->  94, "Am"->  95, "Cm"->  96, "Bk"->  97, "Cf"->  98, "Es"->  99,
    "Fm"-> 100, "Md"-> 101, "No"-> 102, "Lr"-> 103, "Rf"-> 104, "Db"-> 105, "Sg"-> 106, "Bh"-> 107, "Hs"-> 108,
    "Mt"-> 109, "Ds"-> 110, "Rg"-> 111, "Cn"-> 112, "Nh"-> 113, "Fl"-> 114, "Mc"-> 115, "Lv"-> 116, "Ts"-> 117, "Og"-> 118
|>;

elementDataProperties = {
    <| "AtomicNumber" -> 1,  "Abbreviation" ->  "H" |>,
    <| "AtomicNumber" -> 2,  "Abbreviation" ->  "He" |>,
    <| "AtomicNumber" -> 3,  "Abbreviation" ->  "Li" |>,
    <| "AtomicNumber" -> 4,  "Abbreviation" ->  "Be" |>,
    <| "AtomicNumber" -> 5,  "Abbreviation" ->  "B" |>,
    <| "AtomicNumber" -> 6,  "Abbreviation" ->  "C" |>,
    <| "AtomicNumber" -> 7,  "Abbreviation" ->  "N" |>,
    <| "AtomicNumber" -> 8,  "Abbreviation" ->  "O" |>,
    <| "AtomicNumber" -> 9,  "Abbreviation" ->  "F" |>,
    <| "AtomicNumber" -> 10,  "Abbreviation" ->  "Ne" |>,
    <| "AtomicNumber" -> 11,  "Abbreviation" ->  "Na" |>,
    <| "AtomicNumber" -> 12,  "Abbreviation" ->  "Mg" |>,
    <| "AtomicNumber" -> 13,  "Abbreviation" ->  "Al" |>,
    <| "AtomicNumber" -> 14,  "Abbreviation" ->  "Si" |>,
    <| "AtomicNumber" -> 15,  "Abbreviation" ->  "P" |>,
    <| "AtomicNumber" -> 16,  "Abbreviation" ->  "S" |>,
    <| "AtomicNumber" -> 17,  "Abbreviation" ->  "Cl" |>,
    <| "AtomicNumber" -> 18,  "Abbreviation" ->  "Ar" |>,
    <| "AtomicNumber" -> 19,  "Abbreviation" ->  "K" |>,
    <| "AtomicNumber" -> 20,  "Abbreviation" ->  "Ca" |>,
    <| "AtomicNumber" -> 21,  "Abbreviation" ->  "Sc" |>,
    <| "AtomicNumber" -> 22,  "Abbreviation" ->  "Ti" |>,
    <| "AtomicNumber" -> 23,  "Abbreviation" ->  "V" |>,
    <| "AtomicNumber" -> 24,  "Abbreviation" ->  "Cr" |>,
    <| "AtomicNumber" -> 25,  "Abbreviation" ->  "Mn" |>,
    <| "AtomicNumber" -> 26,  "Abbreviation" ->  "Fe" |>,
    <| "AtomicNumber" -> 27,  "Abbreviation" ->  "Co" |>,
    <| "AtomicNumber" -> 28,  "Abbreviation" ->  "Ni" |>,
    <| "AtomicNumber" -> 29,  "Abbreviation" ->  "Cu" |>,
    <| "AtomicNumber" -> 30,  "Abbreviation" ->  "Zn" |>,
    <| "AtomicNumber" -> 31,  "Abbreviation" ->  "Ga" |>,
    <| "AtomicNumber" -> 32,  "Abbreviation" ->  "Ge" |>,
    <| "AtomicNumber" -> 33,  "Abbreviation" ->  "As" |>,
    <| "AtomicNumber" -> 34,  "Abbreviation" ->  "Se" |>,
    <| "AtomicNumber" -> 35,  "Abbreviation" ->  "Br" |>,
    <| "AtomicNumber" -> 36,  "Abbreviation" ->  "Kr" |>,
    <| "AtomicNumber" -> 37,  "Abbreviation" ->  "Rb" |>,
    <| "AtomicNumber" -> 38,  "Abbreviation" ->  "Sr" |>,
    <| "AtomicNumber" -> 39,  "Abbreviation" ->  "Y" |>,
    <| "AtomicNumber" -> 40,  "Abbreviation" ->  "Zr" |>,
    <| "AtomicNumber" -> 41,  "Abbreviation" ->  "Nb" |>,
    <| "AtomicNumber" -> 42,  "Abbreviation" ->  "Mo" |>,
    <| "AtomicNumber" -> 43,  "Abbreviation" ->  "Tc" |>,
    <| "AtomicNumber" -> 44,  "Abbreviation" ->  "Ru" |>,
    <| "AtomicNumber" -> 45,  "Abbreviation" ->  "Rh" |>,
    <| "AtomicNumber" -> 46,  "Abbreviation" ->  "Pd" |>,
    <| "AtomicNumber" -> 47,  "Abbreviation" ->  "Ag" |>,
    <| "AtomicNumber" -> 48,  "Abbreviation" ->  "Cd" |>,
    <| "AtomicNumber" -> 49,  "Abbreviation" ->  "In" |>,
    <| "AtomicNumber" -> 50,  "Abbreviation" ->  "Sn" |>,
    <| "AtomicNumber" -> 51,  "Abbreviation" ->  "Sb" |>,
    <| "AtomicNumber" -> 52,  "Abbreviation" ->  "Te" |>,
    <| "AtomicNumber" -> 53,  "Abbreviation" ->  "I" |>,
    <| "AtomicNumber" -> 54,  "Abbreviation" ->  "Xe" |>,
    <| "AtomicNumber" -> 55,  "Abbreviation" ->  "Cs" |>,
    <| "AtomicNumber" -> 56,  "Abbreviation" ->  "Ba" |>,
    <| "AtomicNumber" -> 57,  "Abbreviation" ->  "La" |>,
    <| "AtomicNumber" -> 58,  "Abbreviation" ->  "Ce" |>,
    <| "AtomicNumber" -> 59,  "Abbreviation" ->  "Pr" |>,
    <| "AtomicNumber" -> 60,  "Abbreviation" ->  "Nd" |>,
    <| "AtomicNumber" -> 61,  "Abbreviation" ->  "Pm" |>,
    <| "AtomicNumber" -> 62,  "Abbreviation" ->  "Sm" |>,
    <| "AtomicNumber" -> 63,  "Abbreviation" ->  "Eu" |>,
    <| "AtomicNumber" -> 64,  "Abbreviation" ->  "Gd" |>,
    <| "AtomicNumber" -> 65,  "Abbreviation" ->  "Tb" |>,
    <| "AtomicNumber" -> 66,  "Abbreviation" ->  "Dy" |>,
    <| "AtomicNumber" -> 67,  "Abbreviation" ->  "Ho" |>,
    <| "AtomicNumber" -> 68,  "Abbreviation" ->  "Er" |>,
    <| "AtomicNumber" -> 69,  "Abbreviation" ->  "Tm" |>,
    <| "AtomicNumber" -> 70,  "Abbreviation" ->  "Yb" |>,
    <| "AtomicNumber" -> 71,  "Abbreviation" ->  "Lu" |>,
    <| "AtomicNumber" -> 72,  "Abbreviation" ->  "Hf" |>,
    <| "AtomicNumber" -> 73,  "Abbreviation" ->  "Ta" |>,
    <| "AtomicNumber" -> 74,  "Abbreviation" ->  "W" |>,
    <| "AtomicNumber" -> 75,  "Abbreviation" ->  "Re" |>,
    <| "AtomicNumber" -> 76,  "Abbreviation" ->  "Os" |>,
    <| "AtomicNumber" -> 77,  "Abbreviation" ->  "Ir" |>,
    <| "AtomicNumber" -> 78,  "Abbreviation" ->  "Pt" |>,
    <| "AtomicNumber" -> 79,  "Abbreviation" ->  "Au" |>,
    <| "AtomicNumber" -> 80,  "Abbreviation" ->  "Hg" |>,
    <| "AtomicNumber" -> 81,  "Abbreviation" ->  "Tl" |>,
    <| "AtomicNumber" -> 82,  "Abbreviation" ->  "Pb" |>,
    <| "AtomicNumber" -> 83,  "Abbreviation" ->  "Bi" |>,
    <| "AtomicNumber" -> 84,  "Abbreviation" ->  "Po" |>,
    <| "AtomicNumber" -> 85,  "Abbreviation" ->  "At" |>,
    <| "AtomicNumber" -> 86,  "Abbreviation" ->  "Rn" |>,
    <| "AtomicNumber" -> 87,  "Abbreviation" ->  "Fr" |>,
    <| "AtomicNumber" -> 88,  "Abbreviation" ->  "Ra" |>,
    <| "AtomicNumber" -> 89,  "Abbreviation" ->  "Ac" |>,
    <| "AtomicNumber" -> 90,  "Abbreviation" ->  "Th" |>,
    <| "AtomicNumber" -> 91,  "Abbreviation" ->  "Pa" |>,
    <| "AtomicNumber" -> 92,  "Abbreviation" ->  "U" |>,
    <| "AtomicNumber" -> 93,  "Abbreviation" ->  "Np" |>,
    <| "AtomicNumber" -> 94,  "Abbreviation" ->  "Pu" |>,
    <| "AtomicNumber" -> 95,  "Abbreviation" ->  "Am" |>,
    <| "AtomicNumber" -> 96,  "Abbreviation" ->  "Cm" |>,
    <| "AtomicNumber" -> 97,  "Abbreviation" ->  "Bk" |>,
    <| "AtomicNumber" -> 98,  "Abbreviation" ->  "Cf" |>,
    <| "AtomicNumber" -> 99,  "Abbreviation" ->  "Es" |>,
    <| "AtomicNumber" -> 100,  "Abbreviation" ->  "Fm" |>,
    <| "AtomicNumber" -> 101,  "Abbreviation" ->  "Md" |>,
    <| "AtomicNumber" -> 102,  "Abbreviation" ->  "No" |>,
    <| "AtomicNumber" -> 103,  "Abbreviation" ->  "Lr" |>,
    <| "AtomicNumber" -> 104,  "Abbreviation" ->  "Rf" |>,
    <| "AtomicNumber" -> 105,  "Abbreviation" ->  "Db" |>,
    <| "AtomicNumber" -> 106,  "Abbreviation" ->  "Sg" |>,
    <| "AtomicNumber" -> 107,  "Abbreviation" ->  "Bh" |>,
    <| "AtomicNumber" -> 108,  "Abbreviation" ->  "Hs" |>,
    <| "AtomicNumber" -> 109,  "Abbreviation" ->  "Mt" |>,
    <| "AtomicNumber" -> 110,  "Abbreviation" ->  "Ds" |>,
    <| "AtomicNumber" -> 111,  "Abbreviation" ->  "Rg" |>,
    <| "AtomicNumber" -> 112,  "Abbreviation" ->  "Cn" |>,
    <| "AtomicNumber" -> 113,  "Abbreviation" ->  "Nh" |>,
    <| "AtomicNumber" -> 114,  "Abbreviation" ->  "Fl" |>,
    <| "AtomicNumber" -> 115,  "Abbreviation" ->  "Mc" |>,
    <| "AtomicNumber" -> 116,  "Abbreviation" ->  "Lv" |>,
    <| "AtomicNumber" -> 117,  "Abbreviation" ->  "Ts" |>,
    <| "AtomicNumber" -> 118,  "Abbreviation" ->  "Og" |>
};

elementData[el_Integer, property_String] := elementDataProperties[[el]][[property]];
elementData[el_String, property_String] := elementDataProperties[[elementDataAbrv2An[[el]]]][[property]];

End[]


EndPackage[]
