(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2016.         *)
(*  See LICENSE.txt for license. *)

(* Regression models *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ******************  *)
(*  *  Package-wide  *  *)
(*  ******************  *)

If[Not[ValueQ[Centering::usage]], Centering::usage =
"Centering is an option for kernel models that controls whether kernel matrices are centered in kernel feature space."];


(*  ***************************  *)
(*  *  KernelRidgeRegression  *  *)
(*  ***************************  *)

If[Not[ValueQ[KernelRidgeRegression::usage]], KernelRidgeRegression::usage =
"KernelRidgeRegression[K,y,{lambda}] returns a kernel ridge regression model.
Parameters:
  K - Kernel matrix of training samples.
  y - Labels of training samples.
  lambda - Regularization strength. Either a non-negative scalar or a vector.
Options:
  Centering [False] - If True, kernel matrix is centered to have zero mean in kernel feature space.
Returns:
  model - A model that can be used to predict new samples and queried for parameters.
  Usage of the model:
    model[L], model[L,\"Predictions\"] - Returns predictions for new samples; L is the kernel matrix between training and test samples.
    model[\"Properties\"] - Returns a list of queryable properties.
Remarks:
  If Centering is True, L and M are also centered with respect to K for predictions.
  Kernel ridge regression models and Gaussian process regression models yield identical predictions.
"];

If[Not[ValueQ[kernelRidgeRegressionModel::usage]], kernelRidgeRegressionModel::usage =
"kernelRidgeRegressionModel[state_] is a model m returned by KernelRidgeRegression[].
Uses:
  m[L], m[L, \"Predictions\"] predictions for kernel matrix L.

  m[\"KernelMatrix\"]  the kernel matrix K.
  m[\"LabelMean\"]  the mean of the labels y. 0 if not centering.
  m[\"Labels\"]  the label vector y.
  m[\"Theta\"]  method hyperparameters as passed
  m[\"RegularizationStrength\"]  processed regularization strength \[Lambda]
  m[\"Options\"]  processed KernelRidgeRegression options
  m[\"Properties\"]  list of queryable properties
  m[\"Weights\"]  weight vector \[Alpha]
"];

Begin["`Private`"]

SetAttributes[kernelRidgeRegressionModel, HoldAll];

KernelRidgeRegression::warn = "Warning: ``";
KernelRidgeRegression::fail = "Kernel ridge regression failed: ``";

Options[KernelRidgeRegression] = {Centering -> False};

KernelRidgeRegression[kk_?(MatrixQ[#, NumericQ] &), y_?(VectorQ[#, NumericQ] &), theta_, opts:OptionsPattern[]] := Module[
    {n,ymean,yc,kkc,uu,alpha,popts,regularizationStrength,i,oCentering},

    (* Parameter and options processing *)
    n = Length[y]; Assert[n > 0];
    If[Dimensions[kk] != {n,n}, Message[KernelRidgeRegression::fail, "incompatible dimensions of kernel matrix and label vector."]; Return[$Failed]];
    regularizationStrength = If[Not[ListQ[theta] && Length[theta] == 1], Message["invalid format for regularization strength {lambda}"]; Return[$Failed], First[theta]];
    If[Not[NumericQ[regularizationStrength] || (VectorQ[regularizationStrength, NumericQ] && Length[regularizationStrength] == n)], Message["invalid value for regularization strength lambda encountered."]; Return[$Failed]];

    {oCentering} = OptionValue[Options[KernelRidgeRegression][[All,1]]];
    If[Not[MemberQ[{False,True}, oCentering]], Message["invalid value for option Centering encountered."]; Return[$Failed]];

    (* Centering of labels *)
    ymean = If[oCentering, Chop[Mean[y]], 0.];
    yc = y - ymean;

    (* Centering of kernel matrix *)
    kkc = If[oCentering, CenterKernelMatrix[kk], kk];
    regularizationStrength = If[VectorQ[regularizationStrength], regularizationStrength, ConstantArray[regularizationStrength, n]]; (* Transform lambda to vector form if scalar *)
    kkc += DiagonalMatrix[regularizationStrength];

    (* Regression coefficients *)
    uu = Quiet[Check[CholeskyDecomposition[kkc], $Failed, {CholeskyDecomposition::posdef}], {CholeskyDecomposition::posdef}];  (* U^T U = Kc + lambda I *)
    If[uu === $Failed, Message[KernelRidgeRegression::fail, StringForm["regularization strength too low; ``-``", NumberForm[Min[regularizationStrength], 3], NumberForm[Max[regularizationStrength], 3]]]; Return[$Failed];];  (* If matrix decomposition fails, noise level is too low. Abort and leave consequences to caller (e.g., increase noise level) *)
    alpha = BackwardForwardSubstitution[uu, yc];  (* alpha = (Kc + sigma^2 I)^-1 y *)

    (* Processed option values *)
    popts = MapThread[#1[[1]] -> #2 &, {Options[KernelRidgeRegression], {oCentering}}];

    kernelRidgeRegressionModel[<|
        "KernelMatrix" -> kk,                                (* Kernel matrix (as passed, e.g., not centered; n x n matrix) *)
        "CholeskyDecomposition" -> uu,                       (* Cholesky decomposition (n x n upper triangular matrix) of K + lambda I (with regularization constant) *)
        "Labels" -> yc,                                      (* Centered label vector (n vector) *)
        "LabelMean" -> ymean,                                (* Mean of label vector (scalar) *)
        "Weights" -> alpha,                                  (* Regression weights (n vector) *)
        "Theta" -> theta,                                    (* Method hyperparameters (as passed) *)
        "RegularizationStrength" -> regularizationStrength,  (* Regularization weight lambda (processed) *)
        "Options" -> popts                                   (* Processed options (list) *)
    |>]
];

kernelRidgeRegressionModel[state_]["Properties"] = {"Properties", "Usage", "CholeskyDecomposition", "KernelMatrix", "LabelMean", "Labels", "Theta", "RegularizationStrength", "Options", "Weights"};
kernelRidgeRegressionModel[state_]["Usage"] = kernelRidgeRegressionModel::usage;
kernelRidgeRegressionModel[state_]["CholeskyDecomposition"] := state["CholeskyDecomposition"];
kernelRidgeRegressionModel[state_]["KernelMatrix"] := state["KernelMatrix"];
kernelRidgeRegressionModel[state_]["LabelMean"] := state["LabelMean"];
kernelRidgeRegressionModel[state_]["Labels"] := state["Labels"];
kernelRidgeRegressionModel[state_]["RegularizationStrength"] := state["RegularizationStrength"];
kernelRidgeRegressionModel[state_]["Options"] := state["Options"];
kernelRidgeRegressionModel[state_]["Weights"] := state["Weights"];
kernelRidgeRegressionModel[state_]["Theta"] := state["Theta"];

kernelRidgeRegressionModel[state_][ll_?(MatrixQ[#, NumericQ] &)] := kernelRidgeRegressionModel[state][ll, "Predictions"];
kernelRidgeRegressionModel[state_][ll_?(MatrixQ[#, NumericQ] &), "Predictions"] := Module[
    {n, m, kk, llc, alpha, ymean, opts, oCenter},

    (* Parameter and options processing *)
    {n, m} = Dimensions[ll];

    {opts, kk, ymean, alpha} = Lookup[state, {"Options", "KernelMatrix", "LabelMean", "Weights"}];

    oCenter = OptionValue[KernelRidgeRegression, opts, Centering];

    (* Center kernel matrix if required *)
    llc = If[oCenter, CenterKernelMatrix[kk, ll], ll];

    (* Predictive mean *)
    Transpose[llc] . alpha + ymean  (* If no label centering, mean will be 0 *)
];

End[]


(*  *******************************  *)
(*  *  GaussianProcessRegression  *  *)
(*  *******************************  *)

If[Not[ValueQ[GaussianProcessRegression::usage]], GaussianProcessRegression::usage =
"GaussianProcessRegression[K,y,{s2}] returns a Gaussian process regression model.
Parameters:
  K - Kernel matrix of training samples.
  y - Labels of training samples.
  s2 - Noise variance. Either a non-negative scalar or a vector.
Options:
  Centering [False] - If True, kernel matrix is centered to have zero mean in kernel feature space.
Returns:
  model - A model that can be used to predict new samples and queried for parameters.
  Usage of the model:
    model[L], model[L,\"Predictions\"] - Returns predictions for new samples; L is the kernel matrix between training and test samples.
    model[\"Properties\"] - Returns a list of queryable properties.
Remarks:
  If Centering is True, L and M are also centered with respect to K for predictions.
  Kernel ridge regression models and Gaussian process regression models yield identical predictions.
"];

Begin["`Private`"]

SetAttributes[gaussianProcessRegressionModel, HoldAll];

GaussianProcessRegression::warn = "Warning: ``";
GaussianProcessRegression::fail = "Gaussian process regression failed: ``";

Options[GaussianProcessRegression] = {Centering -> False};

GaussianProcessRegression[kk_?(MatrixQ[#, NumericQ] &), y_?(VectorQ[#, NumericQ] &), theta_, opts:OptionsPattern[]] := Module[
    {n,ymean,yc,kkc,uu,alpha,popts,noisevariance,oCentering},

    (* Parameter and options processing *)
    n = Length[y]; Assert[n > 0];
    If[Dimensions[kk] != {n,n}, Message[GaussianProcessRegression::fail, "incompatible dimensions of kernel matrix and label vector."]; Return[$Failed]];
    noisevariance = If[Not[ListQ[theta] && Length[theta] == 1], Message["invalid format for noise variance {s2}"]; Return[$Failed], First[theta]];
    If[Not[NumericQ[noisevariance] || (VectorQ[noisevariance, NumericQ] && Length[noisevariance] == n)], Message["invalid value for noise variance s2 encountered."]; Return[$Failed]];

    {oCentering} = OptionValue[Options[GaussianProcessRegression][[All,1]]];
    If[Not[MemberQ[{False,True}, oCentering]], Message["invalid value for option Centering encountered."]; Return[$Failed]];

    (* Centering of labels *)
    ymean = If[oCentering, Chop[Mean[y]], 0];
    yc = y - ymean;

    (* Centering of kernel matrix *)
    kkc = If[oCentering, CenterKernelMatrix[kk], kk];
    noisevariance = If[VectorQ[noisevariance], noisevariance, ConstantArray[noisevariance, n]]; (* Transform lambda to vector form if scalar *)
    kkc += DiagonalMatrix[noisevariance];

    (* Regression coefficients *)
    uu = Quiet[Check[CholeskyDecomposition[kkc], $Failed, {CholeskyDecomposition::posdef}], {CholeskyDecomposition::posdef}];  (* U^T U = Kc + lambda I *)
    If[uu === $Failed, Message[GaussianProcessRegression::fail, StringForm["regularization strength too low; ``-``", NumberForm[Min[regularizationStrength], 3], NumberForm[Max[regularizationStrength], 3]]]; Return[$Failed];];  (* If matrix decomposition fails, noise level is too low. Abort and leave consequences to caller (e.g., increase noise level) *)
    alpha = BackwardForwardSubstitution[uu, yc];  (* alpha = (Kc + sigma^2 I)^-1 y *)

    (* Processed option values *)
    popts = MapThread[#1[[1]] -> #2 &, {Options[GaussianProcessRegression], {oCentering}}];

    gaussianProcessRegressionModel[<|
        "KernelMatrix" -> kk,           (* Kernel matrix (as passed, e.g., not centered; n x n matrix) *)
        "CholeskyDecomposition" -> uu,  (* Cholesky decomposition (n x n upper triangular matrix) of K + lambda I (with regularization constant) *)
        "Labels" -> yc,                 (* Centered label vector (n vector) *)
        "LabelMean" -> ymean,           (* Mean of label vector (scalar) *)
        "Weights" -> alpha,             (* Regression weights (n vector) *)
        "Theta" -> theta,               (* Method hyperparameters (as passed) *)
        "Options" -> popts              (* Processed options (list) *)
    |>]
];

End[]


EndPackage[]
