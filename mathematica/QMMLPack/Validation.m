(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2016.         *)
(*  See LICENSE.txt for license. *)

(* Regression models *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ********************  *)
(*  *  Stratification  *  *)
(*  ********************  *)

If[Not[ValueQ[Stratify::usage]], Stratify::usage =
"Stratify[y,k] returns a list of stratified indices.

Parameters:
  y - List of numerical values.
  k - Number of strata.

Returns:
  List of strata indices. Each stratum has either Floor[Length[y]]/k entries or one more (the first Mod[Length[y],k] ones).
  Labels are sorted in ascending order.

Divides indices of sorted labels into same-size blocks.
Works for numeric class and regression labels."];

Begin["`Private`"]

Stratify[labels_?(VectorQ[#, NumericQ] &), numstrata_?(IntegerQ[#] && Positive[#] &)] := Module[
    {n, order, div, rem, splitind, i},

    n = Length[labels];
    {div, rem} = QuotientRemainder[n, numstrata];
    splitind = Table[{1 + (i - 1)*div + Min[i - 1, rem], i*div + Min[i, rem]}, {i, numstrata}];
    order = Ordering[labels];
    Map[order[[#[[1]] ;; #[[2]]]] &, splitind]
] /; numstrata <= Length[labels];

End[]


(*  *********************  *)
(*  *  LocalGridSearch  *  *)
(*  *********************  *)

If[Not[ValueQ[LocalGridSearch::usage]], LocalGridSearch::usage =
"LocalGridSearch[f,{v1,v2,...}] finds a local minimum of f[v1,v2,...] via discrete local gradient descent on a logarithmic grid.

A variable is a tuple {value, priority, stepsize, minimum, maximum, direction, base}, where
  value     = initial value; real number; refers to exponent
  priority  = positive integer; higher priorities are optimized before lower ones; several variables may have the same priority
  stepsize  = positive real number; refers to exponent
  minimum   = lowest allowed value for variable, value in [minimum,maximum]; refers to exponent
  maximum   = highest allowed value for variable, value in [minimum,maximum]; refers to exponent
  direction = either -1 (small values preferred), 0 (indifferent), +1 (large values preferred)
  base      = positive real number
  borderwarnings = whether to emit warnings if the value of an optimized variable is at it's boundaries.
  
Later entries can be omitted. Default values are {0., 1, 1., [-Infinity,+Infinity], 0, 2., {True,True}}.
Instead of a tuple, a list of rules or an association can be used.
The logarithmized grid is the intersection between ...,i-2s,i-s,i,i+s,i+2s,... and [min,max].

Options:
  \"Resolution\" [None] - If a, f[...] will be rounded to multiples of a.
  \"MaxEvaluations\" [Infinity] - Maximum number of times f is evaluated.
  EvaluationMonitor [None] - Function called with trialv, trialf, bestv, bestf, state, where state is an association.

If optimizing an expensive composite function, such as hyperparameters of a machine learning model, it is important that f uses cached values.
"];

Begin["`Private`"]

LocalGridSearch::error = "``";
LocalGridSearch::warning = "Warning: ``";

Options[LocalGridSearch] = {"Resolution"->None, "MaxEvaluations"->Automatic, EvaluationMonitor->None};

parseLocalGridSearchVariable[var_] := Module[{defaults, v},
    defaults = { {"value", 0.}, {"priority", 1}, {"stepsize", 1.}, {"minimum", -Infinity}, 
                 {"maximum", +Infinity}, {"direction", 0}, {"base", 2}, {"borderwarnings", {True, True}}};
    v = QMMLPack`Private`parseOptions[var, defaults];
    
    If[Not[NumericQ[v[[1]]]], (Message[LocalGridSearch::error, "Invalid initial value in variable "<>ToString[v]]; Throw[$Failed])];
    If[Not[IntegerQ[v[[2]]] && Positive[v[[2]]]], (Message[LocalGridSearch::error, "Invalid priority in variable "<>ToString[v]]; Throw[$Failed])];
    If[Not[NumericQ[v[[3]]] && Positive[v[[3]]]], (Message[LocalGridSearch::error, "Invalid step size in variable "<>ToString[v]]; Throw[$Failed])];
    If[Not[MemberQ[{-1,0,1}, v[[6]]]], (Message[LocalGridSearch::error, "Invalid direction in variable "<>ToString[v]]; Throw[$Failed])];
    If[v[[7]] <= 0, (Message[LocalGridSearch::error, "Invalid base in variable "<>ToString[v]]; Throw[$Failed])];
    
    If[v[[4]] > v[[5]], (Message[LocalGridSearch::error, "Invalid bounds in variable "<ToString[v]]; Throw[$Failed])];
    If[Not[v[[4]] <= v[[1]] <= v[[5]]], (Message[LocalGridSearch::error, "Initial value not in range in variable "<>ToString[v]]; Throw[$Failed])];

    v
];

localGridSearch[f_, vars_, resolution_, maxeval_, evaluationmonitor_] := Module[
    {numvars, numevals, done, conv, v, ftilde, bestv, bestf, trialstep, i, d},

    trialstep[] := Module[{trialpowers, state},
        (* determine trial value *)
        v = bestv; v[[i]] += d * vars[[i, 3]];
        v[[i]] = Clip[v[[i]], {vars[[i, 4]], vars[[i, 5]]}];
        If[v[[i]] == bestv[[i]] && d != 0, Return[False]];  (* was already at boundary before *)

        (* call f *)
        trialpowers = Power[vars[[All, 7]], v];
        ftilde = f@@trialpowers;      
        ++numevals; 
        If[numevals == maxeval, done = True; Message[LocalGridSearch::warning, "Maximum number of evaluations reached."];];
        If[Not[resolution === None], ftilde = Round[ftilde, resolution]];
        
        (* call evaluation monitor *)
        state = <| "variables" -> vars, "resolution" -> resolution, "max_evals" -> maxeval, "num_vars" -> numvars, 
            "num_evals" -> numevals, "converged" -> conv, "index" -> i, "direction" -> d |>;
        evaluationmonitor[v, ftilde, If[ftilde < bestf, v, bestv], If[ftilde < bestf, ftilde, bestf], state];
        
        (* update if improvement *)
        If[ftilde < bestf,
            {bestv, bestf, conv} = {v, ftilde, ConstantArray[False, numvars]};
            True
            ,
            False
        ]
    ];

    (* Initialization *)
    {numvars, numevals, done} = {Length[vars], 0, False};  (* number of variables, evaluations of f, termination condition *)
    {conv, bestv, bestf} = {ConstantArray[False, numvars], vars[[All,1]], Infinity};  (* convergence, best solution so far *)

    {i, d} = {1, 0};
    trialstep[];  (* obtain f for initial values *)

    While[Not[done],
        (* determine variable to optimize next *)
        i = Flatten[Position[conv, False, {1}]];  (* variables to be optimized *)
        i = i[[Flatten[Position[vars[[i, 2]], Max[vars[[i, 2]]], {1}]]]];  (* highest-priority variables *)
        i = RandomChoice[i];
        
        d = vars[[i, 6]];
        If[d == 0, d = RandomChoice[{-1, +1}]];
        
        (* optimize variable *)
        If[Not[trialstep[]], d = -d];
        While[Not[done] && trialstep[]];
        conv[[i]] = True;
        
        (* terminate if all variables are converged *)
        If[And@@conv, done = True];
    ];

    (* Warnings if found values lie on grid border *)
    For[i = 1, i <= numvars, ++i, (
        If[bestv[[i]] <= vars[[i, 4]] && vars[[i, 8, 1]], Message[LocalGridSearch::warning, "Parameter "<>IntegerString[i]<>" at lower grid bound."]];
        If[bestv[[i]] >= vars[[i, 5]] && vars[[i, 8, 2]], Message[LocalGridSearch::warning, "Parameter "<>IntegerString[i]<>" at upper grid bound."]];
    )];

    {bestv, bestf}
];

LocalGridSearch[f_, v_, OptionsPattern[]] := Module[
    {vars,resolution,maxeval,evaluationmonitor,oresolution,omaxeval,oevaluationmonitor},

    (* Parameters and options processing *)
    vars = Map[parseLocalGridSearchVariable, v];
    {oresolution, omaxeval, oevaluationmonitor} = Map[OptionValue[#] &, {"Resolution", "MaxEvaluations", EvaluationMonitor}];

    resolution = Switch[oresolution,
        None, None,
        _Real?Positive, oresolution,
        _, (Message[LocalGridSearch::error, "Invalid option value for \"Resolution\""]; Throw[$Failed])
    ];

    maxeval = Switch[omaxeval,
        Automatic, Infinity,
        _?(IntegerQ[#] && Positive[#] &), omaxeval,
        _, (Message[LocalGridSearch::error, "Invalid option value for \"MaxEvaluations\""]; Throw[$Failed])
    ];
    
    evaluationmonitor = Switch[oevaluationmonitor,
        None, Function[None],
        _, oevaluationmonitor
    ];

    (* Grid search *)
    localGridSearch[f, vars, resolution, maxeval, evaluationmonitor]
];

End[]


EndPackage[]
