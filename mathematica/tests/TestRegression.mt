(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for regression.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ***************************  *)
(*  *  KernelRidgeRegression  *  *)
(*  ***************************  *)

(* Queryable properties *)
Test[
    Module[{model},
        model = KernelRidgeRegression[KernelLinear[{{-1},{0},{1}}, {}], { -1, 0, 1 }, { 1 }, Centering -> False];
        N[{
            model["KernelMatrix"],
            model["LabelMean"],
            model["Labels"],
            model["Weights"],
            model["Theta"],
            model["RegularizationStrength"],
            ToCharacterCode[ToString[Sort[model["Options"][[All,1]]],InputForm]]
        }]
    ]
    ,
    N[{
        { { 1, 0, -1 }, { 0, 0, 0 }, { -1, 0, 1 } },
        0,  (* no centering *)
        { -1, 0, 1 },
        { -1/3, 0, 1/3 },
        { 1 },
        { 1, 1, 1 },
        ToCharacterCode[ToString[Sort[Options[KernelRidgeRegression][[All,1]]], InputForm]]
    }]
    ,
    TestID->"Regression-20160721-oErj45"
    ,
    SameTest->(ApproximatelyEqualListableAll[#1,#2] &)
]

(* Invalid Option *)
Test[
    KernelRidgeRegression[KernelLinear[{{1}}, {}], {1}, {1}, NonExistingOption->True]; True
    ,
    True
    ,
    {OptionValue::nodef}
    ,
    TestID->"Regression-20160721-Eo84kD"
]

(* Test for memory leaks is not reliable due to fluctuations in MemoryInUse[] *)

(* 2 samples in 1 dimension example, no centering *)
Test[
    Module[{X = {{-1}, {1}}, y = {-1, 1}, XX = {{-2}, {-1}, {0}, {1}, {2}}, model},
	      model = KernelRidgeRegression[KernelLinear[X, {}], y, { 10^-7 }, Centering->False];
	      model[KernelLinear[X, XX, {}], "Predictions"]
	  ]
	  ,
	  {-2, -1, 0, 1, 2}
	  ,
	  TestID->"KernelRidgeRegression-20160721-R6I6E4"
    ,
    SameTest->(ApproximatelyEqualListableAll[#1, #2, Precision->6, Accuracy->6] &)
]

(* 2 samples in 1 dimension example, with centering *)
Test[
	  Module[{X = {{-1}, {0}}, y = {0, 1}, XX = {{-2}, {-1}, {0}, {1}}, model},
	      model = KernelRidgeRegression[KernelLinear[X, {}], y, { 10^-7 }, Centering->True];
	      model[KernelLinear[X, XX, {}], "Predictions"]
	  ]
	  ,
	  { -1, 0, 1, 2 }
	  ,
	  TestID->"KernelRidgeRegression-20160721-ppqEr3"
    ,
    SameTest->(ApproximatelyEqualListableAll[#1, #2, Precision->6, Accuracy->6] &)
]
