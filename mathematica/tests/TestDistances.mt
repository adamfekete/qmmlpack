(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for kernel functions.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  *********************  *)
(*  *  DistanceOneNorm  *  *)
(*  *********************  *)

(* K *)

Test[  (* Same results as built-in function, small inputs *)
    Module[{X},
        X = RandomReal[{-1,1}, {11, 5}];
        Total[Chop[Flatten[DistanceMatrix[X, DistanceFunction->ManhattanDistance] - DistanceOneNorm[X]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20160804-AbD2P5"
]

Test[  (* Same results as built-in function, large inputs *)
    Module[{X},
        X = RandomReal[{-100,100}, {299, 111}];
        Total[Chop[Flatten[DistanceMatrix[X, DistanceFunction->ManhattanDistance] - DistanceOneNorm[X]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20160804-oLde90"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceOneNorm[{{1,2,3},{4,5,6}}]]
    ,
    True
    ,
    TestID->"Numerics-20160804-T2dea3"
]

(* L *)

Test[  (* Same results as built-in SquaredEuclideanDistance function, small inputs *)
    Module[{X,Z},
        X = RandomReal[{-1,1}, {12, 6}];
        Z = RandomReal[{-1,1}, {17, 6}];
        Total[Chop[Flatten[DistanceMatrix[X, Z, DistanceFunction->ManhattanDistance] - DistanceOneNorm[X, Z]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20160804-odP4iU"
]

Test[  (* Same results as built-in SquaredEuclideanDistance function, large inputs *)
    Module[{X,Z},
        X = RandomReal[{-100,100}, {300, 129}];
        Z = RandomReal[{-100,100}, {151, 129}];
        Total[Chop[Flatten[DistanceMatrix[X, Z, DistanceFunction->ManhattanDistance] - DistanceOneNorm[X, Z]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20160804-Gh938I"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceOneNorm[{{1,2,3},{4,5,6}}, {{-4,1,0},{5,-5,1}}]]
    ,
    True
    ,
    TestID->"Numerics-20160804-pppoL3"
]


(*  ***********************  *)
(*  *  DistanceEuclidean  *  *)
(*  ***********************  *)

(* 1-argument version *)

Test[  (* Same results as built-in EuclideanDistance function *)
    Module[{X},
        X = RandomReal[{-1,1}, {50, 5}];
        Total[Chop[Flatten[Outer[EuclideanDistance, X, X, 1] - DistanceEuclidean[X]], 10^-7]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20110817-J2D2P5"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceEuclidean[{{1,2,3},{4,5,6}}]]
    ,
    True
    ,
    TestID->"Numerics-20110817-T2L7N5"
]

(* 2-argument version *)

Test[  (* Same results as built-in EuclideanDistance function *)
    Module[{X,Z},
        X = RandomReal[{-1,1}, {50, 5}];
        Z = RandomReal[{-1,1}, {100, 5}];
        Total[Chop[Flatten[Outer[EuclideanDistance, X, Z, 1] - DistanceEuclidean[X, Z]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20110817-A7Y5G8"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceEuclidean[{{1,2,3},{4,5,6}}, {{-4,1,0},{5,-5,1}}]]
    ,
    True
    ,
    TestID->"Numerics-20110817-OeN6U7"
]


(*  ******************************  *)
(*  *  DistanceSquaredEuclidean  *  *)
(*  ******************************  *)

(* 1-argument version *)

Test[  (* Same results as built-in SquaredEuclideanDistance function *)
    Module[{X},
        X = RandomReal[{-1,1}, {50, 5}];
        Total[Chop[Flatten[Outer[SquaredEuclideanDistance, X, X, 1] - DistanceSquaredEuclidean[X]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20110703-J2D2P5"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceSquaredEuclidean[{{1,2,3},{4,5,6}}]]
    ,
    True
    ,
    TestID->"Numerics-20110703-T2L7N5"
]

(* 2-argument version *)

Test[  (* Same results as built-in SquaredEuclideanDistance function *)
    Module[{X,Z},
        X = RandomReal[{-1,1}, {50, 5}];
        Z = RandomReal[{-1,1}, {100, 5}];
        Total[Chop[Flatten[Outer[SquaredEuclideanDistance, X, Z, 1] - DistanceSquaredEuclidean[X, Z]]]]
    ]
    ,
    0
    ,
    TestID->"Numerics-20110707-B7Y5G8"
]

Test[  (* Result is packed array for speed of later computations with the matrix *)
    PackedArrayQ[DistanceSquaredEuclidean[{{1,2,3},{4,5,6}}, {{-4,1,0},{5,-5,1}}]]
    ,
    True
    ,
    TestID->"Numerics-20110707-O2N6U7"
]
