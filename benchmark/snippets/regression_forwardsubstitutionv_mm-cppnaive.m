(* qmmlpack                      *)
(* (c) Matthias Rupp, 2006-2016. *)
(* See LICENSE.txt for license.  *)

description = "Forward substitution (vector), Mathematica/C++/naive";

ngrid = {10, 50, 100, 500, 1000, 5000, 10000};  (* Size of matrix *)
theta = {{"n", ngrid}};  (* thetaGrid computed automatically *)

before[thetaval_, thetaind_] := (
    xx = LowerTriangularize[RandomReal[{-100, 100}, {thetaval[[1]], thetaval[[1]]}]];
    b  = RandomReal[{-100, 100}, thetaval[[1]]];
    If[Not[Developer`PackedArrayQ[xx]], bmError[StringFrom["error creating input data for theta = ``.", thetaind]]];
);

after[thetaval_, thetaind_, result_] := (
    If[Not[VectorQ[result, NumberQ]], bmError[StringForm["non-vector result for theta = ``.", thetaind]]];
    xx = None; ClearSystemCache[];
);

function[thetaval_, thetaind_] := ForwardSubstitution[xx, b];
