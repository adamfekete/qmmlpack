(* qmmlpack                      *)
(* (c) Matthias Rupp, 2006-2016. *)
(* See LICENSE.txt for license.  *)

description = "CenterKernelMatrix[K], Mathematica/C++V2"; (* does not exploit symmetry of K *)

ngrid = {10, 50, 100, 500, 1000, 5000, 10000};  (* n *)
theta = {{"n", ngrid}};  (* thetaGrid computed automatically *)

before[thetaval_, thetaind_] := (
    xx = RandomReal[{-100, 100}, {thetaval[[1]], thetaval[[1]]}];
    If[Not[Developer`PackedArrayQ[xx]], bmError[StringFrom["error creating input data for theta = ``.", thetaind]]];
);

after[thetaval_, thetaind_, result_] := (
    If[Not[MatrixQ[result, NumberQ]], bmError[StringForm["non-matrix result for theta = ``.", thetaind]]];
    xx = None; ClearSystemCache[];
);

function[thetaval_, thetaind_] := CenterKernelMatrix[xx];
